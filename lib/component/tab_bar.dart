import 'package:flutter/material.dart';

class MyTabBar extends StatefulWidget {
  final String text;
  final Function(dynamic) onPressed;
  final value;
  final groupValue;

  MyTabBar({Key key, this.text, this.onPressed, this.value, this.groupValue})
      : super(key: key);

  @override
  _MyTabBarState createState() => _MyTabBarState();
}

class _MyTabBarState extends State<MyTabBar> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onPressed(widget.value);
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
        margin: EdgeInsets.only(left: 10),
        decoration: BoxDecoration(
          color: widget.value == widget.groupValue
              ? Theme.of(context).accentColor
              : Colors.transparent,
          borderRadius: BorderRadius.all(Radius.circular(5)),
        ),
        child: Text(
          widget.text ?? '',
          style: TextStyle(
            fontSize: 15,
            color:
                widget.groupValue == widget.value ? Colors.white : Colors.grey,
          ),
        ),
      ),
    );
  }
}
