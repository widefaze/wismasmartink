enum AlertAction {
  cancel,
  discard,
  disagree,
  agree,
}

const String API_URL = "http://wismart-link.com/api/";
const String API_URL_NEW = "http://wismart-link.com/apis/";
const String NETWORK_ERROR = "Network Error";
const String SERVER_RESPONSE_ERROR = "Server Response Error";
const bool devMode = false;
const double textScaleFactor = 1.0;
