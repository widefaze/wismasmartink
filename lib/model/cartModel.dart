// To parse this JSON data, do
//
//     final cartModel = cartModelFromJson(jsonString);

import 'dart:convert';

CartModel cartModelFromJson(String str) => CartModel.fromJson(json.decode(str));

String cartModelToJson(CartModel data) => json.encode(data.toJson());

class CartModel {
  CartModel({
    this.id,
    this.namaBarang,
    this.hargaSatuan,
    this.qty,
    this.totalHarga,
    this.subtotal,
    this.img,
  });

  String id;
  String namaBarang;
  String hargaSatuan;
  String qty;
  String totalHarga;
  String subtotal;
  String img;

  factory CartModel.fromJson(Map<String, dynamic> json) => CartModel(
        id: json["id"],
        namaBarang: json["nama_barang"],
        hargaSatuan: json["harga_satuan"],
        qty: json["qty"],
        totalHarga: json["total_harga"],
        subtotal: json["subtotal"],
        img: json["img"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "nama_barang": namaBarang,
        "harga_satuan": hargaSatuan,
        "qty": qty,
        "total_harga": totalHarga,
        "subtotal": subtotal,
        "img": img,
      };
}
