// To parse this JSON data, do
//
//     final chat = chatFromJson(jsonString);

import 'dart:convert';

Chat chatFromJson(Map str) => Chat.fromJson((str));

String chatToJson(Chat data) => json.encode(data.toJson());

class Chat {
  Chat({
    this.name,
    this.chats,
  });

  String name;
  List<ChatElement> chats;

  factory Chat.fromJson(Map<String, dynamic> json) => Chat(
        name: json["name"],
        chats: List<ChatElement>.from(
            json["chats"].map((x) => ChatElement.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "chats": List<dynamic>.from(chats.map((x) => x.toJson())),
      };
}

class ChatElement {
  ChatElement({
    this.nama,
    this.message,
    this.time,
    this.sender,
    this.receiver,
  });

  String nama;
  String message;
  DateTime time;
  String sender;
  String receiver;

  factory ChatElement.fromJson(Map<String, dynamic> json) => ChatElement(
        nama: json["nama"],
        message: json["message"],
        time: DateTime.parse(json["time"]),
        sender: json["sender"],
        receiver: json["receiver"],
      );

  Map<String, dynamic> toJson() => {
        "nama": nama,
        "message": message,
        "time": time.toIso8601String(),
        "sender": sender,
        "receiver": receiver,
      };
}

User userFromJson(Map str) => User.fromJson((str));

class User {
  User({
    this.userId,
    this.nama,
  });

  String userId;
  String nama;

  factory User.fromJson(Map<String, dynamic> json) => User(
        userId: json["user_id"],
        nama: json["nama"],
      );
}
