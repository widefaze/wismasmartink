// To parse this JSON data, do
//
//     final helpdesk = helpdeskFromJson(jsonString);

import 'dart:convert';

Helpdesk helpdeskFromJson(Map str) => Helpdesk.fromJson((str));

String helpdeskToJson(Helpdesk data) => json.encode(data.toJson());

class Helpdesk {
  Helpdesk({
    this.id,
    this.idUser,
    this.kodeTiket,
    this.keterangan,
    this.level,
    this.status,
    this.picId,
    this.createdDate,
    this.updatedDate,
    this.message,
  });

  String id;
  String idUser;
  String kodeTiket;
  String keterangan;
  String level;

  /// Processed' & 'Selesai'
  String status;
  String picId;
  DateTime createdDate;
  String updatedDate;
  List<Message> message;

  factory Helpdesk.fromJson(Map<String, dynamic> json) => Helpdesk(
        id: json["id"],
        idUser: json["id_user"],
        kodeTiket: json["kode_tiket"],
        keterangan: json["keterangan"],
        level: json["level"],
        status: json["status"],
        picId: json["pic_id"],
        createdDate: DateTime.parse(json["created_date"]),
        updatedDate: json["updated_date"],
        message: json["message"] == null
            ? null
            : List<Message>.from(
                json["message"].map((x) => Message.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "id_user": idUser,
        "kode_tiket": kodeTiket,
        "keterangan": keterangan,
        "level": level,
        "status": status,
        "pic_id": picId,
        "created_date": createdDate.toIso8601String(),
        "updated_date": updatedDate,
        "message": List<dynamic>.from(message.map((x) => x.toJson())),
      };
}

class Message {
  Message({
    this.id,
    this.sentBy,
    this.ticketId,
    this.message,
    this.createdDate,
    this.updatedDate,
  });

  String id;
  String sentBy;
  String ticketId;
  String message;
  DateTime createdDate;
  String updatedDate;

  factory Message.fromJson(Map<String, dynamic> json) => Message(
        id: json["id"],
        sentBy: json["sent_by"],
        ticketId: json["ticket_id"],
        message: json["message"],
        createdDate: DateTime.parse(json["created_date"]),
        updatedDate: json["updated_date"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "sent_by": sentBy,
        "ticket_id": ticketId,
        "message": message,
        "created_date": createdDate.toIso8601String(),
        "updated_date": updatedDate,
      };
}
