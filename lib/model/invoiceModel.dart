// To parse this JSON data, do
//
//     final invoice = invoiceFromJson(jsonString);

import 'dart:convert';

Invoice invoiceFromJson(String str) => Invoice.fromJson(json.decode(str));

class Invoice {
  Invoice({
    this.sewaApartemen,
    this.maintenanceCharge,
    this.roomService,
    this.sewaFasilitas,
    this.periodeTagihan,
    this.jatuhTempo,
    this.tanggalBast,
    this.biayaKeamanan,
    this.biayaKebersihan,
    this.biayaAdmin,
    this.sinkingFund,
    this.biayaOperasional,
    this.biayaService,
    this.pdam,
    this.listrik,
    this.grandTotal,
    this.isPaid,
    this.nomorInvoice,
    this.namaUser,
    this.namaUnit,
    this.nomor,
    this.lantai,
    this.namaGedung,
    this.alamat,
    this.kota,
    this.namaApartemen,
  });

  SewaApartemen sewaApartemen;
  List<SewaApartemen> maintenanceCharge;
  List<SewaApartemen> roomService;
  List<SewaApartemen> sewaFasilitas;
  String periodeTagihan;
  String jatuhTempo;
  String tanggalBast;
  String biayaKeamanan;
  String biayaKebersihan;
  String biayaAdmin;
  String sinkingFund;
  String biayaOperasional;
  String biayaService;
  Pdam pdam;
  Listrik listrik;
  String grandTotal;
  String isPaid;
  String nomorInvoice;
  String namaUser;
  String namaUnit;
  String nomor;
  String lantai;
  String namaGedung;
  String alamat;
  String kota;
  String namaApartemen;

  factory Invoice.fromJson(Map<String, dynamic> json) => Invoice(
        sewaApartemen: SewaApartemen.fromJson(json["sewa_apartemen"]),
        maintenanceCharge: List<SewaApartemen>.from(
            json["maintenance_charge"].map((x) => SewaApartemen.fromJson(x))),
        roomService: List<SewaApartemen>.from(
            json["room_service"].map((x) => SewaApartemen.fromJson(x))),
        sewaFasilitas: List<SewaApartemen>.from(
            json["sewa_fasilitas"].map((x) => SewaApartemen.fromJson(x))),
        periodeTagihan: json["periode_tagihan"],
        jatuhTempo: json["jatuh_tempo"],
        tanggalBast: json["tanggal_bast"],
        biayaKeamanan: json["biaya_keamanan"],
        biayaKebersihan: json["biaya_kebersihan"],
        biayaAdmin: json["biaya_admin"],
        sinkingFund: json["sinking_fund"],
        biayaOperasional: json["biaya_operasional"],
        biayaService: json["biaya_service"],
        pdam: Pdam.fromJson(json["pdam"]),
        listrik: Listrik.fromJson(json["listrik"]),
        grandTotal: json["grand_total"],
        isPaid: json["is_paid"],
        nomorInvoice: json["nomor_invoice"],
        namaUser: json["nama_user"],
        namaUnit: json["nama_unit"],
        nomor: json["nomor"],
        lantai: json["lantai"],
        namaGedung: json["nama_gedung"],
        alamat: json["alamat"],
        kota: json["kota"],
        namaApartemen: json["nama_apartemen"],
      );
}

class Listrik {
  Listrik({
    this.tagihanListrik,
    this.meteranAwal,
    this.meteranAkhir,
  });

  String tagihanListrik;
  int meteranAwal;
  int meteranAkhir;

  factory Listrik.fromJson(Map<String, dynamic> json) => Listrik(
        tagihanListrik: json["tagihan_listrik"],
        meteranAwal: json["meteran_awal"],
        meteranAkhir: json["meteran_akhir"],
      );

  Map<String, dynamic> toJson() => {
        "tagihan_listrik": tagihanListrik,
        "meteran_awal": meteranAwal,
        "meteran_akhir": meteranAkhir,
      };
}

class SewaApartemen {
  SewaApartemen({
    this.item,
    this.description,
    this.total,
  });

  String item;
  String description;
  String total;

  factory SewaApartemen.fromJson(Map<String, dynamic> json) => SewaApartemen(
        item: json["item"],
        description: json["description"],
        total: json["total"],
      );
}

class Pdam {
  Pdam({
    this.tagihanPdam,
    this.meteranAwal,
    this.meteranAkhir,
  });

  String tagihanPdam;
  int meteranAwal;
  int meteranAkhir;

  factory Pdam.fromJson(Map<String, dynamic> json) => Pdam(
        tagihanPdam: json["tagihan_pdam"],
        meteranAwal: json["meteran_awal"],
        meteranAkhir: json["meteran_akhir"],
      );
}

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
