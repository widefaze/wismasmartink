// To parse this JSON data, do
//
//     final jumlahTransaksiToko = jumlahTransaksiTokoFromJson(jsonString);

import 'dart:convert';

JumlahTransaksiToko jumlahTransaksiTokoFromJson(String str) =>
    JumlahTransaksiToko.fromJson(json.decode(str));

String jumlahTransaksiTokoToJson(JumlahTransaksiToko data) =>
    json.encode(data.toJson());

class JumlahTransaksiToko {
  JumlahTransaksiToko({
    this.jumlah,
  });

  int jumlah;

  factory JumlahTransaksiToko.fromJson(Map<String, dynamic> json) =>
      JumlahTransaksiToko(
        jumlah: json["jumlah"],
      );

  Map<String, dynamic> toJson() => {
        "jumlah": jumlah,
      };
}
