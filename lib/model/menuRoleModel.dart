MenuRole menuRoleFromJson(Map str) => MenuRole.fromJson((str));

class MenuRole {
  MenuRole({
    this.id,
    this.menuName,
    this.add,
    this.edit,
    this.delete,
    this.view,
  });

  String id;
  String menuName;
  List<String> add;
  List<String> edit;
  List<String> delete;
  List<String> view;

  factory MenuRole.fromJson(Map<String, dynamic> json) => MenuRole(
        id: json["id"],
        menuName: json["menu_name"],
        add: json["add"] == null ? null : json["add"].split(', '),
        edit: json["edit"] == null ? null : json["edit"].split(', '),
        delete: json["delete"] == null ? null : json["delete"].split(', '),
        view: json["view"] == null ? null : json["view"].split(', '),
      );
}
