// To parse this JSON data, do
//
//     final userModel = userModelFromJson(jsonString);

import 'dart:convert';

UserModel userModelFromJson(String str) => UserModel.fromJson(json.decode(str));

String userModelToJson(UserModel data) => json.encode(data.toJson());

class UserModel {
  UserModel({
    this.idUser,
    this.nama,
    this.phoneNumber,
    this.email,
    this.tglLahir,
    this.nomorRekening,
    this.namaBank,
    this.namaUnit,
    this.nomor,
    this.lantai,
    this.namaGedung,
    this.alamat,
    this.kota,
    this.namaApt,
  });

  String idUser;
  String nama;
  String phoneNumber;
  String email;
  DateTime tglLahir;
  String nomorRekening;
  String namaBank;
  String namaUnit;
  String nomor;
  String lantai;
  String namaGedung;
  String alamat;
  String kota;
  String namaApt;

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        idUser: json["id_user"],
        nama: json["nama"],
        phoneNumber: json["phone_number"],
        email: json["email"],
        tglLahir: json["tgl_lahir"] == null
            ? null
            : DateTime.parse(json["tgl_lahir"]),
        nomorRekening: json["nomor_rekening"],
        namaBank: json["nama_bank"],
        namaUnit: json["nama_unit"],
        nomor: json["nomor"],
        lantai: json["lantai"],
        namaGedung: json["nama_gedung"],
        alamat: json["alamat"],
        kota: json["kota"],
        namaApt: json["nama_apt"],
      );

  Map<String, dynamic> toJson() => {
        "id_user": idUser,
        "nama": nama,
        "phone_number": phoneNumber,
        "email": email,
        "tgl_lahir": tglLahir,
        "nomor_rekening": nomorRekening,
        "nama_bank": namaBank,
        "nama_unit": namaUnit,
        "nomor": nomor,
        "lantai": lantai,
        "nama_gedung": namaGedung,
        "alamat": alamat,
        "kota": kota,
        "nama_apt": namaApt,
      };
}
