// To parse this JSON data, do
//
//     final voucher = voucherFromJson(jsonString);

import 'dart:convert';

Voucher voucherFromJson(Map str) => Voucher.fromJson((str));

String voucherToJson(Voucher data) => json.encode(data.toJson());

class Voucher {
  Voucher({
    this.id,
    this.title,
    this.image,
    this.description,
  });

  String id;
  String title;
  String image;
  String description;

  factory Voucher.fromJson(Map<String, dynamic> json) => Voucher(
        id: json["id"],
        title: json["title"],
        image: json["image"],
        description: json["description"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "image": image,
        "description": description,
      };
}
