import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';

class AnnouncementFormPage extends StatefulWidget {
  const AnnouncementFormPage({Key key}) : super(key: key);

  @override
  _AnnouncementFormPageController createState() =>
      _AnnouncementFormPageController();
}

class _AnnouncementFormPageController extends State<AnnouncementFormPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isLoading = false;
  TextEditingController keteranganController = new TextEditingController();
  TextEditingController judulController = new TextEditingController();

  @override
  Widget build(BuildContext context) => _AnnouncementFormPageView(this);

  void openTicket() async {
    if (judulController.text.isEmpty) {
      showSnackBar(scaffoldKey, "Judul is required");
    } else if (keteranganController.text.isEmpty) {
      showSnackBar(scaffoldKey, "Keterangan is required");
    } else {
      setState(() => isLoading = true);

      try {
        Response response = await new Dio().post(API_URL_NEW + 'announcement',
            data: new FormData.fromMap({
              "id_user": await getSession("user_id"),
              "title": judulController.text,
              "desc": keteranganController.text,
            }),
            options: Options(method: 'POST', responseType: ResponseType.json));

        setState(() => isLoading = false);

        if (response.toString() != '') {
          if (response.data['success'] == true) {
            showSnackBar(scaffoldKey, response.data['message']);
          } else {
            showSnackBar(scaffoldKey, response.data['message']);
          }
        } else {
          showSnackBar(scaffoldKey, NETWORK_ERROR);
        }

        Navigator.of(context).pop();
      } on DioError catch (e) {
        showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
      }
    }
  }
}

class _AnnouncementFormPageView extends StatelessWidget {
  final _AnnouncementFormPageController state;

  const _AnnouncementFormPageView(this.state, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: state.scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: hexToColor("#344b6b"),
          title: Text("Buat Announcement"),
        ),
        body: state.isLoading
            ? loadingScreen()
            : Container(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(25),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      TextField(
                        controller: state.judulController,
                        obscureText: false,
                        decoration: InputDecoration(
                            contentPadding:
                                EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                            hintText: "Judul",
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(32.0))),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      TextField(
                        maxLines: 5,
                        controller: state.keteranganController,
                        obscureText: false,
                        decoration: InputDecoration(
                            contentPadding:
                                EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                            hintText: "Deskripsi",
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(32.0))),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Material(
                        elevation: 5.0,
                        borderRadius: BorderRadius.circular(30.0),
                        color: Colors.green,
                        child: MaterialButton(
                          minWidth:
                              MediaQuery.of(this.state.context).size.width,
                          padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                          onPressed: state.openTicket,
                          child: Text(
                            "SUBMIT",
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ));
  }
}
