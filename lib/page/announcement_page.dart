import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';
import 'package:wismartlink/model/menuRoleModel.dart';
import 'package:wismartlink/page/announcement_form_page.dart';
import 'package:wismartlink/page/announcement_page_detail.dart';
import 'package:wismartlink/page/home_page.dart';

class AnnouncementPage extends StatefulWidget {
  final MenuRole menuRole;
  const AnnouncementPage({Key key, this.menuRole}) : super(key: key);

  @override
  _AnnouncementPageController createState() => _AnnouncementPageController();
}

class _AnnouncementPageController extends State<AnnouncementPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isLoading = false;
  List data;

  int showListLength = 3;

  @override
  void initState() {
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) => _AnnouncementPageView(this);

  void showMore() {
    int remainder = data.length - showListLength;

    if (remainder >= 3) {
      showListLength += 3;
    } else {
      showListLength += remainder;
    }

    setState(() {});
  }

  void getData() async {
    setState(() => isLoading = true);

    try {
      Response response = await new Dio().get(API_URL_NEW + 'announcement',
          options: Options(method: 'GET', responseType: ResponseType.json));

      setState(() => isLoading = false);

      if (response.toString() != '') {
        setState(() {
          data = response.data['data'];
          data.reversed;
        });
      } else {
        showSnackBar(scaffoldKey, NETWORK_ERROR);
      }
    } on DioError catch (e) {
      showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
    }
  }

  Future<void> newRequest() async {
    await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => AnnouncementFormPage(),
        ));

    getData();
  }
}

class _AnnouncementPageView extends StatelessWidget {
  final _AnnouncementPageController state;

  const _AnnouncementPageView(this.state, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: state.scaffoldKey,
        backgroundColor: Colors.white,
        floatingActionButton: !state.widget.menuRole.add.contains(roleId)
            ? Container()
            : FloatingActionButton.extended(
                onPressed: () => state.newRequest(),
                label: Text('Buat'),
                icon: Icon(Icons.add),
                backgroundColor: Colors.green,
              ),
        appBar: AppBar(
          backgroundColor: hexToColor("#344b6b"),
          title: Text("Announcement"),
        ),
        body: state.isLoading
            ? loadingScreen()
            : ListView(
                padding: EdgeInsets.only(bottom: 100, top: 20),
                children: [
                  ...state.data.sublist(0, state.showListLength).map((data) {
                    return GestureDetector(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (_) =>
                                AnnouncementPageDetail(data: data)));
                      },
                      child: Container(
                        margin: EdgeInsets.fromLTRB(20, 10, 20, 0),
                        padding: EdgeInsets.all(15),
                        decoration: BoxDecoration(
                            color: Colors.grey[50],
                            border: Border.all(color: Colors.grey[300]),
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black12,
                                blurRadius: 5,
                              )
                            ]),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              data['title'],
                              style: TextStyle(
                                color: Colors.lightBlue[800],
                                fontWeight: FontWeight.bold,
                                fontSize: 18,
                              ),
                            ),
                            SizedBox(height: 5),
                            Text(
                              DateFormat('dd MMMM yyyy')
                                  .format(DateTime.parse(data['created_date'])),
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(height: 5),
                            Text(
                              removeAllHtmlTags(data['desc']),
                              style: TextStyle(
                                color: Colors.grey,
                                fontWeight: FontWeight.w600,
                                height: 1.25,
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  }),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 15, 0, 10),
                        child: FlatButton(
                          padding: EdgeInsets.all(15),
                          color: Colors.lightBlue[800],
                          child: Text(
                            'Lihat Pengumuman Lainnya',
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                          onPressed: state.showMore,
                        ),
                      ),
                    ],
                  ),
                ],
              ));
  }
}
