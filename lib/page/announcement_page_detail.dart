import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:wismartlink/helper/utility.dart';

class AnnouncementPageDetail extends StatelessWidget {
  final Map data;
  const AnnouncementPageDetail({Key key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: hexToColor("#344b6b"),
        title: Text("Informasi Detail"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(25),
        child: ListView(
          children: [
            Text(
              data['title'],
              style: TextStyle(
                color: Colors.lightBlue[800],
                fontWeight: FontWeight.bold,
                fontSize: 18,
              ),
            ),
            SizedBox(height: 5),
            Text(
              DateFormat('dd MMMM yyyy')
                  .format(DateTime.parse(data['created_date'])),
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 25),
            Text(
              removeAllHtmlTags(data['desc']),
              style: TextStyle(
                color: Colors.grey,
                fontWeight: FontWeight.w600,
                height: 1.25,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
