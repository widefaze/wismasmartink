import 'package:flutter/material.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/utility.dart';

class CctvPage extends StatefulWidget {
  const CctvPage({Key key}) : super(key: key);

  @override
  _CctvPageController createState() => _CctvPageController();
}

class _CctvPageController extends State<CctvPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  int counter = 0;
  bool isLoading = false;

  @override
  Widget build(BuildContext context) => _CctvPageView(this);
}

class _CctvPageView extends StatelessWidget {
  final _CctvPageController state;

  const _CctvPageView(this.state, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: state.scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: hexToColor("#344b6b"),
          title: Text("CCTV"),
        ),
        body: state.isLoading
            ? loadingScreen()
            : Center(
                child: new SingleChildScrollView(
                  child: Container(
                    color: Colors.white,
                    child: Padding(
                      padding: const EdgeInsets.all(25),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          InkWell(
                              child: Text(
                            "CCTV",
                            textAlign: TextAlign.left,
                            style: TextStyle(fontSize: 25),
                          )),
                        ],
                      ),
                    ),
                  ),
                ),
              ));
  }
}
