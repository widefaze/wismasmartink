import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';
import 'package:wismartlink/model/chatModel.dart';
import 'package:wismartlink/model/menuRoleModel.dart';
import 'package:wismartlink/page/chat_page.dart';

class ChatContactPage extends StatefulWidget {
  final bool isSecurity;
  final MenuRole menuRole;

  const ChatContactPage({Key key, this.isSecurity = false, this.menuRole})
      : super(key: key);

  @override
  _ChatContactPageController createState() => _ChatContactPageController();
}

class _ChatContactPageController extends State<ChatContactPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  bool isLoading = false;

  String kodeTiket;
  List<User> data;

  @override
  void initState() {
    super.initState();
    fetchUser();
  }

  @override
  Widget build(BuildContext context) => _ChatContactPageView(this);

  Future fetchUser() async {
    setState(() => isLoading = true);

    try {
      var userId = await getSession("user_id");
      Response response;

      response = await Dio().get(
          API_URL_NEW +
              "chat/${widget.isSecurity ? 'security' : 'user'}?user_id=$userId'",
          options: Options(responseType: ResponseType.json));

      var json = response.data;

      print("json " + json["data"].toString());

      if (response.toString() != '') {
        if (response.statusCode == 200) {
          if (json['data'].isNotEmpty) {
            List<User> temp = [];
            json['data'].forEach((v) {
              temp.add(userFromJson(v));
            });
            data = temp;
          }
          if (mounted) setState(() {});
        } else {
          throw Exception('Failed to load');
        }
      } else {
        showSnackBar(scaffoldKey, NETWORK_ERROR);
      }
    } on DioError catch (e) {
      showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
    } catch (e) {
      debugPrint("Error $e");
    } finally {
      if (mounted) setState(() => isLoading = false);
    }
  }
}

class _ChatContactPageView extends StatelessWidget {
  final _ChatContactPageController state;

  const _ChatContactPageView(this.state, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: state.scaffoldKey,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: hexToColor("#344b6b"),
        title:
            Text(state.widget.isSecurity ? "Contact Security" : "My Contact"),
      ),
      body: state.data == null
          ? loadingScreen()
          : Column(
              children: <Widget>[
                Flexible(
                  child: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: ListView.separated(
                      itemCount: state.data?.length ?? 0,
                      itemBuilder: (context, index) {
                        User item = state.data[index];
                        return ListTile(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => ChatPage(
                                      userId: item.userId,
                                      menuRole: state.widget.menuRole,
                                    )));
                          },
                          title: Text(
                            '${item.nama}',
                            style: TextStyle(fontSize: 15),
                          ),
                        );
                      },
                      separatorBuilder: (context, index) {
                        return Divider();
                      },
                    ),
                  ),
                ),
              ],
            ),
    );
  }
}
