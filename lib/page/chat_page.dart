import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';
import 'package:wismartlink/model/chatModel.dart';
import 'package:wismartlink/model/menuRoleModel.dart';
import 'package:wismartlink/page/home_page.dart';

class ChatPage extends StatefulWidget {
  final String userId;
  final MenuRole menuRole;
  const ChatPage({Key key, this.userId, this.menuRole}) : super(key: key);

  @override
  _ChatPageController createState() => _ChatPageController();
}

class _ChatPageController extends State<ChatPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  bool isLoading = false;

  bool _isComposing = false;

  Chat data;
  Timer timer;
  final twentyMillis = const Duration(seconds: 5);

  TextEditingController chatPageBoxTC = TextEditingController();

  @override
  void initState() {
    super.initState();
    timer = Timer.periodic(twentyMillis, (Timer t) => fetchChatPageData());
  }

  @override
  void dispose() {
    super.dispose();
    timer.cancel();
  }

  @override
  Widget build(BuildContext context) => _ChatPageView(this);

  Future fetchChatPageData() async {
    setState(() => isLoading = true);

    try {
      var myId = await getSession("user_id");

      Response response;

      response =
          await Dio().get(API_URL_NEW + "chat/message", queryParameters: {
        "me": myId,
        "friend": widget.userId,
      });

      final json = response.data;

      print("json " + json["data"].toString());

      if (response.toString() != '') {
        if (response.statusCode == 200) {
          if (json['data'].isNotEmpty) {
            data = chatFromJson(json['data']);
            data.chats.sort((a, b) => a.time.compareTo(b.time));
          }
          if (mounted) setState(() {});
        } else {
          throw Exception('Failed to load');
        }
      } else {
        showSnackBar(scaffoldKey, NETWORK_ERROR);
      }
    } on DioError catch (e) {
      showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
    } catch (e) {
      debugPrint("Error $e");
    } finally {
      if (mounted) setState(() => isLoading = false);
    }
  }

  Future sendMessage() async {
    setState(() => isLoading = true);

    try {
      var myId = await getSession("user_id");

      Response response = await Dio().post(API_URL_NEW + 'chat/send',
          data: FormData.fromMap({
            "sender": "$myId",
            "receiver": "${widget.userId}",
            "message": "${chatPageBoxTC.text}",
          }),
          options: Options(method: 'POST', responseType: ResponseType.json));

      if (mounted) setState(() => isLoading = false);

      if (response.toString() != '') {
        if (response.statusCode == 200) {
          chatPageBoxTC.clear();
          fetchChatPageData();
        } else {
          throw Exception('Failed to load');
        }
      } else {
        showSnackBar(scaffoldKey, NETWORK_ERROR);
      }
    } on DioError catch (e) {
      showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
    }
  }

  Widget _buildTextComposer() {
    return IconTheme(
      data: IconThemeData(color: Theme.of(context).accentColor),
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Row(children: <Widget>[
          Flexible(
            child: TextField(
              controller: chatPageBoxTC,
              onChanged: (String text) {
                _isComposing = text.length > 0;
                setState(() {});
              },
              decoration: InputDecoration.collapsed(hintText: "Send a message"),
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 4.0),
            child: IconButton(
              icon: Icon(Icons.send),
              color: Colors.green,
              onPressed: _isComposing ? () => (sendMessage()) : null,
            ),
          ),
        ]),
        decoration: BoxDecoration(
          border: Border(
            top: BorderSide(
              color: Colors.grey[200],
            ),
          ),
        ),
      ),
    );
  }
}

class _ChatPageView extends StatelessWidget {
  final _ChatPageController state;

  const _ChatPageView(this.state, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: state.scaffoldKey,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: hexToColor("#344b6b"),
        title: Text("ChatPage"),
      ),
      body: state.data == null
          ? loadingScreen()
          : Column(
              children: <Widget>[
                Flexible(
                  child: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: ListView.separated(
                      itemCount: state.data.chats.length,
                      itemBuilder: (context, index) {
                        var item = state.data.chats[index];
                        return ListTile(
                          title: Text(
                            '${item.nama}',
                            style: TextStyle(fontSize: 15),
                          ),
                          subtitle: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 8.0, bottom: 5),
                                child: Text(
                                  item.message,
                                  style: TextStyle(fontSize: 15),
                                ),
                              ),
                              Text(
                                '${DateFormat('dd/MM/yy HH:mm').format(item.time)}',
                                style: TextStyle(fontSize: 12),
                              ),
                            ],
                          ),
                        );
                      },
                      separatorBuilder: (context, index) {
                        return Divider();
                      },
                    ),
                  ),
                ),
                Divider(height: 1.0),
                !state.widget.menuRole.add.contains(roleId)
                    ? Container()
                    : Container(
                        decoration:
                            BoxDecoration(color: Theme.of(context).cardColor),
                        child: state._buildTextComposer(),
                      ),
              ],
            ),
    );
  }
}
