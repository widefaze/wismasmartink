import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';

class ContactPage extends StatefulWidget {
  const ContactPage({Key key}) : super(key: key);

  @override
  _ContactPageController createState() => _ContactPageController();
}

class _ContactPageController extends State<ContactPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isLoading = false;
  List data;

  final TextEditingController controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) => _ContactPageView(this);

  void getData() async {
    setState(() => isLoading = true);

    try {
      Response response = await new Dio().get(API_URL_NEW + 'contact/about',
          options: Options(responseType: ResponseType.json));

      setState(() => isLoading = false);

      if (response.toString() != '') {
        setState(() {
          data = response.data['data'];
        });
      } else {
        showSnackBar(scaffoldKey, NETWORK_ERROR);
      }
    } on DioError catch (e) {
      showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
    } finally {
      setState(() => isLoading = false);
    }
  }

  Future sendMesage() async {
    if (controller.text.isEmpty) {
      showSnackBar(scaffoldKey, 'Pesan masih kosong!');
      return;
    }

    setState(() => isLoading = true);

    try {
      Response response = await new Dio().post(API_URL_NEW + 'contact',
          data: new FormData.fromMap({
            "id_user": await getSession("user_id"),
            "message": controller.text,
          }),
          options: Options(method: 'POST', responseType: ResponseType.json));

      print(response.data);

      if (response.toString().isNotEmpty) {
        if (response.data['message'] == 'success') {
          print(response.data);
          showSnackBar(scaffoldKey, 'Pesan berhasil terkirim!');
          controller.clear();
        } else {
          showSnackBar(scaffoldKey, response.data['message']);
        }
      } else {
        showSnackBar(scaffoldKey, NETWORK_ERROR);
      }
    } on DioError catch (_) {
      showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
    }

    setState(() => isLoading = false);
  }
}

class _ContactPageView extends StatelessWidget {
  final _ContactPageController state;

  const _ContactPageView(this.state, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: state.scaffoldKey,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: hexToColor("#344b6b"),
        title: Text("Kontak Kami"),
      ),
      body: state.isLoading
          ? loadingScreen()
          : ListView(
              padding: EdgeInsets.all(25),
              children: [
                Text(
                  'Gedung ABC',
                  style: TextStyle(
                    fontSize: 25,
                    color: Colors.blue[800],
                    fontWeight: FontWeight.w600,
                  ),
                ),
                ...state.data.map((val) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 15),
                      Text(
                        val['title'],
                        style: TextStyle(
                          fontSize: 18,
                          color: Colors.blue[800],
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      SizedBox(height: 10),
                      Text(
                        val['text'],
                        style: TextStyle(
                          fontSize: 12,
                          color: Colors.grey,
                          fontWeight: FontWeight.w600,
                          height: 1.25,
                        ),
                      ),
                    ],
                  );
                }).toList(),
                SizedBox(height: 25),
                Container(
                  padding: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                    color: Colors.blue[800],
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Kirim Pesan ke Pengelola',
                        style: TextStyle(
                          fontSize: 18,
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      SizedBox(height: 10),
                      TextFormField(
                        controller: state.controller,
                        minLines: 5,
                        maxLines: 5,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          hintText: 'Isi Pesan...',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5),
                          ),
                        ),
                      ),
                      SizedBox(height: 15),
                      state.isLoading
                          ? loadingScreen()
                          : Container(
                              width: double.maxFinite,
                              child: FlatButton(
                                padding: EdgeInsets.all(15),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5)),
                                color: Colors.blue[300],
                                child: Text('Kirim Pesan'),
                                onPressed: state.sendMesage,
                              ),
                            ),
                    ],
                  ),
                ),
              ],
            ),
    );
  }
}
