import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';
import 'package:wismartlink/page/emergency_page.dart';

var globalContext;

enum Type { maintenance, security, roomservice }

class ContactWAPage extends StatefulWidget {
  final Type type;
  ContactWAPage({Key key, @required this.type}) : super(key: key);

  @override
  _ContactWAPageState createState() => _ContactWAPageState();
}

class _ContactWAPageState extends State<ContactWAPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController requestController = new TextEditingController();

  bool isLoading = true;
  var phone = '';
  String type;
  String typeText;
  String image;

  Type selectedType;

  @override
  void initState() {
    super.initState();
    selectedType = widget.type;

    switch (selectedType) {
      case Type.maintenance:
        type = 'maintenance';
        typeText = 'Maintenance';
        image =
            'https://my-care.co.uk/wp-content/uploads/2019/04/shutterstock_439956097-1210x423.jpg';
        break;
      case Type.roomservice:
        type = 'room_service';
        typeText = 'Room Service';
        image =
            'https://resources.stuff.co.nz/content/dam/images/1/x/v/a/7/8/image.related.StuffLandscapeSixteenByNine.710x400.1xv9wg.png/1573518466288.jpg';
        break;
      case Type.security:
        type = 'security';
        typeText = 'Security';
        image =
            'https://www.discoversoon.com/wp-content/uploads/2016/03/security-guard.jpg';
        break;
      default:
        type = 'maintenance';
        typeText = 'Maintenance';
        image =
            'https://my-care.co.uk/wp-content/uploads/2019/04/shutterstock_439956097-1210x423.jpg';
    }

    fetchNumber();
  }

  Future fetchNumber() async {
    try {
      Response response;

      response = await Dio().get(
        API_URL_NEW + "contact?type=$type",
      );

      final json = response.data;

      print("json " + json["data"].toString());

      if (response.toString() != '') {
        if (response.statusCode == 200 && json['data'].isNotEmpty) {
          phone = json['data']['number'];
          if (mounted) setState(() {});
        } else {
          throw Exception('Failed to load');
        }
      } else {
        showSnackBar(scaffoldKey, NETWORK_ERROR);
      }
    } on DioError catch (e) {
      showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
    } catch (e) {
      debugPrint("Error $e");
    } finally {
      if (mounted) setState(() => isLoading = false);
    }
  }

  void requestMaintenance() async {
    if (requestController.text.isEmpty) {
      showSnackBar(scaffoldKey, "Service Description is required");
    } else {
      var whatsappUrl =
          "whatsapp://send?phone=$phone&text=${Uri.parse(requestController.text)}";

      print(whatsappUrl);

      await canLaunch(whatsappUrl)
          ? launch(whatsappUrl)
          : showSnackBar(scaffoldKey, 'Anda tidak memiliki aplikasi WhatsApp');
    }
  }

  @override
  Widget build(BuildContext context) {
    globalContext = context;

    return Scaffold(
        key: scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: hexToColor("#344b6b"),
          title: Text(typeText),
        ),
        body: isLoading
            ? loadingScreen()
            : Column(
                children: [
                  Expanded(
                    child: Container(
                      color: Colors.white,
                      child: Padding(
                        padding: const EdgeInsets.all(25),
                        child: ListView(
                          children: <Widget>[
                            Text(
                              'Hubungi Tim $typeText',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline5
                                  .copyWith(
                                    fontWeight: FontWeight.bold,
                                  ),
                              textAlign: TextAlign.center,
                            ),
                            SizedBox(height: 25),
                            Image.network(
                              image,
                              height: 150,
                              fit: BoxFit.cover,
                            ),
                            SizedBox(height: 25),
                            TextField(
                              controller: requestController,
                              obscureText: false,
                              minLines: 5,
                              maxLines: 5,
                              decoration: InputDecoration(
                                  contentPadding: EdgeInsets.fromLTRB(
                                      20.0, 15.0, 20.0, 15.0),
                                  hintText: "Deskripsi...",
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10))),
                            ),
                            SizedBox(height: 20.0),
                            Material(
                              borderRadius: BorderRadius.circular(10),
                              color: Colors.blue[400],
                              child: MaterialButton(
                                minWidth:
                                    MediaQuery.of(this.context).size.width,
                                padding:
                                    EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                                onPressed: requestMaintenance,
                                child: Text(
                                  "WA Chat",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: FlatButton(
                          padding: EdgeInsets.all(22.5),
                          color: Colors.grey[100],
                          child: Text('Panic Button'),
                          onPressed: () {
                            Navigator.of(context).pushReplacement(
                                MaterialPageRoute(
                                    builder: (_) => EmergencyPage()));
                          },
                        ),
                      ),
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border(
                                  top: BorderSide(color: Colors.blue[500]))),
                          child: FlatButton(
                            padding: EdgeInsets.all(22.5),
                            color: Colors.grey[100],
                            child: Text(
                              'Ticket Helpdesk',
                              style: TextStyle(
                                color: Colors.blue[500],
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            onPressed: () {},
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ));
  }
}
