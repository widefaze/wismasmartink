import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';

class EmergencyPage extends StatefulWidget {
  const EmergencyPage({Key key}) : super(key: key);

  @override
  _EmergencyPageController createState() => _EmergencyPageController();
}

class _EmergencyPageController extends State<EmergencyPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  int counter = 0;
  bool isLoading = false;

  @override
  Widget build(BuildContext context) => _EmergencyPageView(this);

  void increaseCounter() => setState(() => counter += 1);

  void emergencyProcess() async {
    increaseCounter();

    if (counter == 5) {
      setState(() => isLoading = true);

      try {
        Response response = await new Dio().post(API_URL + 'emergency',
            data:
                new FormData.fromMap({"user_id": await getSession("user_id")}),
            options: Options(method: 'POST', responseType: ResponseType.json));

        setState(() => isLoading = false);

        if (response.toString() != '') {
          if (response.data['success'] == true) {
            showSnackBar(scaffoldKey, response.data['message']);
            counter = 0;
            setState(() {});
          } else {
            showSnackBar(scaffoldKey, response.data['message']);
          }
        } else {
          showSnackBar(scaffoldKey, NETWORK_ERROR);
        }
      } on DioError catch (e) {
        showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
      }
    }
  }
}

class _EmergencyPageView extends StatelessWidget {
  final _EmergencyPageController state;

  const _EmergencyPageView(this.state, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: state.scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: hexToColor("#344b6b"),
          title: Text(""),
        ),
        body: state.isLoading
            ? loadingScreen()
            : Stack(
                children: [
                  Image.asset(
                    'assets/images/bg.png',
                    fit: BoxFit.cover,
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                  ),
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.all(25),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            "Tombol Emergency Memanggil Security",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.white,
                            ),
                          ),
                          Spacer(),
                          Text(
                            "Ketuk 5 Kali",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontSize: 35,
                              color: Colors.amber[400],
                            ),
                          ),
                          SizedBox(
                            height: 30.0,
                          ),
                          GestureDetector(
                            child: Stack(
                              alignment: Alignment.center,
                              children: [
                                Image.asset(
                                  'assets/icon/button.png',
                                  width: 250,
                                  height: 250,
                                ),
                                Text(
                                  '${state.counter == 0 ? 'PANIC' : state.counter}',
                                  style: TextStyle(
                                    fontSize: 30,
                                    color: Colors.white,
                                  ),
                                ),
                              ],
                            ),
                            onTap: state.emergencyProcess,
                          ),
                          Spacer(),
                          Text(
                            "Perhatian!\n\nTombol ini hanya untuk keadaan darurat,\nbukan untuk disalahgunakan",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.white,
                              height: 1.25,
                            ),
                          ),
                          Spacer(),
                        ],
                      ),
                    ),
                  ),
                ],
              ));
  }
}
