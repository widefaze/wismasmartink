import 'package:flutter/material.dart';
import 'package:wismartlink/helper/utility.dart';
import 'package:wismartlink/page/request_fasilitas_form_page.dart';

class FasilitasDetailPage extends StatefulWidget {
  final Map data;

  FasilitasDetailPage({Key key, this.data}) : super(key: key);

  @override
  _FasilitasDetailPageState createState() => _FasilitasDetailPageState();
}

class _FasilitasDetailPageState extends State<FasilitasDetailPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: hexToColor("#344b6b"),
      appBar: AppBar(
        backgroundColor: hexToColor("#344b6b"),
        title: Text("Detail Fasilitas"),
      ),
      body: Column(
        children: [
          Expanded(
            child: ListView(
              padding: EdgeInsets.all(35),
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Image.network(
                    widget.data['img'],
                  ),
                ),
                SizedBox(height: 25),
                Text(
                  widget.data['fasilitas'],
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 25,
                    fontWeight: FontWeight.w700,
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 15),
                Container(
                  padding: EdgeInsets.all(10),
                  color: Colors.black12,
                  child: Row(
                    children: [
                      Expanded(
                        child: Text(
                          'Setiap Hari',
                          style: TextStyle(
                            color: Colors.greenAccent,
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Text(
                        '08:00 - 18:00',
                        style: TextStyle(
                          color: Colors.greenAccent,
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  color: Colors.green,
                  child: Text(
                    'Hari Ini Buka',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w700,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(height: 15),
                Text(
                  'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ullamcorper, lorem at ornare ornare, est nisi pulvinar est, quis vulputate mi purus sit amet leo. Integer et nibh a orci aliquam suscipit. Nullam vel eros velit. Cras fringilla purus ante, vel luctus dui varius et. Donec nulla mi, pellentesque sed purus et, aliquam maximus sapien.',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 14,
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(15),
            color: Colors.white,
            child: Row(
              children: [
                Text('Punya rencana ke sini?'),
                Spacer(),
                FlatButton(
                  color: Colors.green,
                  child: Text(
                    'Isi Buku Tamu',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (_) => RequestFasilitasFormPage()));
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
