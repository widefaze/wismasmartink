import 'package:carousel_slider/carousel_slider.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';
import 'package:wismartlink/page/fasilitas_detail_page.dart';
import 'package:wismartlink/page/request_fasilitas_page.dart';

class FasilitasListPage extends StatefulWidget {
  const FasilitasListPage({Key key}) : super(key: key);

  @override
  _FasilitasListPageController createState() => _FasilitasListPageController();
}

class _FasilitasListPageController extends State<FasilitasListPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isLoading = false;
  List data;

  String query = '';

  void updateQuery(String text) {
    query = text;
    setState(() {});
  }

  final List<String> imgList = [
    'assets/images/slide1.png',
    'assets/images/slide2.png',
    'assets/images/slide3.jpg',
    'assets/images/slide4.png',
    'assets/images/slide5.png',
    'assets/images/slide6.png',
    'assets/images/slide7.png',
  ];

  @override
  void initState() {
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) => _FasilitasListPageView(this);

  void getData() async {
    setState(() => isLoading = true);

    try {
      Response response = await new Dio().post(API_URL + 'fasilitas',
          options: Options(method: 'POST', responseType: ResponseType.json));

      setState(() => isLoading = false);

      if (response.toString() != '') {
        setState(() {
          data = response.data['data'];
        });
      } else {
        showSnackBar(scaffoldKey, NETWORK_ERROR);
      }
    } on DioError catch (e) {
      showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
    }
  }
}

class _FasilitasListPageView extends StatelessWidget {
  final _FasilitasListPageController state;

  const _FasilitasListPageView(this.state, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: state.scaffoldKey,
        backgroundColor: hexToColor("#344b6b"),
        appBar: AppBar(
          backgroundColor: hexToColor("#344b6b"),
          title: Text("Fasilitas"),
          actions: [
            IconButton(
              icon: Icon(Icons.history),
              onPressed: () {
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (_) => RequestFasilitasPage()));
              },
            ),
          ],
        ),
        body: state.isLoading
            ? loadingScreen()
            : ListView(
                children: [
                  CarouselSlider(
                    height: 200,
                    autoPlay: true,
                    enableInfiniteScroll: true,
                    viewportFraction: 1.0,
                    enlargeCenterPage: true,
                    aspectRatio: MediaQuery.of(state.context).size.aspectRatio,
                    items: state.imgList.map(
                      (url) {
                        return Container(
                          child: ClipRRect(
                            borderRadius:
                                BorderRadius.all(Radius.circular(0.0)),
                            child: Image(image: AssetImage(url)),
                          ),
                        );
                      },
                    ).toList(),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(25, 25, 25, 0),
                    child: TextField(
                      onChanged: state.updateQuery,
                      style: TextStyle(color: Colors.white),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide: BorderSide.none,
                        ),
                        fillColor: Colors.black54,
                        filled: true,
                        prefixIcon: Icon(
                          Icons.search,
                          color: Colors.white,
                        ),
                        hintText: 'Cari Fasilitas',
                        hintStyle: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                  ListView.separated(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    padding: EdgeInsets.all(25),
                    itemCount: state.data?.length ?? 0,
                    itemBuilder: (context, index) {
                      if (state.data[index]['fasilitas']
                          .toString()
                          .toLowerCase()
                          .contains(state.query.toLowerCase())) {
                        return GestureDetector(
                          onTap: () {
                            Navigator.of(context)
                                .push(MaterialPageRoute(builder: (context) {
                              return FasilitasDetailPage(
                                  data: state.data[index]);
                            }));
                          },
                          child: Container(
                            height: 100,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Row(
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(10),
                                    topLeft: Radius.circular(10),
                                  ),
                                  child: Image.network(
                                    state.data[index]['img'],
                                    width: 125,
                                    height: 100,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.all(15),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    // mainAxisAlignment: MainAxisAlignment.,
                                    children: [
                                      Text(
                                        state.data[index]['fasilitas'],
                                        style: TextStyle(
                                          color: Colors.blue[800],
                                          fontWeight: FontWeight.w700,
                                          fontSize: 16,
                                        ),
                                      ),
                                      Spacer(),
                                      Text(
                                        'Setiap Hari',
                                        style: TextStyle(
                                          fontWeight: FontWeight.w700,
                                        ),
                                      ),
                                      Text(
                                        '08:00-18:00',
                                        style: TextStyle(
                                          fontWeight: FontWeight.w700,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      } else {
                        return Container();
                      }
                    },
                    separatorBuilder: (context, index) {
                      return Divider();
                    },
                  ),
                ],
              ));
  }
}
