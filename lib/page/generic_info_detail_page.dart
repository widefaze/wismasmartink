import 'package:flutter/material.dart';
import 'package:wismartlink/helper/utility.dart';

class GenericInfoDetailPage extends StatelessWidget {
  final String title, desc, image;
  const GenericInfoDetailPage({Key key, this.title, this.desc, this.image})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: hexToColor("#344b6b"),
        title: Text("Informasi Detail"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(25),
        child: ListView(
          children: [
            Image.network(image),
            SizedBox(height: 25),
            Text(title, style: Theme.of(context).textTheme.headline6),
            SizedBox(height: 15),
            Text(
              desc,
              style: Theme.of(context).textTheme.subtitle1,
            ),
          ],
        ),
      ),
    );
  }
}
