import 'package:path/path.dart' as path;
import 'package:async/async.dart';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';

class GenericInfoFormPage extends StatefulWidget {
  const GenericInfoFormPage({Key key}) : super(key: key);

  @override
  _MakananRegisterPageController createState() =>
      _MakananRegisterPageController();
}

class _MakananRegisterPageController extends State<GenericInfoFormPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isLoading = false;
  File imageFile;
  dynamic pickImageError;
  String retrieveDataError;
  TextEditingController titleController = new TextEditingController();
  TextEditingController descController = new TextEditingController();
  File _imageFile;

  @override
  Widget build(BuildContext context) => _BarangRegisterPageView(this);

  void sendData() async {
    if (titleController.text.isEmpty) {
      showSnackBar(scaffoldKey, "Title harap diisi");
    } else if (_imageFile == null) {
      showSnackBar(scaffoldKey, "Gambar belum dipilih");
    } else if (descController.text.isEmpty) {
      showSnackBar(scaffoldKey, "Description harap diisi");
    } else {
      setState(() => isLoading = true);

      try {
        var stream =
            http.ByteStream(DelegatingStream.typed(_imageFile.openRead()));
        var length = await _imageFile.length();
        var uri = Uri.parse(API_URL_NEW + 'information');
        var request = http.MultipartRequest("POST", uri);

        request.fields['title'] = titleController.text;
        request.fields['description'] = descController.text;

        request.files.add(http.MultipartFile("image", stream, length,
            filename: path.basename(_imageFile.path)));

        var response = await request.send();

        if (response.toString() != '') {
          if (response.statusCode == 201) {
            Alert(
              context: context,
              type: AlertType.success,
              title: 'success',
              buttons: [
                DialogButton(
                  child: Text(
                    "OK",
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                  onPressed: () async {
                    Navigator.pop(context);
                    Navigator.pop(context);
                  },
                  width: 120,
                )
              ],
            ).show();
          } else {
            showSnackBar(scaffoldKey, 'failed');
          }
        } else {
          showSnackBar(scaffoldKey, NETWORK_ERROR);
        }
      } catch (e) {
        debugPrint("Error $e");
      }
    }
  }

  void chooseImage(ImageSource source) async {
    try {
      imageFile = await ImagePicker.pickImage(source: source, imageQuality: 50);
      setState(() {});
    } catch (e) {
      pickImageError = e;
    }
  }

  _selectImage(ImageSource source) async {
    var image = await ImagePicker.pickImage(
        source: source, maxHeight: 1920.0, maxWidth: 1080.0);
    setState(() {
      _imageFile = image;
    });
  }

  Widget _previewImage() {
    final Text retrieveError = _getRetrieveErrorWidget();
    if (retrieveError != null) {
      return retrieveError;
    }
    if (_imageFile != null) {
      return ClipOval(
          child: Image.file(
        _imageFile,
        width: 200,
        height: 200,
      ));
    } else if (pickImageError != null) {
      return Text(
        'Pick image error: $pickImageError',
        textAlign: TextAlign.center,
      );
    } else {
      return const Text(
        'Image',
        textAlign: TextAlign.center,
      );
    }
  }

  Text _getRetrieveErrorWidget() {
    if (retrieveDataError != null) {
      final Text result = Text(retrieveDataError);
      retrieveDataError = null;
      return result;
    }
    return null;
  }

  Future<void> retrieveLostData() async {
    final LostDataResponse response = await ImagePicker.retrieveLostData();
    if (response.isEmpty) {
      return;
    }
    if (response.file != null) {
      setState(() {
        imageFile = response.file;
      });
    } else {
      retrieveDataError = response.exception.code;
    }
  }
}

class _BarangRegisterPageView extends StatelessWidget {
  final _MakananRegisterPageController state;

  const _BarangRegisterPageView(this.state, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: state.scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: hexToColor("#344b6b"),
          title: Text("Buat Informasi"),
        ),
        body: state.isLoading
            ? loadingScreen()
            : SingleChildScrollView(
                child: Container(
                  color: Colors.white,
                  child: Padding(
                    padding: const EdgeInsets.all(25),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        TextField(
                          controller: state.titleController,
                          decoration: InputDecoration(
                              contentPadding:
                                  EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                              hintText: "Title",
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(32.0))),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        TextField(
                          maxLines: 8,
                          controller: state.descController,
                          decoration: InputDecoration(
                              contentPadding:
                                  EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                              hintText: "Description",
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(32.0))),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Row(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(5),
                              child: FloatingActionButton(
                                onPressed: () {
                                  state._selectImage(ImageSource.gallery);
                                },
                                heroTag: 'image0',
                                child: const Icon(Icons.photo_library),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(5),
                              child: FloatingActionButton(
                                onPressed: () {
                                  state._selectImage(ImageSource.camera);
                                },
                                heroTag: 'image1',
                                child: const Icon(Icons.photo_camera),
                              ),
                            ),
                            Platform.isAndroid
                                ? FutureBuilder<void>(
                                    future: state.retrieveLostData(),
                                    builder: (BuildContext context,
                                        AsyncSnapshot<void> snapshot) {
                                      switch (snapshot.connectionState) {
                                        case ConnectionState.none:
                                        case ConnectionState.waiting:
                                          return const Text(
                                            'Gambar belum dipilih.',
                                            textAlign: TextAlign.center,
                                          );
                                        case ConnectionState.done:
                                          return state._previewImage();
                                        default:
                                          if (snapshot.hasError) {
                                            return Text(
                                              'Pick image/video error: ${snapshot.error}}',
                                              textAlign: TextAlign.center,
                                            );
                                          } else {
                                            return const Text(
                                              'Gambar belum dipilih.',
                                              textAlign: TextAlign.center,
                                            );
                                          }
                                      }
                                    },
                                  )
                                : state._previewImage()
                          ],
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Material(
                          elevation: 5.0,
                          borderRadius: BorderRadius.circular(30.0),
                          color: Colors.green,
                          child: MaterialButton(
                            minWidth:
                                MediaQuery.of(this.state.context).size.width,
                            padding:
                                EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                            onPressed: state.sendData,
                            child: Text(
                              "SUBMIT",
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ));
  }
}

typedef void OnPickImageCallback(
    double maxWidth, double maxHeight, int quality);
