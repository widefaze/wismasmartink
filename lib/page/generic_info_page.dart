import 'package:carousel_slider/carousel_slider.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';
import 'package:wismartlink/page/generic_info_detail_page.dart';

import 'generic_info_form_page.dart';

class GenericInfoPage extends StatefulWidget {
  const GenericInfoPage({Key key}) : super(key: key);

  @override
  _GenericInfoPageController createState() => _GenericInfoPageController();
}

class _GenericInfoPageController extends State<GenericInfoPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isLoading = false;
  List data;

  @override
  void initState() {
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) => _GenericInfoPageView(this);

  void getData() async {
    setState(() => isLoading = true);

    try {
      Response response = await new Dio().post(API_URL + 'informasi_generic',
          options: Options(method: 'GET', responseType: ResponseType.json));

      setState(() => isLoading = false);

      if (response.toString() != '') {
        print(response.data['data']);
        setState(() {
          data = response.data['data'];
        });
      } else {
        showSnackBar(scaffoldKey, NETWORK_ERROR);
      }
    } on DioError catch (e) {
      showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
    }
  }
}

class _GenericInfoPageView extends StatelessWidget {
  final _GenericInfoPageController state;

  const _GenericInfoPageView(this.state, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<String> imgList = [
      'assets/images/iklan1.jpg',
      'assets/images/iklan2.jpg',
    ];

    return Scaffold(
        key: state.scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: hexToColor("#344b6b"),
          title: Text("Information"),
        ),
        floatingActionButton: Padding(
          padding: const EdgeInsets.only(bottom: 100),
          child: FloatingActionButton.extended(
            onPressed: () {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (_) => GenericInfoFormPage()));
            },
            label: Text('Buat'),
            icon: Icon(Icons.add),
            backgroundColor: Colors.green,
          ),
        ),
        body: state.isLoading
            ? loadingScreen()
            : Column(
                children: [
                  Expanded(
                    child: ListView.separated(
                      itemCount: state.data?.length ?? 0,
                      itemBuilder: (context, index) {
                        return ListTile(
                          leading: Image.network(state.data[index]['image']),
                          onTap: () =>
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (_) => GenericInfoDetailPage(
                                        title: state.data[index]['title'],
                                        desc: removeAllHtmlTags(
                                            state.data[index]['description']),
                                        image: state.data[index]['image'],
                                      ))),
                          title: Text(state.data[index]['title']),
                          subtitle: Text(
                            removeAllHtmlTags(state.data[index]['description']),
                            maxLines: 1,
                          ),
                        );
                      },
                      separatorBuilder: (context, index) {
                        return Divider();
                      },
                    ),
                  ),
                  CarouselSlider(
                    height: 100,
                    autoPlay: true,
                    enableInfiniteScroll: true,
                    viewportFraction: 1.0,
                    enlargeCenterPage: true,
                    aspectRatio: MediaQuery.of(state.context).size.aspectRatio,
                    items: imgList.map(
                      (url) {
                        return Row(
                          children: [
                            Expanded(
                              child: ClipRRect(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(0.0)),
                                child: Image(
                                    image:
                                        AssetImage('assets/images/iklan1.jpg')),
                              ),
                            ),
                            Expanded(
                              child: ClipRRect(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(0.0)),
                                child: Image(
                                    image:
                                        AssetImage('assets/images/iklan2.jpg')),
                              ),
                            ),
                          ],
                        );
                      },
                    ).toList(),
                  ),
                ],
              ));
  }
}
