import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';
import 'package:wismartlink/model/helpDesk.dart';

class HelpdeskChatPage extends StatefulWidget {
  final String kodeTiket;
  const HelpdeskChatPage({Key key, this.kodeTiket}) : super(key: key);

  @override
  _HelpdeskChatPageController createState() => _HelpdeskChatPageController();
}

class _HelpdeskChatPageController extends State<HelpdeskChatPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  bool isLoading = false;
  bool isClosed = false;
  bool isNotFound = false;

  bool _isComposing = false;

  String kodeTiket;

  Helpdesk data;
  TextEditingController chatBoxTC = TextEditingController();

  @override
  void initState() {
    super.initState();
    fetchChatData();
  }

  @override
  Widget build(BuildContext context) => _HelpdeskChatPageView(this);

  Future fetchChatData() async {
    setState(() => isLoading = true);

    try {
      Response response;

      response = await Dio().get(
        API_URL_NEW + "helpdesk/ticket?kode_tiket='${widget.kodeTiket}'",
      );

      final json = response.data;

      print("json " + json["data"].toString());

      if (response.toString() != '') {
        if (response.statusCode == 200) {
          if (json['data'].isNotEmpty) {
            data = helpdeskFromJson(json['data'][0]);
            if (data.status.toLowerCase() == 'selesai') isClosed = true;
          } else {
            isNotFound = true;
          }
          if (mounted) setState(() {});
        } else {
          throw Exception('Failed to load');
        }
      } else {
        showSnackBar(scaffoldKey, NETWORK_ERROR);
      }
    } on DioError catch (e) {
      showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
    } catch (e) {
      debugPrint("Error $e");
    } finally {
      if (mounted) setState(() => isLoading = false);
    }
  }

  Future createTicket() async {
    setState(() => isLoading = true);

    try {
      Response response = await Dio().post(API_URL_NEW + 'helpdesk/ticket',
          data: FormData.fromMap({
            "id_user": await getSession("user_id"),
            "keterangan": "${chatBoxTC.text}",
          }),
          options: Options(method: 'POST', responseType: ResponseType.json));

      if (mounted) setState(() => isLoading = false);

      final json = response.data;

      print("json " + json["data"].toString());

      if (response.toString() != '') {
        if (response.statusCode == 201) {
          kodeTiket = json['data']["kode_tiket"];
          if (mounted) setState(() {});
          fetchChatData();
        } else {
          throw Exception('Failed to load');
        }
      } else {
        showSnackBar(scaffoldKey, NETWORK_ERROR);
      }
    } on DioError catch (e) {
      showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
    }
  }

  Future sendMessage() async {
    setState(() => isLoading = true);

    try {
      Response response = await Dio().post(API_URL_NEW + 'helpdesk/comment',
          data: FormData.fromMap({
            "kode_tiket": "${widget.kodeTiket}",
            "message": "${chatBoxTC.text}",
          }),
          options: Options(method: 'POST', responseType: ResponseType.json));

      if (mounted) setState(() => isLoading = false);

      if (response.toString() != '') {
        if (response.statusCode == 201) {
          chatBoxTC.clear();
          fetchChatData();
        } else {
          throw Exception('Failed to load');
        }
      } else {
        showSnackBar(scaffoldKey, NETWORK_ERROR);
      }
    } on DioError catch (e) {
      showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
    }
  }

  Future closeTicket() async {
    setState(() => isLoading = true);

    try {
      Response response = await Dio().put(
        API_URL_NEW + 'helpdesk/close',
        data: {
          "kode_tiket": "${widget.kodeTiket}",
        },
      );

      if (mounted) setState(() => isLoading = false);

      final json = response.data;

      print("json " + json["data"].toString());

      if (response.toString() != '') {
        if (response.statusCode == 200) {
          Navigator.of(context).pop();
        } else {
          throw Exception('Failed to load');
        }
      } else {
        showSnackBar(scaffoldKey, NETWORK_ERROR);
      }
    } on DioError catch (e) {
      showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
    }
  }

  Widget _buildTextComposer() {
    return isClosed
        ? Padding(
            padding: const EdgeInsets.all(10),
            child: Text('Kasus ini sudah ditutup'),
          )
        : IconTheme(
            data: IconThemeData(color: Theme.of(context).accentColor),
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Row(children: <Widget>[
                Flexible(
                  child: TextField(
                    controller: chatBoxTC,
                    onChanged: (String text) {
                      _isComposing = text.length > 0;
                      setState(() {});
                    },
                    decoration:
                        InputDecoration.collapsed(hintText: "Send a message"),
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 4.0),
                  child: IconButton(
                    icon: Icon(Icons.send),
                    color: Colors.green,
                    onPressed: _isComposing ? () => (sendMessage()) : null,
                  ),
                ),
              ]),
              decoration: BoxDecoration(
                border: Border(
                  top: BorderSide(
                    color: Colors.grey[200],
                  ),
                ),
              ),
            ),
          );
  }
}

class _HelpdeskChatPageView extends StatelessWidget {
  final _HelpdeskChatPageController state;

  const _HelpdeskChatPageView(this.state, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: state.scaffoldKey,
      backgroundColor: Colors.white,
      floatingActionButton: state.data == null
          ? Container()
          : state.isClosed
              ? Container()
              : Padding(
                  padding: EdgeInsets.only(bottom: 50),
                  child: FloatingActionButton.extended(
                    onPressed: state.closeTicket,
                    label: Text('Tutup Kasus'),
                    icon: Icon(Icons.close),
                    backgroundColor: Colors.green,
                  ),
                ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      appBar: AppBar(
        backgroundColor: hexToColor("#344b6b"),
        title: Text("Chat"),
      ),
      body: state.data == null
          ? state.isNotFound
              ? Container(
                  child: Center(
                    child: Text('Tidak ditemukan'),
                  ),
                )
              : loadingScreen()
          : Column(
              children: <Widget>[
                Flexible(
                  child: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: ListView.separated(
                      itemCount: state.data.message.length,
                      itemBuilder: (context, index) {
                        var item = state.data.message[index];
                        return ListTile(
                          title: Text(
                            '${item.sentBy}',
                            style: TextStyle(fontSize: 15),
                          ),
                          subtitle: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 8.0, bottom: 5),
                                child: Text(
                                  item.message,
                                  style: TextStyle(fontSize: 15),
                                ),
                              ),
                              Text(
                                '${DateFormat('dd/MM/yy HH:mm').format(item.createdDate)}',
                                style: TextStyle(fontSize: 12),
                              ),
                            ],
                          ),
                        );
                      },
                      separatorBuilder: (context, index) {
                        return Divider();
                      },
                    ),
                  ),
                ),
                Divider(height: 1.0),
                Container(
                  decoration: BoxDecoration(color: Theme.of(context).cardColor),
                  child: state._buildTextComposer(),
                ),
              ],
            ),
    );
  }
}
