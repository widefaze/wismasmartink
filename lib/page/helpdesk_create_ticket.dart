import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';

class HelpdeskCreateTicket extends StatefulWidget {
  const HelpdeskCreateTicket({Key key}) : super(key: key);

  @override
  _HelpdeskCreateTicketController createState() =>
      _HelpdeskCreateTicketController();
}

class _HelpdeskCreateTicketController extends State<HelpdeskCreateTicket> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isLoading = false;
  File imageFile;
  dynamic pickImageError;
  String retrieveDataError;
  TextEditingController ketTC = new TextEditingController();

  Future createTicket() async {
    setState(() => isLoading = true);

    try {
      Response response = await Dio().post(API_URL_NEW + 'helpdesk/ticket',
          data: FormData.fromMap({
            "id_user": await getSession("user_id"),
            "keterangan": "${ketTC.text}",
          }),
          options: Options(method: 'POST', responseType: ResponseType.json));

      if (mounted) setState(() => isLoading = false);

      final json = response.data;

      print("json " + json["data"].toString());

      if (response.toString() != '') {
        if (response.statusCode == 201) {
          var kodeTiket = json['data']["kode_tiket"];
          Alert(
            context: scaffoldKey.currentContext,
            type: AlertType.info,
            title: '$kodeTiket',
            desc:
                'Pertanyaan sedang ditinjau oleh admin harap menunggu. Simpan dengan baik kode tiket ini',
            buttons: [
              DialogButton(
                child: Text(
                  "OK",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
                onPressed: () async {
                  Navigator.pop(context);
                  Navigator.pop(context);
                },
                width: 120,
              )
            ],
          ).show();
        } else {
          throw Exception('Failed to load');
        }
      } else {
        showSnackBar(scaffoldKey, NETWORK_ERROR);
      }
    } on DioError catch (e) {
      showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
    }
  }

  @override
  Widget build(BuildContext context) => _HelpdeskCreateTicketView(this);
}

class _HelpdeskCreateTicketView extends StatelessWidget {
  final _HelpdeskCreateTicketController state;

  const _HelpdeskCreateTicketView(this.state, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: state.scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: hexToColor("#344b6b"),
          title: Text("Open Ticket"),
        ),
        body: state.isLoading
            ? loadingScreen()
            : SingleChildScrollView(
                child: Container(
                  color: Colors.white,
                  child: Padding(
                    padding: const EdgeInsets.all(25),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Silakan masukkan topik keluhan Anda',
                          style: TextStyle(fontSize: 15),
                        ),
                        SizedBox(
                          height: 20.0,
                        ),
                        TextField(
                          controller: state.ketTC,
                          maxLines: 3,
                          decoration: InputDecoration(
                              contentPadding:
                                  EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                              hintText: "Keterangan",
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(32.0))),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Material(
                          elevation: 5.0,
                          borderRadius: BorderRadius.circular(30.0),
                          color: Colors.green,
                          child: MaterialButton(
                            minWidth:
                                MediaQuery.of(this.state.context).size.width,
                            padding:
                                EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                            onPressed: state.createTicket,
                            child: Text(
                              "SUBMIT",
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ));
  }
}

typedef void OnPickImageCallback(
    double maxWidth, double maxHeight, int quality);
