import 'dart:io';
import 'package:flutter/material.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/utility.dart';
import 'package:wismartlink/page/helpdesk_chat_page.dart';
import 'package:wismartlink/page/helpdesk_create_ticket.dart';

class HelpdeskMainPage extends StatefulWidget {
  const HelpdeskMainPage({Key key}) : super(key: key);

  @override
  _HelpdeskMainPageController createState() => _HelpdeskMainPageController();
}

class _HelpdeskMainPageController extends State<HelpdeskMainPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isLoading = false;
  File imageFile;
  dynamic pickImageError;
  String retrieveDataError;
  TextEditingController kodeTiketTC = new TextEditingController();

  void submit() async {
    if (kodeTiketTC.text.isEmpty) {
      showSnackBar(scaffoldKey, "Kode tiket harap diisi");
    } else {
      Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => HelpdeskChatPage(
                kodeTiket: kodeTiketTC.text,
              )));
    }
  }

  @override
  Widget build(BuildContext context) => _HelpdeskMainPageView(this);
}

class _HelpdeskMainPageView extends StatelessWidget {
  final _HelpdeskMainPageController state;

  const _HelpdeskMainPageView(this.state, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: state.scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: hexToColor("#344b6b"),
          title: Text("Complaint"),
        ),
        body: state.isLoading
            ? loadingScreen()
            : SingleChildScrollView(
                child: Container(
                  color: Colors.white,
                  child: Padding(
                    padding: const EdgeInsets.all(25),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Silakan masukkan kode tiket Anda',
                          style: TextStyle(fontSize: 15),
                        ),
                        SizedBox(
                          height: 20.0,
                        ),
                        TextField(
                          controller: state.kodeTiketTC,
                          decoration: InputDecoration(
                              contentPadding:
                                  EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                              hintText: "Kode Tiket",
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(32.0))),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Material(
                          elevation: 5.0,
                          borderRadius: BorderRadius.circular(30.0),
                          color: Colors.green,
                          child: MaterialButton(
                            minWidth:
                                MediaQuery.of(this.state.context).size.width,
                            padding:
                                EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                            onPressed: state.submit,
                            child: Text(
                              "SUBMIT",
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Material(
                          elevation: 5.0,
                          borderRadius: BorderRadius.circular(30.0),
                          color: Colors.blue,
                          child: MaterialButton(
                            minWidth:
                                MediaQuery.of(this.state.context).size.width,
                            padding:
                                EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                            onPressed: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) =>
                                      HelpdeskCreateTicket()));
                            },
                            child: Text(
                              "BUAT BARU",
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ));
  }
}

typedef void OnPickImageCallback(
    double maxWidth, double maxHeight, int quality);
