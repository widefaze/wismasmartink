import 'dart:io';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';
import 'package:wismartlink/model/menuRoleModel.dart';
import 'package:wismartlink/model/userModel.dart';
import 'package:wismartlink/page/announcement_page.dart';
import 'package:wismartlink/page/chat_contact_page.dart';
import 'package:wismartlink/page/contact_wa.dart';
import 'package:wismartlink/page/emergency_page.dart';
import 'package:wismartlink/page/fasilitas_list_page.dart';
import 'package:wismartlink/page/generic_info_page.dart';
import 'package:wismartlink/page/invoice_history_page.dart';
import 'package:wismartlink/page/market_place_page.dart';
import 'package:wismartlink/page/marketplace/marketplace_page.dart';
import 'package:wismartlink/page/notifikasi_page.dart';
import 'package:wismartlink/page/payment_page.dart';
import 'package:wismartlink/page/profile_page.dart';
import 'package:wismartlink/page/restaurant/restaurant_page.dart';
import 'package:wismartlink/page/scan_barcode_page.dart';
import 'package:wismartlink/page/upload_page.dart';
import 'package:wismartlink/page/voucher_page.dart';

import 'contact_page.dart';
import 'invoice_page.dart';

String roleId;

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageController createState() => _HomePageController();
}

class _HomePageController extends State<HomePage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isLoading = true;
  TextEditingController requestController = new TextEditingController();
  TextEditingController requestDateController = new TextEditingController();

  List<MenuRole> menuRoleList;

  UserModel user;
  final List<String> imgList = [
    'assets/images/slide1.png',
    'assets/images/slide2.png',
    'assets/images/slide3.jpg',
    'assets/images/slide4.png',
    'assets/images/slide5.png',
    'assets/images/slide6.png',
    'assets/images/slide7.png',
  ];

  List<Menu> menus = const [
    const Menu('Invoice', 'assets/icon/billing_invoice.png', 106),
    const Menu('History Invoice', 'assets/icon/History_invoice.png', 119),
    const Menu('Contact', 'assets/icon/Contact.png', 107),
    const Menu('Announcement', 'assets/icon/Announcement.png', 120),
    const Menu('Information', 'assets/icon/Informasi_Pengumuman.png', 103),
    const Menu('Facilities', 'assets/icon/fasilitas.png', 105),
    const Menu('Maintenance', 'assets/icon/manitence.png', 104),
    const Menu('Room Service', 'assets/icon/room_service.png', 108),
    const Menu('Shop', 'assets/icon/shop.png', 109),
    const Menu('Restaurant', 'assets/icon/Restauran.png', 110),
    const Menu('Security', 'assets/icon/Security.png', 113),
    const Menu('Emergency', 'assets/icon/Emergency.png', 101),
    const Menu('Chat', 'assets/icon/Chat.png', 111),
    const Menu('Scan Barcode', 'assets/icon/scan.png', 115),
    const Menu('CCTV', 'assets/icon/CCTV.png', 102),
    const Menu('Complaint', 'assets/icon/Complaint.png', 112),
    // const Menu('Upload', 'assets/icon/Upload.png', 114),
    const Menu('Market Place', 'assets/icon/Market Place.png', 112),
    const Menu('Voucher', 'assets/icon/Voucher.png', 116),
    const Menu('Payment', 'assets/icon/payment.png', 112),
  ];

  Future openApps() async {
    var appId = "com.ezviz";
    var url = "https://play.google.com/store/apps/details?id=$appId";

    if (Platform.isAndroid) {
      try {
        // await AppAvailability.isAppEnabled(appId).then((bool v) async {
        //   if (v) {
        //     AppAvailability.launchApp(appId).catchError(() {
        //       throw 'error';
        //     });
        //   } else {
        //     // if (await canLaunch(url)) {
        //     //   await launch(url);
        //     // } else {
        //     //   throw 'Could not launch $url';
        //     // }
        //   }
        // });

        if (await canLaunch(url)) {
          await launch(url);
        } else {
          throw 'Could not launch $url';
        }
      } catch (e) {}
    }
  }

  Future<List<UserModel>> getUser() async {
    var notes = List<UserModel>();

    SharedPreferences prefs = await SharedPreferences.getInstance();
    roleId = prefs.getString("role");

    Response response;
    var param = {"id_user": await getSession("user_id")};
    response = await Dio().post(API_URL + "user_address",
        data: param,
        options: new Options(
          headers: {"Content-Type": "application/json"},
        ));
    final json = response.data;
    print("json " + json["data"].toString());
    if (response.statusCode == 200) {
      notes = (json["data"])
          .map<UserModel>((item) => UserModel.fromJson(item))
          .toList();
      user = notes[0];
      isLoading = false;
      if (mounted) setState(() {});
    } else {
      throw Exception('Failed to load');
    }
    print("json " + json.toString());
  }

  Future fetchMenuRole() async {
    try {
      Response response;

      response = await Dio().get(
        API_URL_NEW + "role/menu",
      );

      final json = response.data;

      print("json " + json["data"].toString());

      if (response.toString() != '') {
        if (response.statusCode == 200 && json['data'].isNotEmpty) {
          List<MenuRole> list = [];
          json['data'].forEach((v) {
            list.add(menuRoleFromJson(v));
          });
          menuRoleList = list;

          //filter menu list based on role
          List<Menu> temp = menus.where((v) {
            return v.id != null &&
                menuRoleList[v.id - 101].view.contains(roleId);
          }).toList();
          menus = temp;

          if (mounted) setState(() {});
        } else {
          throw Exception('Failed to load');
        }
      } else {
        showSnackBar(scaffoldKey, NETWORK_ERROR);
      }
    } on DioError catch (e) {
      showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
    } catch (e) {
      debugPrint("Error $e");
    } finally {
      if (mounted) setState(() => isLoading = false);
    }
  }

  @override
  void initState() {
    super.initState();
    getUser();
    fetchMenuRole();
  }

  @override
  Widget build(BuildContext context) => _HomePageView(this);
}

class _HomePageView extends StatelessWidget {
  final _HomePageController state;

  const _HomePageView(this.state, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: state.scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: hexToColor("#344b6b"),
          title: Row(
            children: [
              Image.asset(
                'assets/images/wismartlogosmall.png',
                fit: BoxFit.contain,
                height: 25,
              ),
              Spacer(),
              InkWell(
                onTap: () {
                  // Navigator.push(
                  //     state.context,
                  //     MaterialPageRoute(
                  //       builder: (context) => ProfilePage(),
                  //     ));
                },
                child: Icon(
                  Icons.home,
                  color: Colors.white,
                ),
              ),
              SizedBox(width: 10),
              InkWell(
                onTap: () {
                  Navigator.push(
                      state.context,
                      MaterialPageRoute(
                        builder: (context) => ProfilePage(),
                      ));
                },
                child: Icon(
                  Icons.person,
                  color: Colors.white,
                ),
              ),
              SizedBox(width: 10),
              InkWell(
                onTap: () {
                  Navigator.push(
                      state.context,
                      MaterialPageRoute(
                        builder: (context) => NotifikasiPage(),
                      ));
                },
                child: Icon(
                  Icons.notifications,
                  color: Colors.white,
                ),
              ),
            ],
          ),
        ),
        body: state.isLoading
            ? loadingScreen()
            : Container(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Column(
                    children: <Widget>[
                      Card(
                        margin: EdgeInsets.all(10),
                        child: Padding(
                          padding: const EdgeInsets.all(10),
                          child: Row(
                            children: <Widget>[
                              Text(state.user?.namaApt ?? ''),
                              Spacer(),
                              Text(state.user?.nomor ?? ''),
                            ],
                          ),
                        ),
                      ),
                      CarouselSlider(
                        height: 150,
                        autoPlay: true,
                        enableInfiniteScroll: true,
                        viewportFraction: 1.0,
                        enlargeCenterPage: true,
                        aspectRatio:
                            MediaQuery.of(state.context).size.aspectRatio,
                        items: state.imgList.map(
                          (url) {
                            return Container(
                              child: ClipRRect(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(0.0)),
                                child: Image(image: AssetImage(url)),
                              ),
                            );
                          },
                        ).toList(),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Expanded(
                        child: Container(
                          height: MediaQuery.of(context).size.height - 415,
                          child: GridView.custom(
                            shrinkWrap: true,
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 4,
                              childAspectRatio: 35 / 51,
                              mainAxisSpacing: 0,
                              crossAxisSpacing: 10,
                            ),
                            childrenDelegate: SliverChildListDelegate(
                              List.generate(state.menus.length, (index) {
                                return InkWell(
                                    onTap: () {
                                      getGridViewSelectedItem(
                                          state.menus[index], context);
                                    },
                                    child: Center(
                                      child: MenuCard(menu: state.menus[index]),
                                    ));
                              }),
                            ),
                          ),
                        ),
                      ),
                      // Container(
                      //   height: MediaQuery.of(context).size.height - 500,
                      //   child: GridView.count(
                      //       crossAxisCount: 4,
                      //       children:
                      //           List.generate(state.menus.length, (index) {
                      //         return Container(
                      //           height: 500,
                      //           color: Colors.red,
                      //           child: InkWell(
                      //               onTap: () {
                      //                 getGridViewSelectedItem(
                      //                     state.menus[index], context);
                      //               },
                      //               child: Center(
                      //                 child:
                      //                     MenuCard(menu: state.menus[index]),
                      //               )),
                      //         );
                      //       })),
                      // ),
                      Container(
                        height: 100,
                        width: MediaQuery.of(context).size.width,
                        child: Row(
                          children: <Widget>[
                            Expanded(
                                child: Image.asset('assets/images/iklan1.jpg')),
                            Expanded(
                                child: Image.asset('assets/images/iklan2.jpg'))
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ));
  }

  getGridViewSelectedItem(Menu menu, BuildContext context) {
    StatefulWidget widget;

    if (menu.title == "Emergency") {
      widget = EmergencyPage();
    } else if (menu.title == "CCTV") {
      state.openApps();
    } else if (menu.title == "Payment") {
      widget = PaymentPage();
    } else if (menu.title == "Market Place") {
      widget = MarketPlaceKatalogPage();
    } else if (menu.title == "Information") {
      widget = GenericInfoPage();
    } else if (menu.title == "Announcement") {
      widget = AnnouncementPage(menuRole: state.menuRoleList[120 - 101]);
    } else if (menu.title == "Maintenance") {
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ContactWAPage(
              type: Type.maintenance,
            ),
          ));
      // widget = RequestMaintenancePage(menuRole: state.menuRoleList[104 - 101]);
    } else if (menu.title == "Facilities") {
      // widget = RequestFasilitasPage(menuRole: state.menuRoleList[105 - 101]);
      widget = FasilitasListPage();
    } else if (menu.title == "History Invoice") {
      widget = HistoryInvoicePage();
    } else if (menu.title == "Invoice") {
      widget = InvoicePage();
    } else if (menu.title == "Contact") {
      widget = ContactPage();
    } else if (menu.title == "Room Service") {
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ContactWAPage(
              type: Type.roomservice,
            ),
          ));
      // widget = RequestRoomServicePage(menuRole: state.menuRoleList[108 - 101]);
    } else if (menu.title == "Shop") {
      widget = MarketPlacePage();
    } else if (menu.title == "Restaurant") {
      widget = RestaurantPage();
    } else if (menu.title == "Chat") {
      widget = ChatContactPage(menuRole: state.menuRoleList[111 - 101]);
    } else if (menu.title == "Complaint") {
      widget = UploadPage();

      // widget = HelpdeskMainPage();
    } else if (menu.title == "Scan Barcode") {
      widget = ScanBarcode();
    } else if (menu.title == "Upload") {
      widget = UploadPage();
    } else if (menu.title == "Security") {
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ContactWAPage(
              type: Type.security,
            ),
          ));
      // widget = ChatContactPage(
      //     isSecurity: true, menuRole: state.menuRoleList[113 - 101]);
    } else if (menu.title == "Voucher") {
      widget = VoucherPage();
    }
    if (widget != null) {
      Navigator.push(
          state.context,
          MaterialPageRoute(
            builder: (context) => widget,
          ));
    }
  }
}

class Menu {
  const Menu(this.title, this.icon, [this.id]);

  final String title;
  final String icon;
  final int id;
}

class MenuCard extends StatelessWidget {
  const MenuCard({Key key, this.menu}) : super(key: key);

  final Menu menu;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Center(
        child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.all(5),
                    child: Image.asset(
                      menu.icon,
                      fit: BoxFit.contain,
                      height: 10,
                    ),
                  )),
              SizedBox(
                height: 3.0,
              ),
              Expanded(
                flex: 1,
                child: Text(menu.title,
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center),
              ),
            ]),
      ),
    );
  }
}
