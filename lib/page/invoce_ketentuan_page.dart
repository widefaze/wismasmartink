import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';
import 'package:html/dom.dart' as dom;

class InvoiceKetentuanPage extends StatefulWidget {
  InvoiceKetentuanPage({Key key}) : super(key: key);

  @override
  _InvoiceKetentuanPageState createState() => _InvoiceKetentuanPageState();
}

class _InvoiceKetentuanPageState extends State<InvoiceKetentuanPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  bool isLoading = false;
  String data;

  void getData() async {
    setState(() => isLoading = true);

    try {
      Response response = await new Dio().get(API_URL_NEW + 'invoice/ketentuan',
          options: Options(responseType: ResponseType.json));

      setState(() => isLoading = false);

      if (response.toString() != '') {
        setState(() {
          data = response.data['data'];
        });
      } else {
        showSnackBar(scaffoldKey, NETWORK_ERROR);
      }
    } on DioError catch (e) {
      showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
    }
  }

  @override
  void initState() {
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        backgroundColor: hexToColor("#344b6b"),
        title: Text('Ketentuan'),
      ),
      body: isLoading
          ? loadingScreen()
          : Html(
              data: """${data}""",
              //Optional parameters:
              padding: EdgeInsets.all(8.0),
              linkStyle: const TextStyle(
                color: Colors.redAccent,
                decorationColor: Colors.redAccent,
                decoration: TextDecoration.underline,
              ),

              //Must have useRichText set to false for this to work
              customRender: (node, children) {
                if (node is dom.Element) {
                  switch (node.localName) {
                    case "br":
                      return Column(children: children);
                  }
                }
                return null;
              },
              customTextAlign: (dom.Node node) {
                if (node is dom.Element) {
                  switch (node.className) {
                    case "ql-align-right":
                      return TextAlign.right;
                    default:
                      return TextAlign.justify;
                  }
                }
                return null;
              },
              customTextStyle: (dom.Node node, TextStyle baseStyle) {
                if (node is dom.Element) {
                  switch (node.localName) {
                    case "p":
                      return baseStyle.merge(
                        TextStyle(
                          height: 1.5,
                          fontSize: 18,
                        ),
                      );
                    case "br":
                      return baseStyle.merge(
                        TextStyle(
                          height: 0,
                          fontSize: 0,
                        ),
                      );
                  }
                }
                return baseStyle;
              },
            ),
    );
  }
}
