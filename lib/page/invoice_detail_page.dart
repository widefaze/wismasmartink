import 'dart:convert';
import 'dart:ui';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';
import 'package:wismartlink/model/invoiceModel.dart';
import 'package:wismartlink/page/invoce_ketentuan_page.dart';

class InvoiceDetailpage extends StatefulWidget {
  final String invoiceNumber;
  final total;
  final bool isPaid;

  const InvoiceDetailpage(
      {Key key, this.invoiceNumber, this.total, this.isPaid})
      : super(key: key);

  @override
  _InvoiceDetailpageState createState() => _InvoiceDetailpageState();
}

class _InvoiceDetailpageState extends State<InvoiceDetailpage> {
  bool isLoading = true;
  Invoice data;

  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    getData();
  }

  void getData() async {
    setState(() => isLoading = true);

    try {
      Response response = await new Dio().post(API_URL_NEW + 'invoice/detail',
          data: new FormData.fromMap({
            "id_user": await getSession("user_id"),
            "nomor_invoice": widget.invoiceNumber,
            "total": widget.total,
          }),
          options: Options(method: 'POST', responseType: ResponseType.json));

      print(response.data['data']);

      setState(() => isLoading = false);

      if (response.toString() != '') {
        setState(() {
          data = invoiceFromJson(jsonEncode(response.data['data']));
        });
      } else {
        showSnackBar(scaffoldKey, NETWORK_ERROR);
      }
    } on DioError catch (e) {
      showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
    }
  }

  @override
  Widget build(BuildContext context) {
    var textStyle = TextStyle(
      fontSize: 16.0,
      fontWeight: FontWeight.bold,
      color: Colors.blue[900],
    );
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: hexToColor("#344b6b"),
        title: Text('Detail Invoice'),
      ),
      body: isLoading
          ? loadingScreen()
          : Stack(
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 55),
                  child: ListView(
                    padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
                    children: [
                      MyCard(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: [
                                Expanded(
                                  child: Text(
                                    "No invoice : ${data.nomorInvoice}",
                                    textAlign: TextAlign.left,
                                    style: textStyle,
                                  ),
                                ),
                              ],
                            ),
                            Divider(height: 20),
                            Row(
                              children: [
                                Expanded(child: Text('Periode Tagihan: ')),
                                Text(
                                  '${DateFormat('MMMM yyyy').format(DateTime.now())}',
                                ),
                              ],
                            ),
                            SizedBox(height: 2.5),
                            Row(
                              children: [
                                Expanded(child: Text('Tgl. Jatuh Tempo: ')),
                                Text(
                                  '${DateFormat('dd/mm/yyyy').format(DateTime.now())}',
                                ),
                              ],
                            ),
                            SizedBox(height: 2.5),
                            Row(
                              children: [
                                Expanded(child: Text('Tanggal BAST: ')),
                                Text(
                                  '${DateFormat('dd/mm/yyyy').format(DateTime.now())}',
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      MyCard(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: [
                                Expanded(
                                  child: Text(
                                    '${data.namaUser}',
                                    textAlign: TextAlign.left,
                                    style: textStyle,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: 10),
                            Text(
                                '${data.namaApartemen}, ${data.namaGedung}, ${data.namaUnit}\nLantai : ${data.lantai}, Nomor : ${data.nomor}, \n${data.alamat}'),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(15),
                        color: widget.isPaid
                            ? Colors.greenAccent[100]
                            : Colors.redAccent[100],
                        child: Text(
                          "${widget.isPaid ? 'Lunas' : 'Belum Lunas'}",
                          textAlign: TextAlign.center,
                          style: textStyle.copyWith(
                            color: widget.isPaid ? Colors.green : Colors.red,
                            fontSize: 18,
                          ),
                        ),
                      ),
                      SizedBox(height: 15),
                      MyCard(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Detail Tagihan",
                              textAlign: TextAlign.center,
                              style: textStyle,
                            ),
                            Divider(height: 20),
                            ListTile(
                              title: Text('Keamanan'),
                              trailing: Text(
                                data.biayaKeamanan ?? '-',
                                style: TextStyle(
                                  color: Colors.red,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            ListTile(
                              title: Text('Kebersihan'),
                              trailing: Text(
                                data.biayaKebersihan ?? '-',
                                style: TextStyle(
                                  color: Colors.red,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            ListTile(
                              title: Text('Air'),
                              trailing: Text(
                                data.pdam.tagihanPdam ?? '-',
                                style: TextStyle(
                                  color: Colors.red,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            Container(
                              color: Colors.grey[100],
                              width: double.maxFinite,
                              margin: EdgeInsets.only(left: 15, right: 15),
                              padding: EdgeInsets.all(10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Meteran Awal: ${data.pdam.meteranAwal} m3',
                                    style: TextStyle(
                                      color: Colors.grey,
                                    ),
                                  ),
                                  Text(
                                    'Meteran Akhir: ${data.pdam.meteranAkhir} m3',
                                    style: TextStyle(
                                      color: Colors.grey,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            ListTile(
                              title: Text('Listrik'),
                              trailing: Text(
                                data.listrik.tagihanListrik ?? '-',
                                style: TextStyle(
                                  color: Colors.red,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            Container(
                              color: Colors.grey[100],
                              width: double.maxFinite,
                              margin: EdgeInsets.only(left: 15, right: 15),
                              padding: EdgeInsets.all(10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Meteran Awal: ${data.listrik.meteranAwal}',
                                    style: TextStyle(
                                      color: Colors.grey,
                                    ),
                                  ),
                                  Text(
                                    'Meteran Akhir: ${data.listrik.meteranAkhir}',
                                    style: TextStyle(
                                      color: Colors.grey,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            ListTile(
                              title: Text('Biaya Admin'),
                              trailing: Text(
                                data.biayaAdmin ?? '-',
                                style: TextStyle(
                                  color: Colors.red,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            ListTile(
                              title: Text('Sinking Fund'),
                              trailing: Text(
                                data.sinkingFund ?? '-',
                                style: TextStyle(
                                  color: Colors.red,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            ListTile(
                              title: Text('Biaya Operasional'),
                              trailing: Text(
                                data.biayaOperasional ?? '-',
                                style: TextStyle(
                                  color: Colors.red,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            ListTile(
                              title: Text('Service Charge'),
                              trailing: Text(
                                data.biayaService ?? '-',
                                style: TextStyle(
                                  color: Colors.red,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        color: Colors.red[100],
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            ListTile(
                              title: Text(
                                'TOTAL',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              trailing: Text(
                                data.grandTotal ?? '0', //TODO: fix this
                                style: TextStyle(
                                  fontWeight: FontWeight.w900,
                                  fontSize: 18,
                                  color: Colors.red,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 15),
                      FlatButton(
                        color: Colors.black,
                        padding: EdgeInsets.all(15),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5)),
                        child: Text(
                          'Ketentuan - ketentuan',
                          style: TextStyle(color: Colors.white, fontSize: 18),
                        ),
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (_) => InvoiceKetentuanPage()));
                        },
                      ),
                      SizedBox(height: 10),
                    ],
                  ),
                ),
                Positioned(
                  bottom: 0,
                  left: 0,
                  height: 55,
                  width: MediaQuery.of(context).size.width,
                  child: FlatButton(
                    color: Colors.blue[400],
                    child: Text(
                      'Download Invoice',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                      ),
                    ),
                    onPressed: () async {
                      var url =
                          "${API_URL_NEW}invoice/cetak_invoice?nomor=${widget.invoiceNumber}";

                      await canLaunch(url)
                          ? launch(url)
                          : showSnackBar(scaffoldKey, 'Error!');
                    },
                  ),
                ),
              ],
            ),
    );
  }
}

class MyCard extends StatelessWidget {
  const MyCard({
    Key key,
    @required this.child,
    this.color,
  }) : super(key: key);

  final Widget child;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(20.0),
        margin: EdgeInsets.only(bottom: 10),
        decoration: BoxDecoration(
          color: color ?? Colors.white,
          border: Border.all(
            color: Colors.grey[300],
          ),
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          // boxShadow: <BoxShadow>[
          //   new BoxShadow(
          //     color: Colors.black12,
          //     blurRadius: 10.0,
          //     offset: new Offset(0.0, 10.0),
          //   ),
          // ],
        ),
        child: child);
  }
}
