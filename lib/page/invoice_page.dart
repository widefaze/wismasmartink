import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:wismartlink/component/component.dart';

import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';
import 'package:wismartlink/page/invoice_detail_page.dart';

enum TabValue { all, paid, unpaid }

class InvoicePage extends StatefulWidget {
  const InvoicePage({Key key}) : super(key: key);

  @override
  _InvoicePageController createState() => _InvoicePageController();
}

class _InvoicePageController extends State<InvoicePage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isLoading = false;
  List data;

  var selectedTab = TabValue.all;

  @override
  void initState() {
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) => _InvoicePageView(this);

  void getData() async {
    setState(() => isLoading = true);

    try {
      Response response = await new Dio().post(API_URL + 'history_invoice',
          data: new FormData.fromMap({"id_user": await getSession("user_id")}),
          options: Options(method: 'POST', responseType: ResponseType.json));

      setState(() => isLoading = false);

      if (response.toString() != '') {
        setState(() {
          data = response.data['data'];
        });
      } else {
        showSnackBar(scaffoldKey, NETWORK_ERROR);
      }
    } on DioError catch (e) {
      showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
    }
  }
}

class _InvoicePageView extends StatefulWidget {
  final _InvoicePageController state;

  const _InvoicePageView(this.state, {Key key}) : super(key: key);

  @override
  __InvoicePageViewState createState() => __InvoicePageViewState();
}

class __InvoicePageViewState extends State<_InvoicePageView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: widget.state.scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: hexToColor("#344b6b"),
          title: Text("Invoice"),
        ),
        body: widget.state.isLoading
            ? loadingScreen()
            : Column(
                children: <Widget>[
                  // Container(
                  //   padding:
                  //       const EdgeInsets.only(top: 20, left: 5, bottom: 10),
                  //   child: Row(
                  //     children: <Widget>[
                  //       MyTabBar(
                  //         text: 'All',
                  //         value: TabValue.all,
                  //         groupValue: widget.state.selectedTab,
                  //         onPressed: (value) {
                  //           setState(() {
                  //             widget.state.selectedTab = value;
                  //           });
                  //         },
                  //       ),
                  //       MyTabBar(
                  //         text: 'Lunas',
                  //         value: TabValue.paid,
                  //         groupValue: widget.state.selectedTab,
                  //         onPressed: (value) {
                  //           setState(() {
                  //             widget.state.selectedTab = value;
                  //           });
                  //         },
                  //       ),
                  //       MyTabBar(
                  //         text: 'Belum Lunas',
                  //         value: TabValue.unpaid,
                  //         groupValue: widget.state.selectedTab,
                  //         onPressed: (value) {
                  //           setState(() {
                  //             widget.state.selectedTab = value;
                  //           });
                  //         },
                  //       ),
                  //     ],
                  //   ),
                  // ),
                  Expanded(
                    child: ListView.separated(
                      itemCount: widget.state?.data?.length ?? 0,
                      itemBuilder: (context, index) {
                        return (widget.state.selectedTab == TabValue.paid &&
                                    widget.state.data[index]['is_paid'] ==
                                        'Lunas') ||
                                (widget.state.selectedTab == TabValue.unpaid &&
                                    widget.state.data[index]['is_paid'] ==
                                        'Belum Lunas') ||
                                widget.state.selectedTab == TabValue.all
                            ? GestureDetector(
                                onTap: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (_) => InvoiceDetailpage(
                                            invoiceNumber: widget.state
                                                .data[index]['no_invoice'],
                                            total: widget.state.data[index]
                                                    ['total'] ??
                                                '',
                                            isPaid: widget.state.data[index]
                                                    ['is_paid'] ==
                                                'Lunas',
                                          )));
                                },
                                child: Container(
                                  padding: const EdgeInsets.all(10),
                                  color: Colors.white,
                                  child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                                widget.state.data[index]
                                                    ['no_invoice'],
                                                style: TextStyle(fontSize: 16)),
                                            SizedBox(height: 3),
                                            Text(
                                                widget.state.data[index]
                                                    ['invoice_date'],
                                                style: TextStyle(
                                                    color: Colors.black45))
                                          ],
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: <Widget>[
                                            Text(
                                                widget.state.data[index]
                                                    ['is_paid'],
                                                style: TextStyle(
                                                    color: Colors.black45)),
                                            SizedBox(height: 3),
                                            Text(
                                                widget.state.data[index]
                                                        ['total'] ??
                                                    '',
                                                style: TextStyle(
                                                    fontSize: 18,
                                                    color: Colors.green))
                                          ],
                                        )
                                      ]),
                                ),
                              )
                            : Container();
                      },
                      separatorBuilder: (context, index) {
                        return Divider();
                      },
                    ),
                  ),
                ],
              ));
  }
}
