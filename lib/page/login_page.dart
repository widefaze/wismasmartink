import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';

import 'main_page.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageController createState() => _LoginPageController();
}

class _LoginPageController extends State<LoginPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  bool obscureText = true;
  bool isLoading = false;
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  String emailError, passwordError;

  @override
  void initState() {
    super.initState();

//    emailController.text = "admin@gmail.com";
    emailController.text = "wahyuartadigital@gmail.com";
    passwordController.text = "123456789";
  }

  @override
  Widget build(BuildContext context) => _LoginPageView(this);

  void togglePassword() => setState(() => obscureText = !obscureText);

  void loginProcess() async {
    if (emailController.text.isEmpty) {
      showSnackBar(scaffoldKey, "Email is required !");
    } else if (passwordController.text.isEmpty) {
      showSnackBar(scaffoldKey, "Password is required !");
    } else {
      setState(() => isLoading = true);

      var email = emailController.text;
      var password = passwordController.text;
      FormData formdata = new FormData.fromMap({
        "email": email,
        "password": password,
      });
      try {
        Response response = await new Dio().post(API_URL + 'login',
            data: formdata,
            options: Options(
                headers: {"Content-Type": "application/json"},
                method: 'POST',
                responseType: ResponseType.json));

        setState(() => isLoading = false);

        if (response.toString() != '') {
          if (response.data['success'] == true) {
            SharedPreferences prefs = await SharedPreferences.getInstance();
            prefs.setString("role", response.data['data'][0]['role']);
            prefs.setString("user_id", response.data['data'][0]['user_id']);
            prefs.setString("nama", response.data['data'][0]['nama']);
            prefs.setString("email", response.data['data'][0]['email']);
            prefs.setString("username", response.data['data'][0]['username']);
            prefs.setString(
                "phone_number", response.data['data'][0]['phone_number']);
            if (response.data['data'][0]['id_toko'] != '')
              prefs.setString("id_toko", response.data['data'][0]['id_toko']);
            if (response.data['data'][0]['id_resto'] != '')
              prefs.setString("id_resto", response.data['data'][0]['id_resto']);

            Navigator.pushReplacement(
                context, MaterialPageRoute(builder: (context) => MainPage()));
          } else {
            showSnackBar(scaffoldKey, response.data['message']);
          }
        } else {
          showSnackBar(scaffoldKey, NETWORK_ERROR);
        }
      } on DioError catch (e) {
        showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
      }
    }
  }
}

class _LoginPageView extends StatelessWidget {
  final _LoginPageController state;

  const _LoginPageView(this.state, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: state.scaffoldKey,
        backgroundColor: Colors.white,
        body: state.isLoading
            ? loadingScreen()
            : Container(
                decoration: new BoxDecoration(
                    image: new DecorationImage(
                        image: new ExactAssetImage('assets/images/bglogin.jpg'),
                        fit: BoxFit.cover)),
                child: Center(
                  child: new SingleChildScrollView(
                    child: Container(
                      child: Padding(
                        padding: const EdgeInsets.all(25),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            SizedBox(
                              height: 120.0,
                              child: Image.asset(
                                "assets/images/wismartlogo.png",
                                fit: BoxFit.contain,
                              ),
                            ),
                            SizedBox(height: 100.0),
                            TextField(
                              style: TextStyle(color: Colors.white),
                              cursorColor: Colors.white,
                              controller: state.emailController,
                              obscureText: false,
                              decoration: InputDecoration(
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(32.0),
                                    borderSide: const BorderSide(
                                        color: Colors.white, width: 0.0),
                                  ),
                                  hintStyle: TextStyle(color: Colors.white60),
                                  contentPadding: EdgeInsets.fromLTRB(
                                      20.0, 15.0, 20.0, 15.0),
                                  hintText: "Email",
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(32.0),
                                  )),
                            ),
                            SizedBox(height: 25.0),
                            TextField(
                              style: TextStyle(color: Colors.white),
                              controller: state.passwordController,
                              obscureText: state.obscureText,
                              decoration: InputDecoration(
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(32.0),
                                    borderSide: const BorderSide(
                                        color: Colors.white, width: 0.0),
                                  ),
                                  hintStyle: TextStyle(color: Colors.white60),
                                  contentPadding: EdgeInsets.fromLTRB(
                                      20.0, 15.0, 20.0, 15.0),
                                  hintText: "Password",
                                  suffixIcon: IconButton(
                                      icon: Icon(Icons.remove_red_eye,
                                          color: Colors.white),
                                      onPressed: state.togglePassword),
                                  border: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.circular(32.0))),
                            ),
                            SizedBox(
                              height: 35.0,
                            ),
                            Material(
                              elevation: 5.0,
                              borderRadius: BorderRadius.circular(30.0),
                              color: Colors.green.withOpacity(0.8),
                              child: MaterialButton(
                                minWidth: MediaQuery.of(this.state.context)
                                    .size
                                    .width,
                                padding:
                                    EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                                onPressed: state.loginProcess,
                                child: Text(
                                  "LOGIN",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 15.0,
                            ),
                            InkWell(
                                onTap: () {
//                        _launchURL();
                                },
                                child: Text(
                                  "Forgot Password",
                                  textAlign: TextAlign.left,
                                  style: TextStyle(color: Colors.white),
                                )),
                            SizedBox(
                              height: 25.0,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ));
  }
}
