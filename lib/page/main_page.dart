import 'package:flutter/material.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/page/profile_page.dart';

import 'home_page.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key key}) : super(key: key);

  @override
  _MainPageController createState() => _MainPageController();
}

class _MainPageController extends State<MainPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  int selectedBottomNavigation = 0;

  final bottomNavigationPage = [HomePage(), ProfilePage()];
  bool isLoading = true;

  String message;

  @override
  void initState() {
    super.initState();
  }

  void _onItemTapped(int index) {
    setState(() {
      selectedBottomNavigation = index;
    });
  }

  @override
  Widget build(BuildContext context) => _MainPageView(this);
}

class _MainPageView extends StatelessWidget {
  final _MainPageController state;

  const _MainPageView(this.state, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return state.isLoading
        ? Scaffold(
            body: loadingScreen(),
          )
        : state.message != null
            ? Scaffold(
                body: Container(
                  padding: EdgeInsets.all(25),
                  alignment: Alignment.center,
                  child: Text(
                    '${state.message}',
                    textAlign: TextAlign.center,
                  ),
                ),
              )
            : Scaffold(
                body: state.bottomNavigationPage
                    .elementAt(state.selectedBottomNavigation),
                // bottomNavigationBar: BottomNavigationBar(
                //   items: [
                //     BottomNavigationBarItem(
                //       icon: Icon(Icons.home),
                //       title: Text('Home'),
                //     ),
                //     BottomNavigationBarItem(
                //       icon: Icon(Icons.account_circle),
                //       title: Text('Profile'),
                //     ),
                //   ],
                //   type: BottomNavigationBarType.fixed,
                //   currentIndex: state.selectedBottomNavigation,
                //   fixedColor: hexToColor("#344b6b"),
                //   onTap: state._onItemTapped,
                // ),
              );
  }
}
