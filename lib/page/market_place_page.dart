import 'package:flutter/material.dart';
import 'package:wismartlink/helper/utility.dart';

class MarketPlaceKatalogPage extends StatefulWidget {
  MarketPlaceKatalogPage({Key key}) : super(key: key);

  @override
  _MarketPlaceKatalogPageState createState() => _MarketPlaceKatalogPageState();
}

class _MarketPlaceKatalogPageState extends State<MarketPlaceKatalogPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: hexToColor("#344b6b"),
        title: Text("Market Place"),
      ),
      body: Image.asset('assets/images/marketplace.png'),
    );
  }
}
