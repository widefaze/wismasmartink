// import 'package:dio/dio.dart';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
import 'package:wismartlink/helper/utility.dart';
import 'package:wismartlink/model/cartModel.dart';
import 'package:wismartlink/page/marketplace/marketplace_page.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:wismartlink/page/restaurant/cartFood.dart';

class CartSelect extends StatefulWidget {
  const CartSelect(
      this.counter, this.counter_resto, this.subttl, this.subttlMakanan);
  final String counter, counter_resto, subttl, subttlMakanan;
  @override
  _CartSelectController createState() => _CartSelectController();
}

class _CartSelectController extends State<CartSelect> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  var id = '';
  final _formKey = GlobalKey<FormState>();
  // you must keep track of the TextEditingControllers if you want the values to persist correctly
  List<TextEditingController> controllers = <TextEditingController>[];
  TextEditingController namaBarangController = TextEditingController();
  TextEditingController hargaController = TextEditingController();
  TextEditingController keteranganController = TextEditingController();
  bool isLoading = false;
  dynamic pickImageError;
  Future<List<CartModel>> _listFutureCart;
  String _subtotal = "";
  String retrieveDataError;
  File _imageFile;
  int _itemCount = 0;
  int counterFood = 0;
  int counterShop = 0;

  void showToast(String msg, {int duration, int gravity}) {
    Toast.show(msg, context, duration: Toast.LENGTH_LONG, gravity: gravity);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: hexToColor("#344b6b"),
          title: Text("Shopping Cart"),
          elevation: 0,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () =>
                Navigator.of(context).pushReplacement(new MaterialPageRoute(
              builder: (BuildContext context) => new MarketPlacePage(),
              // new Notif(),
            )),
          )),
      body: ListView(
        children: <Widget>[
          InkWell(
            onTap: () {
              Navigator.of(context).push(new MaterialPageRoute(
                builder: (BuildContext context) =>
                    new CartFood(widget.counter, widget.subttl),
              ));
            },
            child: Padding(
              padding: EdgeInsets.symmetric(
                vertical: 20.0,
                horizontal: 30.0,
              ),
              child: InkWell(
                onTap: () {
                  Navigator.of(context).push(new MaterialPageRoute(
                    builder: (BuildContext context) =>
                        new CartFood(widget.counter, widget.subttl),
                  ));
                },
                child: Material(
                  color: Hexcolor("#344b6b"),
                  elevation: 10.0,
                  borderRadius: BorderRadius.circular(25.0),
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.symmetric(
                            vertical: 20.0,
                          ),
                          child: Material(
                            elevation: 5.0,
                            borderRadius: BorderRadius.circular(100.0),
                            child: Container(
                              // changing from 200 to 150 as to look better
                              height: 90.0,
                              width: 90.0,
                              child: ClipOval(
                                child: Row(
                                  children: <Widget>[
                                    Image(
                                      fit: BoxFit.cover,
                                      image: AssetImage(
                                        'assets/icon/restaurant.png',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                        Center(
                          child: Column(
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    "Food",
                                    style: TextStyle(
                                      fontSize: 20.0,
                                      color: Colors.white,
                                      fontFamily: "Quando",
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                  widget.counter_resto != 0
                                      ? new Positioned(
                                          right: 11,
                                          top: 11,
                                          child: new Container(
                                            padding: EdgeInsets.all(2),
                                            decoration: new BoxDecoration(
                                              color: Colors.red,
                                              borderRadius:
                                                  BorderRadius.circular(6),
                                            ),
                                            constraints: BoxConstraints(
                                              minWidth: 20,
                                              minHeight: 20,
                                            ),
                                            child: Text(
                                              widget.counter_resto,
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 12,
                                              ),
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                        )
                                      : new Container()
                                ],
                              ),
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.all(20.0),
                          child: Text(
                            "Tap Here",
                            style: TextStyle(
                                fontSize: 18.0,
                                color: Colors.white,
                                fontFamily: "Alike"),
                            maxLines: 5,
                            textAlign: TextAlign.justify,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
          InkWell(
            onTap: () {
              // Navigator.of(context).push(new MaterialPageRoute(
              //   builder: (BuildContext context) =>
              //       new Cart(widget.counter, widget.counter_resto, widget.subttl, widget.subttlMakanan),
              // ));
            },
            child: Padding(
              padding: EdgeInsets.symmetric(
                vertical: 20.0,
                horizontal: 30.0,
              ),
              child: InkWell(
                onTap: () {
                  // Navigator.of(context).push(new MaterialPageRoute(
                  //   builder: (BuildContext context) =>
                  //       new Cart(widget.counter, widget.counter_resto, widget.subttl, widget.subttlMakanan),
                  // ));
                },
                child: Material(
                  color: Hexcolor("#344b6b"),
                  elevation: 10.0,
                  borderRadius: BorderRadius.circular(25.0),
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.symmetric(
                            vertical: 20.0,
                          ),
                          child: Material(
                            elevation: 5.0,
                            borderRadius: BorderRadius.circular(100.0),
                            child: Container(
                              // changing from 200 to 150 as to look better
                              height: 90.0,
                              width: 90.0,
                              child: ClipOval(
                                child: Row(
                                  children: <Widget>[
                                    Image(
                                      fit: BoxFit.cover,
                                      image: AssetImage(
                                        'assets/icon/marketplace.png',
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                        Center(
                          child: Column(
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    "Shop",
                                    style: TextStyle(
                                      fontSize: 20.0,
                                      color: Colors.white,
                                      fontFamily: "Quando",
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                  widget.counter != 0
                                      ? new Positioned(
                                          right: 11,
                                          top: 11,
                                          child: new Container(
                                            padding: EdgeInsets.all(2),
                                            decoration: new BoxDecoration(
                                              color: Colors.red,
                                              borderRadius:
                                                  BorderRadius.circular(6),
                                            ),
                                            constraints: BoxConstraints(
                                              minWidth: 20,
                                              minHeight: 20,
                                            ),
                                            child: Text(
                                              widget.counter,
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 12,
                                              ),
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                        )
                                      : new Container()
                                ],
                              ),
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.all(20.0),
                          child: Text(
                            "Tap Here",
                            style: TextStyle(
                                fontSize: 18.0,
                                color: Colors.white,
                                fontFamily: "Alike"),
                            maxLines: 5,
                            textAlign: TextAlign.justify,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
