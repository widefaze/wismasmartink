import 'package:dio/dio.dart';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';
import 'package:wismartlink/model/cartModel.dart';
import 'package:wismartlink/model/userModel.dart';
import 'package:wismartlink/page/marketplace/cart.dart';
import 'package:wismartlink/page/marketplace/pembayaran.dart';
import 'package:wismartlink/page/restaurant/cartFood.dart';

class Checkout extends StatefulWidget {
  const Checkout(this.counter, this.subttl, this.nomorUnik, this.type,
      {this.data});
  final String counter, subttl, nomorUnik, type;
  //type = fasilitas, service, toko, resto
  final Map data;
  @override
  _CheckoutController createState() => _CheckoutController();
}

class _CheckoutController extends State<Checkout> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  var id = '';
  final _formKey = GlobalKey<FormState>();
  // you must keep track of the TextEditingControllers if you want the values to persist correctly
  List<TextEditingController> controllers = <TextEditingController>[];
  TextEditingController namaBarangController = TextEditingController();
  TextEditingController hargaController = TextEditingController();
  TextEditingController keteranganController = TextEditingController();
  bool isLoading = false;
  dynamic pickImageError;
  Future<List<UserModel>> _listFutureUser;
  Future<List<CartModel>> _listFutureCart;
  String _subtotal = "";
  String retrieveDataError;
  File _imageFile;
  int _itemCount = 0;
  int _grandtotal = 0;
  String _nama_unit = "";
  String _nomor;
  String _lantai;
  String _nama_gedung;
  String _alamat;
  String _kota;
  String _nama_apt;
  String api_func;

  void showToast(String msg, {int duration, int gravity}) {
    Toast.show(msg, context, duration: Toast.LENGTH_LONG, gravity: gravity);
  }

  Future<List<UserModel>> getUser() async {
    var notes = List<UserModel>();

    Response response;
    var param = {"id_user": await getSession("user_id")};
    response = await Dio().post(API_URL + "user_address",
        data: param,
        options: new Options(
          headers: {"Content-Type": "application/json"},
        ));
    final json = response.data;
    print("json " + json["data"].toString());
    if (response.statusCode == 200) {
      notes = (json["data"])
          .map<UserModel>((item) => UserModel.fromJson(item))
          .toList();
    } else {
      throw Exception('Failed to load');
    }
    print("json " + json.toString());
    return new Future.delayed(new Duration(seconds: 1), () {
      return notes.where((i) => i.idUser != "").toList();
    });
  }

  Future<List<CartModel>> fetchCart() async {
    var notes = List<CartModel>();
    if (widget.type == 'toko') {
      api_func = 'list_transaksi_toko_belum_lunas';
    } else {
      api_func = 'list_transaksi_resto_belum_lunas';
    }
    Response response;
    var param = {"id_user": await getSession("user_id")};
    response = await Dio().post(API_URL + api_func,
        data: param,
        options: new Options(
          headers: {"Content-Type": "application/json"},
        ));
    final json = response.data;
    print("json " + json["data"].toString());
    if (response.statusCode == 200) {
      notes = (json["data"])
          .map<CartModel>((item) => CartModel.fromJson(item))
          .toList();
    } else {
      throw Exception('Failed to load');
    }
    print("json " + json.toString());
    return new Future.delayed(new Duration(seconds: 1), () {
      return notes.where((i) => i.namaBarang != "").toList();
    });
  }

  _pesan() async {
    if (widget.type == 'toko') {
      api_func = 'checkout_barang';
    } else {
      api_func = 'checkout_makan';
    }

    var param = {
      "id_user": await getSession("user_id"),
      "grand_total": _grandtotal
    };
    print('param ' + param.toString());
    // print('_selectedDestination ' + _selectedDestination.toString());
    final url = API_URL;
    var dio = new Dio();
    try {
      Response response;
      response = await dio.post(url + api_func,
          data: param,
          options: new Options(
            headers: {"Content-Type": "application/x-www-form-urlencoded"},
          ));

      print(response.data.toString());
      print(response.data['success']);
      if (response.data['success'] == true) {
        Navigator.of(context).pushReplacement(new MaterialPageRoute(
          builder: (BuildContext context) => new Pembayaran(
            widget.counter,
            widget.subttl,
            widget.nomorUnik,
            widget.type,
          ),
        ));
      } else {
        showToast("Checkout Failed", duration: 5, gravity: Toast.BOTTOM);
      }
    } on DioError catch (e) {
      debugPrint("Error $e");
    }
  }

  @override
  void initState() {
    super.initState();
    _grandtotal =
        int.parse(widget.subttl) + (int.tryParse(widget.nomorUnik ?? '-') ?? 0);
    (() async {
      _listFutureUser = getUser();
      if (widget.type != 'fasilitas' && widget.type != 'service') {
        _listFutureCart = fetchCart();
      }
    })();
  }

  _listItem(UserModel user) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 10,
            child: Column(
              children: <Widget>[
                Align(
                    alignment: Alignment.topLeft,
                    child: Text((user.namaApt ?? '-') +
                        ', ' +
                        (user.namaGedung ?? '-') +
                        ', ' +
                        (user.namaUnit ?? '-'))),
                Align(
                  alignment: Alignment.topLeft,
                  child: Text('Lantai : ' +
                      (user.lantai ?? '-') +
                      ', Nomor : ' +
                      (user.nomor ?? '-')),
                ),
                Align(
                  alignment: Alignment.topLeft,
                  child: Text(user.alamat ?? '-'),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20.0),
                ),
                Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    "Pembayaran",
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 10.0),
                ),
                Align(
                  alignment: Alignment.topLeft,
                  child: Text('Bank : ' + user.namaBank),
                ),
                Align(
                  alignment: Alignment.topLeft,
                  child: Text('Nomor Rekening Anda: ' + user.nomorRekening),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  _listCart(CartModel cart) {
    return Padding(
      padding: const EdgeInsets.only(left: 10.0, right: 10.0),
      child: Column(
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Flexible(
                flex: 2,
                child: Image.network(
                  cart.img,
                  width: 40.0,
                  height: 40.0,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 10.0, right: 10.0),
              ),
              Flexible(
                  flex: 5,
                  child: Align(
                    alignment: Alignment.topRight,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Container(
                              margin: const EdgeInsets.only(left: 15.0),
                              child: Text(
                                cart.namaBarang,
                                style: TextStyle(
                                    fontSize: 13.0,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  )),
              Flexible(
                flex: 3,
                child: Text(
                  thousandSeparator(cart.totalHarga.toString()),
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 13.0,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Future<bool> cancelRequest(String type) async {
    Response response;
    var param;
    if (type == 'fasilitas') {
      param = {
        "fasilitas_id": widget.data['fasilitas_id'],
        "request_start": widget.data['request_start'],
        "request_end": widget.data['request_end'],
        "id_user": await getSession("user_id"),
      };
    } else {
      param = {
        "request": widget.data['request'],
        "request_date": widget.data['request_date'],
        "id_user": await getSession("user_id"),
      };
    }
    response = await Dio().post(API_URL_NEW + "delete/$type",
        data: param,
        options: new Options(
          headers: {"Content-Type": "application/json"},
        ));
    final json = response.data;
    print("json " + json["data"].toString());
    if (response.statusCode == 200) {
      return true;
    } else {
      throw Exception('Failed to load');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: hexToColor("#344b6b"),
          title: Text("Checkout"),
          elevation: 0,
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                if (widget.type == 'toko') {
                  Navigator.of(context).pushReplacement(new MaterialPageRoute(
                    builder: (BuildContext context) =>
                        new Cart(widget.counter, widget.subttl),
                    // new Notif(),
                  ));
                } else if (widget.type == 'resto') {
                  Navigator.of(context).pushReplacement(new MaterialPageRoute(
                    builder: (BuildContext context) =>
                        new CartFood(widget.counter, widget.subttl),
                    // new Notif(),
                  ));
                } else {
                  cancelRequest(widget.type);
                  Navigator.pop(context);
                }
              })),
      body: new SingleChildScrollView(
        child: Container(
          child: Column(
            children: <Widget>[
              Align(
                child: Padding(
                  padding:
                      const EdgeInsets.only(top: 20.0, left: 10.0, right: 10.0),
                  child: Container(
                      padding: EdgeInsets.all(15.0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        boxShadow: <BoxShadow>[
                          new BoxShadow(
                            color: Colors.black12,
                            blurRadius: 10.0,
                            offset: new Offset(0.0, 10.0),
                          ),
                        ],
                      ),
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Text(
                                "Alamat Apartemen",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black),
                              ),
                            ],
                          ),
                          Padding(
                            padding: EdgeInsets.only(bottom: 10.0),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width,
                            child: new FutureBuilder<List>(
                                future: _listFutureUser,
                                builder: (context, snapshot) {
                                  if (snapshot.hasData) {
                                    return new ListView.builder(
                                      shrinkWrap: true,
                                      physics:
                                          const NeverScrollableScrollPhysics(),
                                      itemCount: snapshot.data.length,
                                      padding: EdgeInsets.only(top: 4.0),
                                      itemBuilder: (context, index) {
                                        return _listItem(snapshot.data[index]);
                                      },
                                    );
                                  }
                                  return Center(
                                    child: SizedBox(
                                        width: 40.0,
                                        height: 40.0,
                                        child:
                                            const CircularProgressIndicator()),
                                  );
                                }),
                          ),
                        ],
                      )),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 10.0),
              ),
              Align(
                child: Padding(
                  padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                  child: Container(
                      padding:
                          EdgeInsets.only(left: 15.0, right: 15.0, top: 15.0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        boxShadow: <BoxShadow>[
                          new BoxShadow(
                            color: Colors.black12,
                            blurRadius: 10.0,
                            offset: new Offset(0.0, 10.0),
                          ),
                        ],
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Text(
                                widget.type == 'fasilitas' ||
                                        widget.type == 'service'
                                    ? "Informasi Detail"
                                    : "Order List",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black),
                              ),
                            ],
                          ),
                          Padding(
                            padding: EdgeInsets.only(bottom: 10.0),
                          ),
                          widget.type == 'fasilitas' || widget.type == 'service'
                              ? widget.type == 'service'
                                  ? Column(
                                      mainAxisSize: MainAxisSize.min,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                            'Nama servis : ${widget.data['request']}'),
                                        Text(
                                            'Tanggal : ${widget.data['request_date']}'),
                                      ],
                                    )
                                  : Container(
                                      margin: EdgeInsets.only(bottom: 20),
                                      child: Row(
                                        children: <Widget>[
                                          Expanded(
                                            flex: 2,
                                            child: Image.network(
                                              widget?.data['img'] ?? '',
                                              width: 40,
                                            ),
                                          ),
                                          Expanded(
                                            flex: 5,
                                            child: Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 20),
                                                child: Column(
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    Text(
                                                        'Nama Fasilitas: ${widget.data['name']}'),
                                                    Text(
                                                        'Mulai : ${widget.data['request_start']}'),
                                                    Text(
                                                        'Selesai : ${widget.data['request_end']}'),
                                                  ],
                                                )),
                                          ),
                                        ],
                                      ),
                                    )
                              : Container(
                                  width: MediaQuery.of(context).size.width,
                                  child: new FutureBuilder<List>(
                                      future: _listFutureCart,
                                      builder: (context, snapshot) {
                                        if (snapshot.hasData) {
                                          return new ListView.builder(
                                            shrinkWrap: true,
                                            physics:
                                                const NeverScrollableScrollPhysics(),
                                            itemCount: snapshot.data.length,
                                            padding: EdgeInsets.only(top: 4.0),
                                            itemBuilder: (context, index) {
                                              return _listCart(
                                                  snapshot.data[index]);
                                            },
                                          );
                                        }
                                        return Center(
                                          child: SizedBox(
                                              width: 40.0,
                                              height: 40.0,
                                              child:
                                                  const CircularProgressIndicator()),
                                        );
                                      }),
                                ),
                          Divider(),
                          Container(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 10.0, right: 10.0, top: 10),
                              child: Column(
                                children: <Widget>[
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Flexible(
                                        flex: 2,
                                        child: Image.asset(
                                          'assets/images/white.jpg',
                                          fit: BoxFit.contain,
                                          width: 40.0,
                                          height: 40.0,
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                            left: 10.0, right: 10.0),
                                      ),
                                      Flexible(
                                          flex: 5,
                                          child: Align(
                                            alignment: Alignment.topRight,
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              children: <Widget>[
                                                Row(
                                                  children: <Widget>[
                                                    Container(
                                                      margin:
                                                          const EdgeInsets.only(
                                                              left: 15.0),
                                                      child: Text(
                                                        "Biaya ",
                                                        style: TextStyle(
                                                            fontSize: 13.0,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          )),
                                      Flexible(
                                        flex: 3,
                                        child: Text(
                                          widget.subttl,
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 13.0,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 10.0, right: 10.0),
                              child: Column(
                                children: <Widget>[
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Flexible(
                                        flex: 2,
                                        child: Image.asset(
                                          'assets/images/white.jpg',
                                          fit: BoxFit.contain,
                                          width: 40.0,
                                          height: 40.0,
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                            left: 10.0, right: 10.0),
                                      ),
                                      Flexible(
                                          flex: 5,
                                          child: Align(
                                            alignment: Alignment.topRight,
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              children: <Widget>[
                                                Row(
                                                  children: <Widget>[
                                                    Container(
                                                      margin:
                                                          const EdgeInsets.only(
                                                              left: 15.0),
                                                      child: Text(
                                                        "Nomor Unik ",
                                                        style: TextStyle(
                                                            fontSize: 13.0,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          )),
                                      Flexible(
                                        flex: 3,
                                        child: Text(
                                          widget.nomorUnik ?? '-',
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 13.0,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 10.0, right: 10.0),
                              child: Column(
                                children: <Widget>[
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Flexible(
                                        flex: 2,
                                        child: Image.asset(
                                          'assets/images/white.jpg',
                                          fit: BoxFit.contain,
                                          width: 40.0,
                                          height: 40.0,
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                            left: 10.0, right: 10.0),
                                      ),
                                      Flexible(
                                          flex: 5,
                                          child: Align(
                                            alignment: Alignment.topRight,
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              children: <Widget>[
                                                Row(
                                                  children: <Widget>[
                                                    Container(
                                                      margin:
                                                          const EdgeInsets.only(
                                                              left: 15.0),
                                                      child: Text(
                                                        "Grand Total ",
                                                        style: TextStyle(
                                                            fontSize: 13.0,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          )),
                                      Flexible(
                                        flex: 3,
                                        child: Text(
                                          thousandSeparator(
                                              _grandtotal.toString()),
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 13.0,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      )),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 10.0),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Container(
        height: 60,
        color: Colors.white,
        child: Padding(
          padding: EdgeInsets.only(top: 4.0, left: 0.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Flexible(
                flex: 10,
                child: InkWell(
                  onTap: () {
                    if (widget.type == 'fasilitas' ||
                        widget.type == 'service') {
                      Navigator.of(context)
                          .pushReplacement(new MaterialPageRoute(
                        builder: (BuildContext context) => new Pembayaran(
                          widget.counter,
                          widget.subttl,
                          widget.nomorUnik,
                          widget.type,
                          data: widget.data,
                        ),
                      ));
                    } else {
                      _pesan();
                    }
                  },
                  child: Align(
                    alignment: Alignment.topRight,
                    child: Container(
                      decoration: BoxDecoration(
                        color: Color.fromRGBO(0, 185, 92, 1),
                        boxShadow: <BoxShadow>[
                          new BoxShadow(
                            color: Colors.black12,
                            blurRadius: 10.0,
                            offset: new Offset(0.0, 10.0),
                          ),
                        ],
                      ),

                      height: 55.0, // height of the button
                      // width: 135.0, // width of the button
                      child: Center(
                          child: Text("Buat Pesanan",
                              style: TextStyle(
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white))),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
