import 'package:carousel_slider/carousel_slider.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
// ignore: unused_import
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';
import 'package:wismartlink/model/cartModel.dart';
import 'package:wismartlink/model/jumlahTransaksiToko.dart';
import 'package:wismartlink/page/home_page.dart';
import 'package:wismartlink/page/marketplace/barang_detail_page.dart';
import 'package:wismartlink/page/marketplace/barang_page.dart';
import 'package:wismartlink/page/marketplace/cart.dart';
import 'package:wismartlink/page/marketplace/marketplace_register_page.dart';
import 'package:wismartlink/page/marketplace/pesananSaya.dart';

class MarketPlacePage extends StatefulWidget {
  const MarketPlacePage({Key key}) : super(key: key);

  @override
  _MarketPlacePageController createState() => _MarketPlacePageController();
}

class _MarketPlacePageController extends State<MarketPlacePage>
    with TickerProviderStateMixin {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isLoading = false;
  bool isPunyaToko = false;
  List data;
  List dataPromo;

  TextEditingController searchController = new TextEditingController();
  int counter = 0;
  String subttl;
  int list = 0;
  List<JumlahTransaksiToko> _requestList = [];

  TabController tabController;
  int index = 0;

  @override
  void initState() {
    super.initState();
    _getRequestDetail();
    _getJmlCheckout();
    validatePunyaToko();
    getData();
    getDataPromo();

    tabController = TabController(
      length: 2,
      vsync: this,
      initialIndex: 0,
    );
  }

  void validatePunyaToko() async {
    String id_toko = await getSession("id_toko");
    if (id_toko != null) {
      if (id_toko != "") {
        setState(() {
          isPunyaToko = true;
        });
      }
    }
  }

  void getData() async {
    setState(() => isLoading = true);

    try {
      Response response = await new Dio().post(API_URL + 'list_barang',
          data: new FormData.fromMap({"keyword": searchController.text}),
          options: Options(method: 'POST', responseType: ResponseType.json));

      setState(() => isLoading = false);

      if (response.toString() != '') {
        setState(() {
          data = response.data['data'];
        });
      } else {
        showSnackBar(scaffoldKey, NETWORK_ERROR);
      }
    } on DioError catch (e) {
      showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
    }
  }

  void getDataPromo() async {
    setState(() => isLoading = true);

    try {
      Response response = await new Dio().post(
          API_URL_NEW + 'toko/barang_promo',
          data: new FormData.fromMap({"keyword": searchController.text}),
          options: Options(method: 'POST', responseType: ResponseType.json));

      setState(() => isLoading = false);

      if (response.toString() != '') {
        setState(() {
          dataPromo = response.data['data'];
        });
      } else {
        showSnackBar(scaffoldKey, NETWORK_ERROR);
      }
    } on DioError catch (e) {
      showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
    }
  }

  _getJmlCheckout() async {
    var dio = new Dio();

    final url = API_URL;
    var param = {
      "id_user": await getSession("user_id"),
    };

    try {
      Response response;
      response = await dio.post(url + "jumlah_list_transaksi_toko_checkout",
          data: param,
          options: new Options(
            headers: {"Content-Type": "application/json"},
          ));
      // print('response.data[] ' + response.data['data']['jumlah'].toString());
      if (response.data['success'] == true) {
        setState(() {
          list = response.data['data']['list'];
          print("list " + list.toString());
        });
      } else {
        setState(() {
          list = 0;
        });
      }
    } on DioError catch (e) {
      print("CCC");
      if (e.response != null) {
        print(e.response.data);
        print(e.response.headers);
        print(e.response.request);
      } else {
        print(e.request);
        print(e.message);
      }
    }
  }

  _getRequestDetail() async {
    var dio = new Dio();

    final url = API_URL;
    var param = {
      "id_user": await getSession("user_id"),
    };

    try {
      Response response;
      response = await dio.post(url + "jumlah_transaksi_toko_belum_lunas",
          data: param,
          options: new Options(
            headers: {"Content-Type": "application/json"},
          ));
      // print('response.data[] ' + response.data['data']['jumlah'].toString());
      if (response.data['success'] == true) {
        setState(() {
          counter = response.data['data']['jumlah'];
          subttl = response.data['data']['subtotal'];
        });
      } else {
        setState(() {
          counter = 0;
        });
      }
    } on DioError catch (e) {
      print("CCC");
      if (e.response != null) {
        print(e.response.data);
        print(e.response.headers);
        print(e.response.request);
      } else {
        print(e.request);
        print(e.message);
      }
    }
  }

  _listItem(CartModel cart) {
    return Column(
      children: <Widget>[
        Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(5.0)),
            ),
            child: Container(
              padding: EdgeInsets.all(10.0),
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(15.0),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      boxShadow: <BoxShadow>[
                        new BoxShadow(
                          color: Colors.black12,
                          blurRadius: 10.0,
                          offset: new Offset(0.0, 10.0),
                        ),
                      ],
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Flexible(
                          flex: 5,
                          child: Image.network(
                            cart.img,
                            width: 80.0,
                            height: 80.0,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 10.0, right: 10.0),
                        ),
                        Flexible(
                            flex: 5,
                            child: Align(
                              alignment: Alignment.topRight,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Container(
                                        margin:
                                            const EdgeInsets.only(left: 15.0),
                                        child: Text(
                                          cart.namaBarang,
                                          style: TextStyle(
                                              fontSize: 16.0,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: 14.0),
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Container(
                                        margin:
                                            const EdgeInsets.only(left: 15.0),
                                        child: Text(
                                          thousandSeparator(
                                              cart.totalHarga.toString()),
                                          style: TextStyle(
                                              color: Colors.green,
                                              fontSize: 13.0,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            )),
                      ],
                    ),
                  ),
                ],
              ),
            )),
      ],
    );
  }

  Widget _icon_cart() {
    if (counter == 0) {
      return Stack(
        children: <Widget>[
          new IconButton(
              icon: Icon(Icons.shopping_cart),
              onPressed: () {
                setState(() {
                  counter = 0;
                });
              }),
          counter != 0
              ? new Positioned(
                  right: 11,
                  top: 11,
                  child: new Container(
                    padding: EdgeInsets.all(2),
                    decoration: new BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(6),
                    ),
                    constraints: BoxConstraints(
                      minWidth: 14,
                      minHeight: 14,
                    ),
                    child: Text(
                      '$counter',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 8,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                )
              : new Container()
        ],
      );
    } else {
      return InkWell(
        onTap: () {
          Navigator.of(context).push(new MaterialPageRoute(builder: (_) {
            return new Cart(counter.toString(), subttl.toString());
          }));
        },
        child: new Stack(
          children: <Widget>[
            new IconButton(
                icon: Icon(Icons.shopping_cart),
                onPressed: () {
                  setState(() {
                    counter = 0;
                  });
                  Navigator.of(context)
                      .push(new MaterialPageRoute(builder: (_) {
                    return new Cart(counter.toString(), subttl.toString());
                  }));
                }),
            counter != 0
                ? new Positioned(
                    right: 11,
                    top: 11,
                    child: new Container(
                      padding: EdgeInsets.all(2),
                      decoration: new BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(6),
                      ),
                      constraints: BoxConstraints(
                        minWidth: 14,
                        minHeight: 14,
                      ),
                      child: Text(
                        '$counter',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 8,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  )
                : new Container()
          ],
        ),
      );
    }
  }

  Widget _icon_list() {
    if (list == 0) {
      return Stack(
        children: <Widget>[
          new IconButton(
              icon: Icon(Icons.assignment),
              onPressed: () {
                setState(() {
                  list = 0;
                });
              }),
          list != 0
              ? new Positioned(
                  right: 11,
                  top: 11,
                  child: new Container(
                    padding: EdgeInsets.all(2),
                    decoration: new BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(6),
                    ),
                    constraints: BoxConstraints(
                      minWidth: 14,
                      minHeight: 14,
                    ),
                    child: Text(
                      '',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 8,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                )
              : new Container()
        ],
      );
    } else {
      return InkWell(
        onTap: () {
          Navigator.of(context).push(new MaterialPageRoute(builder: (_) {
            return new PesananSaya(list.toString(), subttl.toString(), "toko");
          }));
        },
        child: new Stack(
          children: <Widget>[
            new IconButton(
                icon: Icon(Icons.assignment),
                onPressed: () {
                  setState(() {
                    list = 0;
                  });
                  Navigator.of(context)
                      .push(new MaterialPageRoute(builder: (_) {
                    return new PesananSaya(
                        list.toString(), subttl.toString(), "toko");
                  }));
                }),
            list != 0
                ? new Positioned(
                    right: 11,
                    top: 11,
                    child: new Container(
                      padding: EdgeInsets.all(2),
                      decoration: new BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(6),
                      ),
                      constraints: BoxConstraints(
                        minWidth: 14,
                        minHeight: 14,
                      ),
                      child: Text(
                        '$list',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 8,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  )
                : new Container()
          ],
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          leading: new IconButton(
              icon: new Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).push(new MaterialPageRoute(builder: (_) {
                  return new HomePage();
                }));
              }),
          backgroundColor: hexToColor("#344b6b"),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(children: <Widget>[Text("Shop")]),
              Column(children: <Widget>[
                Row(
                  children: <Widget>[
                    _icon_cart(),
                    _icon_list(),
                  ],
                ),
              ]),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) =>
                      isPunyaToko ? BarangPage() : MarketPlaceRegisterPage(),
                ));
          },
          label: Text(isPunyaToko ? 'Atur Toko' : 'Buka Toko'),
          icon: Icon(Icons.shop),
          backgroundColor: Colors.green,
        ),
        body: new Column(
          children: <Widget>[
            CarouselSlider(
              height: 150,
              autoPlay: true,
              enableInfiniteScroll: true,
              viewportFraction: 1.0,
              enlargeCenterPage: true,
              aspectRatio: MediaQuery.of(context).size.aspectRatio,
              items: [
                'assets/images/banner_makanan.png',
                'assets/images/banner_makanan.png',
              ].map(
                (url) {
                  return Container(
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(0.0)),
                      child: Image(image: AssetImage(url)),
                    ),
                  );
                },
              ).toList(),
            ),
            // new Container(
            //   color: hexToColor("#344b6b"),
            //   child: new Padding(
            //     padding: const EdgeInsets.all(3),
            //     child: new Card(
            //       child: new ListTile(
            //         leading: new Icon(Icons.search),
            //         title: new TextField(
            //           controller: searchController,
            //           decoration: new InputDecoration(
            //               hintText: 'Search', border: InputBorder.none),
            //           onChanged: (text) {
            //             getData();
            //           },
            //         ),
            //         trailing: new IconButton(
            //           icon: new Icon(Icons.cancel),
            //           onPressed: () {
            //             searchController.clear();
            //             getData();
            //           },
            //         ),
            //       ),
            //     ),
            //   ),
            // ),
            TabBar(
              labelPadding:
                  EdgeInsets.symmetric(horizontal: 9.0, vertical: 0.0),
              labelStyle: TextStyle(fontSize: 14.0),
              indicatorWeight: 1.0,
              labelColor: Colors.blue[500],
              unselectedLabelColor: Colors.grey,
              indicatorColor: Colors.blue[500],
              controller: tabController,
              onTap: (val) {
                index = val;

                setState(() {});
              },
              tabs: [
                Tab(
                  child: Text('Menu'),
                ),
                Tab(
                  child: Text('Promo'),
                ),
              ],
            ),
            Expanded(
              child: isLoading
                  ? loadingScreen()
                  : TabBarView(
                      controller: tabController,
                      children: tabBarView(),
                    ),
            ),
          ],
        ));
  }

  List<Widget> tabBarView() {
    return [
      ListView.builder(
        itemCount: data == null ? 0 : data.length,
        padding: EdgeInsets.all(15),
        itemBuilder: (context, index) {
          return buildItem(context, index, data);
        },
      ),
      ListView.builder(
        itemCount: dataPromo == null ? 0 : dataPromo.length,
        padding: EdgeInsets.all(15),
        itemBuilder: (context, index) {
          return buildItem(context, index, dataPromo);
        },
      ),
    ];
  }

  InkWell buildItem(BuildContext context, int index, List _data) {
    return InkWell(
        onTap: () {
          Navigator.of(context).push(new MaterialPageRoute(
            builder: (BuildContext context) => new BarangDetailPage(
                _data[index]['id'],
                _data[index]['nama_barang'],
                _data[index]['harga'],
                _data[index]['img'],
                _data[index]['keterangan']),
          ));
        },
        child: Container(
            height: 100,
            margin: EdgeInsets.only(bottom: 15),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              border: Border.all(
                color: Colors.grey[400],
              ),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Flexible(
                  flex: 5,
                  child: Image.network(
                    _data[index]['img'],
                    width: 125,
                    height: 100,
                    fit: BoxFit.cover,
                  ),
                ),
                Flexible(
                    flex: 5,
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(0, 15, 10, 15),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              margin: const EdgeInsets.only(left: 15.0),
                              child: Text(
                                _data[index]['nama_barang'],
                                style: TextStyle(
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                          if (_data[index]['harga_promo'] != null)
                            Container(
                              margin: const EdgeInsets.only(left: 15.0),
                              child: Text(
                                thousandSeparator(
                                    _data[index]['harga_promo'].toString()),
                                style: TextStyle(
                                  color: Colors.red,
                                  fontSize: 13.0,
                                  decoration: TextDecoration.lineThrough,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          Container(
                            margin: const EdgeInsets.only(left: 15.0),
                            child: Text(
                              thousandSeparator(
                                  _data[index]['harga'].toString()),
                              style: TextStyle(
                                  color: Colors.green,
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    )),
              ],
            )));
  }
}
