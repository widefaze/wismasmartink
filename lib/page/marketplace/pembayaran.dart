// import 'package:dio/dio.dart';
import 'package:dio/dio.dart';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';
import 'package:wismartlink/model/bankModel.dart';
import 'package:wismartlink/page/marketplace/marketplace_page.dart';
import 'package:wismartlink/page/restaurant/restaurant_page.dart';

class Pembayaran extends StatefulWidget {
  const Pembayaran(this.counter, this.subttl, this.nomor_unik, this.type,
      {this.data});
  final String counter, subttl, nomor_unik, type;
  final Map data;
  @override
  _PembayaranController createState() => _PembayaranController();
}

class _PembayaranController extends State<Pembayaran> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  var id = '';
  final _formKey = GlobalKey<FormState>();
  // you must keep track of the TextEditingControllers if you want the values to persist correctly
  List<TextEditingController> controllers = <TextEditingController>[];
  TextEditingController namaBarangController = TextEditingController();
  TextEditingController hargaController = TextEditingController();
  TextEditingController keteranganController = TextEditingController();
  bool isLoading = false;
  dynamic pickImageError;
  Future<List<BankModel>> _listFutureBank;
  String _subtotal = "";
  String retrieveDataError;
  File _imageFile;
  int _itemCount = 0;
  int _grandtotal = 0;
  String _nama_unit = "";
  String _nomor;
  String _lantai;
  String _nama_gedung;
  String _alamat;
  String _kota;
  String _nama_apt;

  void showToast(String msg, {int duration, int gravity}) {
    Toast.show(msg, context, duration: Toast.LENGTH_LONG, gravity: gravity);
  }

  Future<List<BankModel>> getBank() async {
    var notes = List<BankModel>();

    Response response;
    var param = {"id_user": await getSession("user_id")};
    response = await Dio().post(API_URL + "list_bank",
        data: param,
        options: new Options(
          headers: {"Content-Type": "application/json"},
        ));
    final json = response.data;
    print("json " + json["data"].toString());
    if (response.statusCode == 200) {
      notes = (json["data"])
          .map<BankModel>((item) => BankModel.fromJson(item))
          .toList();
    } else {
      throw Exception('Failed to load');
    }
    print("json " + json.toString());
    return new Future.delayed(new Duration(seconds: 1), () {
      return notes.where((i) => i.accountNumber != "").toList();
    });
  }

  @override
  void initState() {
    super.initState();
    _grandtotal = int.parse(widget.subttl) +
        (int.tryParse(widget.nomor_unik ?? '-') ?? 0);
    (() async {
      _listFutureBank = getBank();
    })();
  }

  Future<bool> cancelRequest(String type) async {
    Response response;
    var param;
    if (type == 'fasilitas') {
      param = {
        "fasilitas_id": widget.data['fasilitas_id'],
        "request_start": widget.data['request_start'],
        "request_end": widget.data['request_end'],
        "id_user": await getSession("user_id"),
      };
    } else {
      param = {
        "request": widget.data['request'],
        "request_date": widget.data['request_date'],
        "id_user": await getSession("user_id"),
      };
    }
    response = await Dio().post(API_URL_NEW + "delete/$type",
        data: param,
        options: new Options(
          headers: {"Content-Type": "application/json"},
        ));
    final json = response.data;
    print("json " + json["data"].toString());
    if (response.statusCode == 200) {
      return true;
    } else {
      throw Exception('Failed to load');
    }
  }

  _listItem(BankModel bank) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 10,
            child: Column(
              children: <Widget>[
                Align(
                  alignment: Alignment.topLeft,
                  child: Text('Bank : ' + bank.bankName),
                ),
                Align(
                  alignment: Alignment.topLeft,
                  child: Text('Nomor Rekening : ' + bank.accountNumber),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: hexToColor("#344b6b"),
          title: Text("Pembayaran"),
          elevation: 0,
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                if (widget.type == 'toko') {
                  Navigator.of(context).pushReplacement(new MaterialPageRoute(
                    builder: (BuildContext context) => new MarketPlacePage(),
                    // new Notif(),
                  ));
                } else if (widget.type == 'resto') {
                  Navigator.of(context).pushReplacement(new MaterialPageRoute(
                    builder: (BuildContext context) => new RestaurantPage(),
                    // new Notif(),
                  ));
                } else {
                  cancelRequest(widget.type);
                  Navigator.pop(context);
                }
              })),
      body: new SingleChildScrollView(
        child: Container(
          child: Column(
            children: <Widget>[
              Align(
                child: Padding(
                  padding:
                      const EdgeInsets.only(top: 20.0, left: 10.0, right: 10.0),
                  child: Container(
                      padding: EdgeInsets.all(15.0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        boxShadow: <BoxShadow>[
                          new BoxShadow(
                            color: Colors.black12,
                            blurRadius: 10.0,
                            offset: new Offset(0.0, 10.0),
                          ),
                        ],
                      ),
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Text(
                                "Daftar Bank",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black),
                              ),
                            ],
                          ),
                          Padding(
                            padding: EdgeInsets.only(bottom: 10.0),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width,
                            child: new FutureBuilder<List>(
                                future: _listFutureBank,
                                builder: (context, snapshot) {
                                  if (snapshot.hasData) {
                                    return new ListView.builder(
                                      shrinkWrap: true,
                                      physics:
                                          const NeverScrollableScrollPhysics(),
                                      itemCount: snapshot.data.length,
                                      padding: EdgeInsets.only(top: 4.0),
                                      itemBuilder: (context, index) {
                                        return _listItem(snapshot.data[index]);
                                      },
                                    );
                                  }
                                  return Center(
                                    child: SizedBox(
                                        width: 40.0,
                                        height: 40.0,
                                        child:
                                            const CircularProgressIndicator()),
                                  );
                                }),
                          ),
                        ],
                      )),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 10.0),
              ),
              Align(
                child: Padding(
                  padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                  child: Container(
                      padding:
                          EdgeInsets.only(left: 15.0, right: 15.0, top: 15.0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        boxShadow: <BoxShadow>[
                          new BoxShadow(
                            color: Colors.black12,
                            blurRadius: 10.0,
                            offset: new Offset(0.0, 10.0),
                          ),
                        ],
                      ),
                      child: Column(
                        children: <Widget>[
                          Container(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 30.0,
                                  right: 30.0,
                                  top: 30.0,
                                  bottom: 30.0),
                              child: Column(
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        "Grand Total",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontSize: 20.0,
                                            color: Colors.black),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        thousandSeparator(
                                            _grandtotal.toString()),
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 22.0,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      )),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 10.0),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Container(
        height: 60,
        color: Color.fromRGBO(0, 185, 92, 1),
        child: InkWell(
          onTap: () {
            if (widget.type == 'toko') {
              Navigator.of(context).pushReplacement(new MaterialPageRoute(
                builder: (BuildContext context) => new MarketPlacePage(),
                // new Notif(),
              ));
            } else if (widget.type == 'resto') {
              Navigator.of(context).pushReplacement(new MaterialPageRoute(
                builder: (BuildContext context) => new RestaurantPage(),
                // new Notif(),
              ));
            } else {
              Navigator.of(context).pop();
              Navigator.of(context).pop();
            }
          },
          child: Padding(
            padding: EdgeInsets.only(top: 4.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('OK',
                    style: TextStyle(
                        fontSize: 18.0,
                        color: Colors.white,
                        fontWeight: FontWeight.bold)),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
