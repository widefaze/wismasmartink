// import 'package:dio/dio.dart';
import 'package:dio/dio.dart';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';
import 'package:wismartlink/model/orderListModel.dart';
import 'package:wismartlink/model/userModel.dart';
import 'package:wismartlink/page/marketplace/marketplace_page.dart';
import 'package:toast/toast.dart';
import 'package:wismartlink/page/restaurant/restaurant_page.dart';

class PesananSaya extends StatefulWidget {
  const PesananSaya(this.counter, this.subttl, this.type);
  final String counter, subttl, type;
  @override
  _PesananSayaController createState() => _PesananSayaController();
}

class _PesananSayaController extends State<PesananSaya> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  var id = '';
  final _formKey = GlobalKey<FormState>();
  // you must keep track of the TextEditingControllers if you want the values to persist correctly
  List<TextEditingController> controllers = <TextEditingController>[];
  TextEditingController namaBarangController = TextEditingController();
  TextEditingController hargaController = TextEditingController();
  TextEditingController keteranganController = TextEditingController();
  bool isLoading = false;
  dynamic pickImageError;
  Future<List<UserModel>> _listFutureUser;
  Future<List<OrderListModel>> _listFutureCart;
  List<ListOrder> list_order = [];

  String _subtotal = "";
  String retrieveDataError;
  File _imageFile;
  int _itemCount = 0;
  int _grandtotal = 0;
  String _nama_unit = "";
  String _nomor;
  String _lantai;
  String _nama_gedung;
  String _alamat;
  String _kota;
  String _nama_apt;
  String api_func;
  String _nomor_unik = "";

  void showToast(String msg, {int duration, int gravity}) {
    Toast.show(msg, context, duration: Toast.LENGTH_LONG, gravity: gravity);
  }

  Future<List<OrderListModel>> fetchCart() async {
    var notes = List<OrderListModel>();
    if (widget.type == 'toko') {
      api_func = 'list_nomor_transaksi_toko';
    } else {
      api_func = 'list_nomor_transaksi_resto';
    }
    Response response;
    var param = {"id_user": await getSession("user_id")};
    response = await Dio().post(API_URL + api_func,
        data: param,
        options: new Options(
          headers: {"Content-Type": "application/json"},
        ));
    final json = response.data;
    print("json " + json["data"].toString());
    if (response.statusCode == 200) {
      notes = (json["data"])
          .map<OrderListModel>((item) => OrderListModel.fromJson(item))
          .toList();
      if (notes.toString() != "[]") {
        if (mounted) {
          setState(() {
            list_order = notes[0].listOrder;
          });
        }
      }
      ;
    } else {
      throw Exception('Failed to load');
    }
    print("json " + json.toString());
    return new Future.delayed(new Duration(seconds: 1), () {
      return notes.where((i) => i.id != "").toList();
    });
  }

  order() {
    var orderlist = '';
    List<Widget> list = new List<Widget>();
    int a = 1;
    for (int i = 0; i < list_order.length; i++) {
      orderlist = list_order[i].namaBarang;
      List cities = list_order;
      String s = list_order.join(', ');
      list.add(Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new Text(
              a.toString() +
                  '. ' +
                  list_order[i].namaBarang +
                  ', Qty : (' +
                  list_order[i].jumlahBarang +
                  ')',
              style: TextStyle(color: Colors.black)),
          Padding(
            padding: EdgeInsets.only(top: 10.0),
          ),
          Divider(
            color: Colors.black,
          ),
          Padding(
            padding: EdgeInsets.only(top: 10.0),
          ),
        ],
      ));
      a++;
    }
    return Flexible(
        flex: 6,
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start, children: list));
  }

  @override
  void initState() {
    super.initState();
    (() async {
      _listFutureCart = fetchCart();
    })();
  }

  _listCart(OrderListModel cart) {
    var status_order = "";
    if (cart.statusOrder == '1') {
      status_order = 'Belum Bayar';
    } else if (cart.statusOrder == '2' || cart.statusOrder == '3') {
      status_order = 'Dalam Proses';
    } else if (cart.statusOrder == '4') {
      status_order = 'Selesai ';
    } else {
      status_order = '';
    }

    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
        boxShadow: <BoxShadow>[
          new BoxShadow(
            color: Colors.black12,
            blurRadius: 10.0,
            offset: new Offset(0.0, 10.0),
          ),
        ],
      ),
      // height: 300.0,
      padding: EdgeInsets.only(top: 10.0),
      margin: EdgeInsets.only(bottom: 10.0),
      child: Align(
        child: Container(
          child: ListTile(
            contentPadding:
                EdgeInsets.symmetric(horizontal: 0.0, vertical: 0.0),
            leading: Container(
              padding: EdgeInsets.only(right: 10.0, left: 8.0, bottom: 15.0),
              decoration: new BoxDecoration(
                  border: new Border(
                      right: new BorderSide(width: 1.0, color: Colors.black))),
              height: 240.0,
              child: Icon(
                Icons.shopping_basket,
                color: Colors.black,
                size: 20.0,
              ),
            ),
            title: Text(
              'Nomor Transaksi : ' + (cart?.kodeTransaksi ?? '-'),
              style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
            ),
            // subtitle: Text("Intermediate", style: TextStyle(color: Colors.white)),

            subtitle: Column(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: 10.0),
                ),
                Row(
                  children: <Widget>[
                    Flexible(
                      fit: FlexFit.tight,
                      flex: 4,
                      child: Text('Grand Total',
                          style: TextStyle(color: Colors.black)),
                    ),
                    Flexible(
                      flex: 1,
                      child: new Text(":  ",
                          style: TextStyle(color: Colors.black)),
                    ),
                    Flexible(
                        flex: 5,
                        child: new Text(
                          thousandSeparator(cart.grandTotal.toString()),
                          style: TextStyle(color: Colors.black),
                        ))
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(top: 5.0),
                ),
                Row(
                  children: <Widget>[
                    Flexible(
                      fit: FlexFit.tight,
                      flex: 4,
                      child:
                          Text('Status', style: TextStyle(color: Colors.black)),
                    ),
                    Flexible(
                      flex: 1,
                      child: new Text(":  ",
                          style: TextStyle(color: Colors.black)),
                    ),
                    Flexible(
                        flex: 5,
                        child: new Text(
                          status_order,
                          style: TextStyle(color: Colors.black),
                        ))
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(top: 5.0),
                ),
                Row(
                  children: <Widget>[
                    Flexible(
                      fit: FlexFit.tight,
                      flex: 4,
                      child: Text('Detail Order',
                          style: TextStyle(color: Colors.black)),
                    ),
                    Flexible(
                      flex: 6,
                      child: new Text(":  ",
                          style: TextStyle(color: Colors.black)),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 5.0),
                ),
                Row(
                  children: <Widget>[order()],
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 5.0),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: hexToColor("#344b6b"),
          title: Text("Pesanan Anda"),
          elevation: 0,
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                if (widget.type == 'toko') {
                  Navigator.of(context).pushReplacement(new MaterialPageRoute(
                    builder: (BuildContext context) => new MarketPlacePage(),
                    // new Notif(),
                  ));
                } else {
                  Navigator.of(context).pushReplacement(new MaterialPageRoute(
                    builder: (BuildContext context) => new RestaurantPage(),
                    // new Notif(),
                  ));
                }
              })),
      body: new SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(color: Colors.white),
          height: MediaQuery.of(context).size.height,
          child: new FutureBuilder<List>(
              future: _listFutureCart,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return new ListView.builder(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: snapshot.data.length,
                    padding: EdgeInsets.only(top: 4.0),
                    itemBuilder: (context, index) {
                      return _listCart(snapshot.data[index]);
                    },
                  );
                }
                return Center(
                  child: SizedBox(
                      width: 40.0,
                      height: 40.0,
                      child: const CircularProgressIndicator()),
                );
              }),
        ),
      ),
    );
  }
}
