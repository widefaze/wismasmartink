import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';

class NotifikasiPage extends StatefulWidget {
  const NotifikasiPage({Key key}) : super(key: key);

  @override
  _NotifikasiPageController createState() => _NotifikasiPageController();
}

class _NotifikasiPageController extends State<NotifikasiPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isLoading = false;
  List data;

  TextEditingController towerTC = new TextEditingController();
  TextEditingController noUnitTC = new TextEditingController();
  TextEditingController pengirimTC = new TextEditingController();
  TextEditingController descTC = new TextEditingController();

  File _imageFile;

  @override
  void initState() {
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) => _NotifikasiPageView(this);

  void getData() async {
    setState(() => isLoading = true);

    try {
      Response response = await new Dio().post(API_URL + 'notifikasi',
          data: new FormData.fromMap({"id_user": await getSession("user_id")}),
          options: Options(method: 'POST', responseType: ResponseType.json));

      setState(() => isLoading = false);

      if (response.toString() != '') {
        setState(() {
          data = response.data['data'];
        });
      } else {
        showSnackBar(scaffoldKey, NETWORK_ERROR);
      }
    } on DioError catch (e) {
      showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
    }
  }

  _selectImage(ImageSource source) async {
    var image = await ImagePicker.pickImage(
        source: source, maxHeight: 1920.0, maxWidth: 1080.0);
    setState(() {
      _imageFile = image;
    });
  }
}

class _NotifikasiPageView extends StatelessWidget {
  final _NotifikasiPageController state;

  const _NotifikasiPageView(this.state, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: state.scaffoldKey,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: hexToColor("#344b6b"),
        title: Text("Notifikasi"),
      ),
      body: state.isLoading
          ? loadingScreen()
          : ListView(
              padding: EdgeInsets.all(25),
              children: [
                Text(
                  'Notifikasi kepada Pemilik',
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w700,
                    color: Colors.blue[800],
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 25),
                TextField(
                  controller: state.towerTC,
                  decoration: InputDecoration(
                      contentPadding:
                          EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      hintText: "Tower",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(height: 15),
                TextField(
                  controller: state.noUnitTC,
                  decoration: InputDecoration(
                      contentPadding:
                          EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      hintText: "No Unit",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(height: 15),
                TextField(
                  controller: state.pengirimTC,
                  decoration: InputDecoration(
                      contentPadding:
                          EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      hintText: "Pengirim",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(height: 10.0),
                TextField(
                  maxLines: 5,
                  controller: state.descTC,
                  decoration: InputDecoration(
                      contentPadding:
                          EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      hintText: "Deskripsi",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(height: 10.0),
                Container(
                  width: double.maxFinite,
                  child: OutlineButton(
                    borderSide: BorderSide(color: Colors.purple),
                    padding: EdgeInsets.all(15),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                    color: Colors.blue[300],
                    child: Text(
                      'Unggah Foto',
                      style: TextStyle(
                        color: Colors.purple,
                      ),
                    ),
                    onPressed: () {
                      state._selectImage(ImageSource.gallery);
                    },
                  ),
                ),
                SizedBox(height: 10.0),
                Container(
                  width: double.maxFinite,
                  child: FlatButton(
                    padding: EdgeInsets.all(15),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                    color: Colors.blue[300],
                    child: Text('Kirim'),
                    onPressed: () {},
                  ),
                ),
              ],
            ),
      // : ListView.separated(
      //     itemCount: state.data?.length ?? 0,
      //     itemBuilder: (context, index) {
      //       return ListTile(
      //         title: Text(state.data[index]['title']),
      //         subtitle:
      //             Text(removeAllHtmlTags(state.data[index]['desc'])),
      //       );
      //     },
      //     separatorBuilder: (context, index) {
      //       return Divider();
      //     },
      //   )
    );
  }
}
