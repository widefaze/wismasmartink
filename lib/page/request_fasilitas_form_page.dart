import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';
import 'package:wismartlink/model/userModel.dart';
import 'package:wismartlink/page/fasilitas_list_page.dart';
import 'package:wismartlink/page/marketplace/checkout.dart';

class RequestFasilitasFormPage extends StatefulWidget {
  const RequestFasilitasFormPage({Key key}) : super(key: key);

  @override
  _RequestFasilitasFormPageController createState() =>
      _RequestFasilitasFormPageController();
}

class _RequestFasilitasFormPageController
    extends State<RequestFasilitasFormPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isLoading = false;
  String fasilitasId;
  String fasilitasImg;

  TextEditingController fasilitasController = new TextEditingController();
  TextEditingController startDateController = new TextEditingController();
  TextEditingController endDateController = new TextEditingController();

  @override
  Widget build(BuildContext context) => _RequestFasilitasFormPageView(this);

  void setStartDate(startDate) {
    setState(() {
      startDateController.text = startDate;
    });
  }

  void setEndDate(endDate) {
    setState(() {
      endDateController.text = endDate;
    });
  }

  Future<UserModel> getUser() async {
    var notes = List<UserModel>();

    Response response;
    var param = {"id_user": await getSession("user_id")};
    response = await Dio().post(API_URL + "user_address",
        data: param,
        options: new Options(
          headers: {"Content-Type": "application/json"},
        ));
    final json = response.data;
    print("json " + json["data"].toString());
    if (response.statusCode == 200) {
      notes = (json["data"])
          .map<UserModel>((item) => UserModel.fromJson(item))
          .toList();
    } else {
      throw Exception('Failed to load');
    }
    print("json " + json.toString());
    return notes[0];
  }

  void requestFasilitas() async {
    if (fasilitasController.text.isEmpty) {
      showSnackBar(scaffoldKey, "Fasilitas belum dipilih");
    } else if (startDateController.text.isEmpty) {
      startDate();
    } else if (endDateController.text.isEmpty) {
      endDate();
    } else {
      setState(() => isLoading = true);

      try {
        Response response = await new Dio().post(API_URL + 'request_fasilitas',
            data: new FormData.fromMap({
              "id_user": await getSession("user_id"),
              "fasilitas_id": fasilitasId,
              "request_start": startDateController.text,
              "request_end": endDateController.text,
            }),
            options: Options(method: 'POST', responseType: ResponseType.json));

        setState(() => isLoading = false);

        if (response.toString() != '') {
          if (response.data['success'] == true) {
            print(response.data);
            await getUser().then((res) {
              Map data = {
                "name": fasilitasController.text,
                "img": fasilitasImg,
                "fasilitas_id": fasilitasId,
                "request_start": startDateController.text,
                "request_end": endDateController.text,
              };
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => Checkout(
                        '0',
                        response.data['data']['biaya'].toString(),
                        res.nomor,
                        'fasilitas',
                        data: data,
                      )));
            });

            // Alert(
            //   context: context,
            //   type: AlertType.success,
            //   title: response.data['message'],
            //   buttons: [
            //     DialogButton(
            //       child: Text(
            //         "OK",
            //         style: TextStyle(color: Colors.white, fontSize: 20),
            //       ),
            //       onPressed: () {
            //         Navigator.pop(context);
            //         Navigator.pop(context);
            //       },
            //       width: 120,
            //     )
            //   ],
            // ).show();
          } else {
            showSnackBar(scaffoldKey, response.data['message']);
          }
        } else {
          showSnackBar(scaffoldKey, NETWORK_ERROR);
        }
      } on DioError catch (e) {
        showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
      }
    }
  }

  void startDate() {
    DatePicker.showDateTimePicker(context, showTitleActions: true,
        onConfirm: (date) {
      setStartDate(date.toString());
    }, currentTime: DateTime.now());
  }

  void endDate() {
    DatePicker.showDateTimePicker(context, showTitleActions: true,
        onConfirm: (date) {
      setEndDate(date.toString());
    }, currentTime: DateTime.now());
  }

  void awaitReturnValueFromFasilitasPage() async {
    final result = await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => FasilitasListPage(),
        ));

    setState(() {
      print(result);
      fasilitasId = result['id'];
      fasilitasController.text = result['fasilitas'];
      fasilitasImg = result['img'];
    });
  }
}

class _RequestFasilitasFormPageView extends StatelessWidget {
  final _RequestFasilitasFormPageController state;

  const _RequestFasilitasFormPageView(this.state, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: state.scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: hexToColor("#344b6b"),
          title: Text("Buku Tamu"),
        ),
        body: state.isLoading
            ? loadingScreen()
            : Container(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(25),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      TextField(
                        readOnly: true,
                        onTap: () => state.awaitReturnValueFromFasilitasPage(),
                        controller: state.fasilitasController,
                        obscureText: false,
                        decoration: InputDecoration(
                            contentPadding:
                                EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                            hintText: "Pilih Fasilitas",
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(32.0))),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      TextField(
                        readOnly: true,
                        controller: state.startDateController,
                        onTap: () {
                          state.startDate();
                        },
                        obscureText: false,
                        decoration: InputDecoration(
                            contentPadding:
                                EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                            hintText: "Start Date",
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(32.0))),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      TextField(
                        readOnly: true,
                        controller: state.endDateController,
                        onTap: () {
                          state.endDate();
                        },
                        obscureText: false,
                        decoration: InputDecoration(
                            contentPadding:
                                EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                            hintText: "End Date",
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(32.0))),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      Material(
                        elevation: 5.0,
                        borderRadius: BorderRadius.circular(30.0),
                        color: Colors.green,
                        child: MaterialButton(
                          minWidth:
                              MediaQuery.of(this.state.context).size.width,
                          padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                          onPressed: state.requestFasilitas,
                          child: Text(
                            "SUBMIT",
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ));
  }
}
