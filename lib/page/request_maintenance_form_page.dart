import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';

var globalContext;

class RequestMaintenanceFormPage extends StatefulWidget {
  const RequestMaintenanceFormPage({Key key}) : super(key: key);

  @override
  _RequestMaintenanceFormPageController createState() =>
      _RequestMaintenanceFormPageController();
}

class _RequestMaintenanceFormPageController
    extends State<RequestMaintenanceFormPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isLoading = false;
  TextEditingController requestController = new TextEditingController();
  TextEditingController requestDateController = new TextEditingController();

  @override
  Widget build(BuildContext context) => _RequestMaintenanceFormPageView(this);

  void setRequestDate(requestDate) {
    setState(() {
      requestDateController.text = requestDate;
    });
  }

  void requestMaintenance() async {
    if (requestController.text.isEmpty) {
      showSnackBar(scaffoldKey, "Service Description is required");
    } else if (requestDateController.text.isEmpty) {
      requestDate();
    } else {
      setState(() => isLoading = true);

      try {
        Response response = await new Dio().post(
            API_URL + 'request_maintenance',
            data: new FormData.fromMap({
              "id_user": await getSession("user_id"),
              "request": requestController.text,
              "request_date": requestDateController.text,
            }),
            options: Options(method: 'POST', responseType: ResponseType.json));

        setState(() => isLoading = false);

        if (response.toString() != '') {
          if (response.data['success'] == true) {
            Alert(
              context: context,
              type: AlertType.success,
              title: response.data['message'],
              buttons: [
                DialogButton(
                  child: Text(
                    "OK",
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                    Navigator.pop(context);
                  },
                  width: 120,
                )
              ],
            ).show();
          } else {
            showSnackBar(scaffoldKey, response.data['message']);
          }
        } else {
          showSnackBar(scaffoldKey, NETWORK_ERROR);
        }
      } on DioError catch (e) {
        showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
      }
    }
  }

  void requestDate() {
    DatePicker.showDateTimePicker(context, showTitleActions: true,
        onConfirm: (date) {
      print('confirm $date');
      setRequestDate(date.toString());
    }, currentTime: DateTime.now());
  }

  void closeFunction() {
//    Navigator.of(context, rootNavigator: true).pop();
//    Navigator.of(context).pop();
//    Navigator.pop(context);
  }
}

class _RequestMaintenanceFormPageView extends StatelessWidget {
  final _RequestMaintenanceFormPageController state;

  const _RequestMaintenanceFormPageView(this.state, {Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    globalContext = context;
    return Scaffold(
        key: state.scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: hexToColor("#344b6b"),
          title: Text("New Maintenance"),
        ),
        body: state.isLoading
            ? loadingScreen()
            : Container(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(25),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      InkWell(
                          child: Text(
                        "Apa yang ingin anda perbaiki ?",
                        textAlign: TextAlign.left,
                      )),
                      SizedBox(
                        height: 10.0,
                      ),
                      TextField(
                        controller: state.requestController,
                        obscureText: false,
                        decoration: InputDecoration(
                            contentPadding:
                                EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                            hintText: "Service AC, Atap Bocor, WC Mampet",
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(32.0))),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      TextField(
                        readOnly: true,
                        controller: state.requestDateController,
                        onTap: () {
                          state.requestDate();
                        },
                        obscureText: false,
                        decoration: InputDecoration(
                            contentPadding:
                                EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                            hintText: "Tanggal",
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(32.0))),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      Material(
                        elevation: 5.0,
                        borderRadius: BorderRadius.circular(30.0),
                        color: Colors.green,
                        child: MaterialButton(
                          minWidth:
                              MediaQuery.of(this.state.context).size.width,
                          padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                          onPressed: state.requestMaintenance,
                          child: Text(
                            "SUBMIT",
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ));
  }
}
