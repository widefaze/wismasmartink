// import 'package:dio/dio.dart';
import 'package:dio/dio.dart';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';
import 'package:wismartlink/model/CartModel.dart';
import 'package:wismartlink/page/marketplace/checkout.dart';
import 'package:wismartlink/page/restaurant/restaurant_page.dart';

class CartFood extends StatefulWidget {
  const CartFood(this.counter, this.subttl);
  final String counter, subttl;
  @override
  _CartFoodController createState() => _CartFoodController();
}

class _CartFoodController extends State<CartFood> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  var id = '';
  final _formKey = GlobalKey<FormState>();
  // you must keep track of the TextEditingControllers if you want the values to persist correctly
  List<TextEditingController> controllers = <TextEditingController>[];
  TextEditingController namaBarangController = TextEditingController();
  TextEditingController hargaController = TextEditingController();
  TextEditingController keteranganController = TextEditingController();
  bool isLoading = false;
  dynamic pickImageError;
  Future<List<CartModel>> _listFutureCart;
  String _subtotal = "";
  String retrieveDataError;
  File _imageFile;
  int _itemCount = 0;
  String _nomor_unik;

  void showToast(String msg, {int duration, int gravity}) {
    Toast.show(msg, context, duration: Toast.LENGTH_LONG, gravity: gravity);
  }

  Future<List<CartModel>> fetchCart() async {
    var notes = List<CartModel>();

    Response response;
    var param = {"id_user": await getSession("user_id")};
    response = await Dio().post(API_URL + "list_transaksi_resto_belum_lunas",
        data: param,
        options: new Options(
          headers: {"Content-Type": "application/json"},
        ));
    final json = response.data;
    print("json " + json["data"].toString());
    if (response.statusCode == 200) {
      notes = (json["data"])
          .map<CartModel>((item) => CartModel.fromJson(item))
          .toList();
    } else {
      throw Exception('Failed to load');
    }
    print("json " + json.toString());
    return new Future.delayed(new Duration(seconds: 1), () {
      return notes.where((i) => i.namaBarang != "").toList();
    });
  }

  _add_item(id_order) async {
    var param = {"id_order": id_order, "id_user": await getSession("user_id")};
    final url = API_URL;
    var dio = new Dio();
    try {
      Response response;
      response = await dio.post(url + "add_item_food",
          data: param,
          options: new Options(
            headers: {"Content-Type": "application/x-www-form-urlencoded"},
          ));

      print(response.data.toString());
      print(response.data['success']);
      if (response.data['success'] == true) {
        showToast("Success added 1 item", duration: 5, gravity: Toast.BOTTOM);
        setState(() {
          get_subtotal_shop();
          _listFutureCart = fetchCart();
        });
      } else {
        showToast("Failed added 1 item", duration: 5, gravity: Toast.BOTTOM);
      }
    } on DioError catch (e) {
      debugPrint("Error $e");
    }
  }

  _remove_item(id_order) async {
    var param = {"id_order": id_order, "id_user": await getSession("user_id")};
    final url = API_URL;
    var dio = new Dio();
    try {
      Response response;
      response = await dio.post(url + "remove_item_food",
          data: param,
          options: new Options(
            headers: {"Content-Type": "application/x-www-form-urlencoded"},
          ));

      print(response.data.toString());
      print(response.data['success']);
      if (response.data['success'] == true) {
        showToast("Success removed 1 item", duration: 5, gravity: Toast.BOTTOM);
        setState(() {
          get_subtotal_shop();
          _listFutureCart = fetchCart();
        });
      } else {
        showToast("Failed removed 1 item", duration: 5, gravity: Toast.BOTTOM);
      }
    } on DioError catch (e) {
      debugPrint("Error $e");
    }
  }

  _delete_item(id_order) async {
    var param = {"id_order": id_order, "id_user": await getSession("user_id")};
    final url = API_URL;
    var dio = new Dio();
    try {
      Response response;
      response = await dio.post(url + "hapus_order_makan",
          data: param,
          options: new Options(
            headers: {"Content-Type": "application/x-www-form-urlencoded"},
          ));

      print(response.data.toString());
      print(response.data['success']);
      if (response.data['success'] == true) {
        showToast("Success removed 1 item", duration: 5, gravity: Toast.BOTTOM);
        setState(() {
          get_subtotal_shop();
          _listFutureCart = fetchCart();
        });
      } else {
        showToast("Failed removed 1 item", duration: 5, gravity: Toast.BOTTOM);
      }
    } on DioError catch (e) {
      debugPrint("Error $e");
    }
  }

  _delteConfirmation(barangId) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text("Hapus Item",
              style: new TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 14.0)),
          content: SingleChildScrollView(
            child: Container(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Hapus item ini ? ",
                      style: TextStyle(color: Colors.black),
                    )
                  ]),
            ),
          ),
          actions: <Widget>[
            new FlatButton(
              color: Colors.green,
              child: new Text(
                "Delete",
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                Navigator.pop(context);
                _delete_item(barangId);
              },
            ),
            // usually buttons at the bottom of the dialog
            new FlatButton(
              color: Colors.red,
              child: new Text(
                "Cancel",
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                //UPDATE TRACK KE 4.1 YAITU PENGAJUAN DILAKUKAN SURVEY EKSTERNAL
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  get_subtotal_shop() async {
    var param = {"id_user": await getSession("user_id")};
    final url = API_URL;
    var dio = new Dio();
    try {
      Response response;
      response = await dio.post(url + "jumlah_transaksi_resto_belum_lunas",
          data: param,
          options: new Options(
            headers: {"Content-Type": "application/json"},
          ));

      print(response.data.toString());
      print(response.data['success']);
      if (response.data['success'] == true) {
        setState(() {
          print(response.data['data']);

          // var _sub = response.data['data']["subtotal"];
          _subtotal = response.data['data']["subtotal"];
        });
      } else {
        // showToast("Failed added 1 item", duration: 5, gravity: Toast.BOTTOM);
        // get_subtotal_shop();
      }
    } on DioError catch (e) {
      debugPrint("Error $e");
    }
  }

  get_nomor_unik() async {
    var param = {"id_user": await getSession("user_id")};
    final url = API_URL;
    var dio = new Dio();
    try {
      Response response;
      response = await dio.post(url + "nomor_unik",
          data: param,
          options: new Options(
            headers: {"Content-Type": "application/json"},
          ));

      print(response.data.toString());
      print(response.data['success']);
      if (response.data['success'] == true) {
        setState(() {
          print(response.data['data']);

          _nomor_unik = response.data['nomor_unik'];
          // print('_nomor_unik ' + response.data['nomor_unik']);
        });
      } else {
        // showToast("Failed added 1 item", duration: 5, gravity: Toast.BOTTOM);
        // get_subtotal_shop();
      }
    } on DioError catch (e) {
      debugPrint("Error $e");
    }
  }

  @override
  void initState() {
    _subtotal = widget.subttl;
    super.initState();
    _itemCount = 0;

    (() async {
      get_nomor_unik();
      _listFutureCart = fetchCart();
    })();
  }

  _listItem(CartModel cart) {
    return Container(
        height: 100,
        margin: EdgeInsets.fromLTRB(15, 10, 15, 0),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
            border: Border.all(color: Colors.grey[300])),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Image.network(
              cart.img,
              width: 125,
              height: 100,
              fit: BoxFit.cover,
            ),
            Flexible(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(height: 10),
                Container(
                  margin: const EdgeInsets.only(left: 15.0),
                  child: Text(
                    cart.namaBarang,
                    style:
                        TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
                  ),
                ),
                Spacer(),
                Row(
                  children: [
                    Expanded(
                      child: Container(
                        margin: const EdgeInsets.only(left: 15.0),
                        child: Text(
                          thousandSeparator(cart.totalHarga.toString()),
                          style: TextStyle(
                              color: Colors.blue[300],
                              fontSize: 15.0,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey[400]),
                      ),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          cart.qty != '0'
                              ? new GestureDetector(
                                  child: new Icon(Icons.remove),
                                  onTap: () {
                                    _remove_item(cart.id);
                                  })
                              : new Container(),
                          Container(
                            padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                            decoration: BoxDecoration(
                              border: Border(
                                left: BorderSide(color: Colors.grey[400]),
                                right: BorderSide(color: Colors.grey[400]),
                              ),
                            ),
                            child: new Text(cart.qty.toString(),
                                style: TextStyle(color: Colors.black)),
                          ),
                          new GestureDetector(
                              child: new Icon(Icons.add),
                              onTap: () {
                                _add_item(cart.id);
                              }),
                        ],
                      ),
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.delete_outline,
                        color: Colors.red,
                      ),
                      onPressed: () async {
                        _delteConfirmation(cart.id);
                      },
                    ),
                  ],
                ),
              ],
            )),
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: hexToColor("#344b6b"),
          title: Text("Shopping Cart"),
          elevation: 0,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () =>
                Navigator.of(context).pushReplacement(new MaterialPageRoute(
              builder: (BuildContext context) => new RestaurantPage(),
              // new Notif(),
            )),
          )),
      body: Container(
        child: Column(
          children: <Widget>[
            Expanded(
              child: Container(
                child: new FutureBuilder<List>(
                    future: _listFutureCart,
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        if (snapshot.data.length > 0) {
                          return new ListView.builder(
                            itemCount: snapshot.data.length,
                            padding: EdgeInsets.only(top: 4.0),
                            itemBuilder: (context, index) {
                              return _listItem(snapshot.data[index]);
                            },
                          );
                        } else {
                          return Center(
                              child: Text(
                            "Tidak Ada Data",
                            style: TextStyle(fontSize: 25.0),
                          ));
                        }
                      }
                      return Center(
                        child: SizedBox(
                            width: 40.0,
                            height: 40.0,
                            child: const CircularProgressIndicator()),
                      );
                    }),
              ),
            ),
            // new Positioned(
            //   bottom: 0.0,
            //   child: new Container(
            //     width: MediaQuery.of(context).size.width,
            //     color: Colors.white,
            //     margin: const EdgeInsets.all(0.0),
            //     child: new Row(
            //       crossAxisAlignment: CrossAxisAlignment.start,
            //       children: <Widget>[
            //         Flexible(
            //           flex: 6,
            //           child: Container(
            //             margin: const EdgeInsets.only(top: 20.0),
            //             // height: 55.0, // height of the button
            //             // width: 195.0,
            //             child: Row(
            //               mainAxisAlignment: MainAxisAlignment.center,
            //               children: <Widget>[
            //                 Center(
            //                   child: Text(
            //                     "Subtotal : " +
            //                         thousandSeparator(_subtotal.toString()),
            //                     style: TextStyle(
            //                         fontSize: 15.0,
            //                         fontWeight: FontWeight.bold),
            //                     // textAlign: TextAlign.left,
            //                   ),
            //                 ),
            //               ],
            //             ),
            //           ),
            //         ),
            //         Padding(
            //           padding: EdgeInsets.only(left: 10.0, top: 10.0),
            //         ),
            //         Flexible(
            //           flex: 4,
            //           child: InkWell(
            //             onTap: () {
            //               Navigator.of(context)
            //                   .push(new MaterialPageRoute(builder: (_) {
            //                 return new Checkout(widget.counter, widget.subttl,
            //                     _nomor_unik, "resto");
            //               }));
            //             },
            //             child: Align(
            //               alignment: Alignment.topRight,
            //               child: Container(
            //                 decoration: BoxDecoration(
            //                   color: Color.fromRGBO(0, 185, 92, 1),
            //                   boxShadow: <BoxShadow>[
            //                     new BoxShadow(
            //                       color: Colors.black12,
            //                       blurRadius: 10.0,
            //                       offset: new Offset(0.0, 10.0),
            //                     ),
            //                   ],
            //                 ),

            //                 height: 55.0, // height of the button
            //                 width: 95.0, // width of the button
            //                 child: Center(
            //                     child: Text("Checkout",
            //                         style: TextStyle(
            //                             fontSize: 15.0,
            //                             fontWeight: FontWeight.bold,
            //                             color: Colors.white))),
            //               ),
            //             ),
            //           ),
            //         )
            //       ],
            //     ),
            //   ),
            // ),
          ],
        ),
      ),
      bottomNavigationBar: Container(
        height: 125,
        color: Colors.white,
        child: Padding(
          padding: EdgeInsets.only(top: 4.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Row(
                  children: [
                    SizedBox(width: 25),
                    Text(
                      "Total : ",
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.bold),
                    ),
                    Spacer(),
                    Text(
                      thousandSeparator(_subtotal.toString()),
                      style: TextStyle(
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.red,
                      ),
                      // textAlign: TextAlign.left,
                    ),
                    SizedBox(width: 25),
                  ],
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.of(context)
                      .push(new MaterialPageRoute(builder: (_) {
                    return new Checkout(
                        widget.counter, widget.subttl, _nomor_unik, "resto");
                  }));
                },
                child: Align(
                  alignment: Alignment.topRight,
                  child: Container(
                    decoration: BoxDecoration(
                      color: Color.fromRGBO(0, 185, 92, 1),
                      boxShadow: <BoxShadow>[
                        new BoxShadow(
                          color: Colors.black12,
                          blurRadius: 10.0,
                          offset: new Offset(0.0, 10.0),
                        ),
                      ],
                    ),

                    height: 55.0, // height of the button
                    child: Center(
                        child: Text("Pesan",
                            style: TextStyle(
                                fontSize: 18.0,
                                fontWeight: FontWeight.w600,
                                color: Colors.white))),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
