import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:wismartlink/page/restaurant/restaurant_page.dart';

class MakananDetailPage extends StatefulWidget {
  const MakananDetailPage(
      this.id, this.nama_makanan, this.harga, this.img, this.keterangan);
  final String id;
  final String nama_makanan;
  final String harga;
  final String img;
  final String keterangan;

  @override
  _MakananDetailPageController createState() => _MakananDetailPageController();
}

class _MakananDetailPageController extends State<MakananDetailPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isLoading = false;
  bool isPunyaToko = false;
  List data;
  TextEditingController searchController = new TextEditingController();
  ScrollController _scrollController;
  bool _isScrollLimitReached = true;
  int _itemCount = 0;
  @override
  void initState() {
    super.initState();
    _itemCount = 0;

    // getData();
  }

  // void getData() async {
  //   setState(() => isLoading = true);

  //   try {
  //     Response response = await new Dio().post(
  //         API_URL + 'list_barang_by_toko_id',
  //         data: new FormData.fromMap({
  //           "id_toko": await getSession("id_toko"),
  //           "keyword": searchController.text
  //         }),
  //         options: Options(method: 'POST', responseType: ResponseType.json));

  //     setState(() => isLoading = false);

  //     if (response.toString() != '') {
  //       setState(() {
  //         data = response.data['data'];
  //       });
  //     } else {
  //       showSnackBar(scaffoldKey, NETWORK_ERROR);
  //     }
  //   } on DioError catch (e) {
  //     print(e);
  //     showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
  //   }
  // }

  // void awaitReturnRegisterBarang() async {
  //   final result = await Navigator.push(
  //       context,
  //       MaterialPageRoute(
  //         builder: (context) => BarangRegisterPage(),
  //       ));

  //   getData();
  // }

  _orderConfirmation() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return StatefulBuilder(builder: (context, setState) {
          return AlertDialog(
            title: new Text("",
                style: new TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 14.0)),
            content: SingleChildScrollView(
              child: new Container(
                child: new Center(
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text("Jumlah Order : ",
                          style: new TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 14.0)),
                      _itemCount != 0
                          ? new IconButton(
                              icon: new Icon(Icons.remove),
                              color: Colors.black,
                              onPressed: () => setState(() {
                                _itemCount--;
                                // _orderConfirmation();
                              }),
                            )
                          : new Container(),
                      new Text(_itemCount.toString(),
                          style: TextStyle(color: Colors.black)),
                      new IconButton(
                          icon: new Icon(Icons.add),
                          color: Colors.black,
                          onPressed: () => setState(() {
                                _itemCount++;
                                // _orderConfirmation();
                              }))
                    ],
                  ),
                ),
              ),
            ),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              Center(
                child: Row(
                  children: <Widget>[
                    _masukkanKeranjang(),
                    new FlatButton(
                      color: Colors.red,
                      child: new Text(
                        "Batal",
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: () {
                        //UPDATE TRACK KE 4.1 YAITU PENGAJUAN DILAKUKAN SURVEY EKSTERNAL
                        Navigator.pop(context);
                      },
                    ),
                  ],
                ),
              ),
            ],
          );
        });
      },
    );
  }

  _orderBarang() async {
    var param = {
      "id_user": await getSession("user_id"),
      "id_makanan": widget.id,
      "jumlah": _itemCount
    };
    print('param ' + param.toString());
    // print('_selectedDestination ' + _selectedDestination.toString());
    final url = API_URL;
    var dio = new Dio();
    try {
      Response response;
      response = await dio.post(url + "beli_makan",
          data: param,
          options: new Options(
            headers: {"Content-Type": "application/x-www-form-urlencoded"},
          ));

      print(response.data.toString());
      print(response.data['success']);
      if (response.data['success'] == true) {
        Alert(
          context: context,
          type: AlertType.success,
          title: "Order Makanan",
          desc: response.data['message'],
          buttons: [
            DialogButton(
              child: Text(
                "OK",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              onPressed: () {
                Navigator.pop(context);
                Navigator.of(context)
                    .pushReplacement(new MaterialPageRoute(builder: (_) {
                  return new MakananDetailPage(widget.id, widget.nama_makanan,
                      widget.harga, widget.img, widget.keterangan);
                }));
              },
              width: 120,
            )
          ],
        ).show();
      } else {
        Alert(
          context: context,
          type: AlertType.error,
          title: "Order Makanan",
          desc: response.data['message'],
          buttons: [
            DialogButton(
              child: Text(
                "OK",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              onPressed: () {
                Navigator.pop(context);
              },
              width: 120,
            )
          ],
        ).show();
      }
    } on DioError catch (e) {
      debugPrint("Error $e");
    }
  }

  Widget _titleArtikel() {
    if (_isScrollLimitReached == true) {
      return new Container(
        padding:
            EdgeInsets.only(left: 5.0, right: 5.0, top: 10.0, bottom: 10.0),
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          color: hexToColor("#344b6b"),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Text(
              widget.nama_makanan,
              style: new TextStyle(
                  color: Colors.white,
                  fontFamily: "Poppins",
                  fontWeight: FontWeight.bold),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 5.0),
            ),
            Text(thousandSeparator(widget.harga),
                style: TextStyle(
                    fontSize: 11.0,
                    color: Colors.white,
                    fontWeight: FontWeight.bold))
          ],
        ),
      );
    } else {
      return Container();
    }
  }

  Widget _flexible() {
    if (_isScrollLimitReached == false) {
      return FlexibleSpaceBar(
          title: _isScrollLimitReached
              ? ConstrainedBox(
                  constraints: BoxConstraints(maxWidth: 200),
                  child: Text(
                    'aaaaaa',
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                )
              : Text(
                  'assssaaa',
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
          collapseMode: CollapseMode.parallax,
          background: InkWell(
            onTap: () {},
            child: Image.network(
              widget.img,
              width: 80.0,
              height: 80.0,
            ),
          ));
    } else {
      return FlexibleSpaceBar(
          titlePadding: EdgeInsets.all(0.0),
          title: _isScrollLimitReached
              ? ConstrainedBox(
                  constraints: BoxConstraints(maxWidth: 200),
                  child: Text(
                    "",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                )
              : Text(
                  "",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
          collapseMode: CollapseMode.parallax,
          background: InkWell(
            onTap: () {},
            child: Image.network(
              widget.img,
              width: 180.0,
              height: 180.0,
            ),
          ));
    }
  }

  Widget _masukkanKeranjang() {
    if (_itemCount == 0) {
      return FlatButton(
        color: Colors.grey[100],
        child: new Text(
          "Masukkan Keranjang",
          style: TextStyle(color: Colors.black),
        ),
        onPressed: () {
          // Navigator.pop(context);
          // _orderBarang(widget.id);
        },
      );
    } else {
      return FlatButton(
        color: Colors.indigo[700],
        child: new Text(
          "Masukkan Keranjang",
          style: TextStyle(color: Colors.white),
        ),
        onPressed: () {
          Navigator.pop(context);
          _orderBarang();
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        leading: new IconButton(
            icon: new Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).push(new MaterialPageRoute(builder: (_) {
                return new RestaurantPage();
              }));
            }),
        backgroundColor: hexToColor("#344b6b"),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(children: <Widget>[Text(widget.nama_makanan)]),
            Column(children: <Widget>[])
          ],
        ),
      ),
      body: NestedScrollView(
        controller: _scrollController,
        scrollDirection: Axis.vertical,
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              expandedHeight: 200.0,
              floating: false,
              primary: true,
              pinned: true,
              backgroundColor: Colors.white,
              flexibleSpace: _flexible(),
            ),
          ];
        },
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              _titleArtikel(),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.all(15.0),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          boxShadow: <BoxShadow>[
                            new BoxShadow(
                              color: Colors.black12,
                              blurRadius: 10.0,
                              offset: new Offset(0.0, 10.0),
                            ),
                          ],
                        ),
                        child: Container(
                          child: Html(
                            data: widget?.keterangan ?? '',
                            onImageTap: (src) {},
                          ),
                        )),
                    Padding(
                      padding: EdgeInsets.only(bottom: 10.0),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Container(
        height: 60,
        color: Colors.green,
        child: InkWell(
          onTap: () {
            _orderConfirmation();
          },
          child: Padding(
            padding: EdgeInsets.only(top: 4.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Masukkan Keranjang',
                    style: TextStyle(
                        fontSize: 18.0,
                        color: Colors.white,
                        fontWeight: FontWeight.bold)),

                // _beli(),
                // Column(
                //   children: <Widget>[
                //     Text('Beli Sekarang',
                //         style: TextStyle(
                //             fontSize: 16.0,
                //             color: Colors.white,
                //             fontWeight: FontWeight.bold)),
                //   ],
                // ),
                // Column(
                //   children: <Widget>[
                //     Text('Beli Sekarang',
                //         style: TextStyle(
                //             fontSize: 16.0,
                //             color: Colors.white,
                //             fontWeight: FontWeight.bold)),
                //   ],
                // ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
