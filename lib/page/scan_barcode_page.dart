import 'package:barcode_scan/barcode_scan.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';

class ScanBarcode extends StatefulWidget {
  ScanBarcode({Key key}) : super(key: key);

  @override
  ScanBarcodeState createState() => ScanBarcodeState();
}

class ScanBarcodeState extends State<ScanBarcode> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  bool isLoading = false;

  void scanBarcode() async {
    String hasil;
    try {
      ScanResult result = await BarcodeScanner.scan();
      hasil = result.rawContent;
      if (result.type == ResultType.Cancelled) {
        Navigator.of(context).pop();
      } else {
        getData(hasil);
        if (mounted) setState(() {});
      }
    } catch (e) {
      showSnackBar(scaffoldKey, 'Failed');
    }
    print("hasil = $hasil");
  }

  void getData(String barcode) async {
    setState(() => isLoading = true);
    try {
      Response response = await Dio().post(API_URL_NEW + 'scan/absen',
          data: new FormData.fromMap(
              {"user": await getSession("user_id"), "qrcode": barcode}),
          options: Options(method: 'POST', responseType: ResponseType.json));

      if (mounted) setState(() => isLoading = false);

      if (response.toString() != '') {
        if (response.statusCode == 201) {
          Alert(
            context: scaffoldKey.currentContext,
            type: AlertType.success,
            title: 'success',
            desc: '${response.data['data'][0]["info"]}',
            buttons: [
              DialogButton(
                child: Text(
                  "OK",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
                onPressed: () async {
                  Navigator.pop(context);
                  Navigator.pop(context);
                },
                width: 120,
              )
            ],
          ).show();
        } else {
          showSnackBar(scaffoldKey, 'failed');
        }
      } else {
        showSnackBar(scaffoldKey, NETWORK_ERROR);
      }
    } on DioError catch (e) {
      showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
    } finally {
      if (mounted) setState(() => isLoading = false);
    }
  }

  @override
  void initState() {
    super.initState();
    scanBarcode();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        backgroundColor: hexToColor("#344b6b"),
        title: Text("Scan Barcode"),
      ),
      body: Center(
        child: isLoading
            ? loadingScreen()
            : Material(
                elevation: 5.0,
                borderRadius: BorderRadius.circular(30.0),
                color: Colors.green,
                child: MaterialButton(
                  padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                  onPressed: scanBarcode,
                  child: Text(
                    "Scan",
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
      ),
    );
  }
}
