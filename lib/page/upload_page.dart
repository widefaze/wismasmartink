// import 'package:dio/dio.dart';
import 'package:path/path.dart' as path;
import 'package:async/async.dart';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';

enum FormType { security, janitor, maintenance }

class UploadPage extends StatefulWidget {
  const UploadPage({Key key}) : super(key: key);

  @override
  _UploadPageController createState() => _UploadPageController();
}

class _UploadPageController extends State<UploadPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isLoading = false;
  File imageFile;
  dynamic pickImageError;
  String retrieveDataError;
  TextEditingController namaBarangController = new TextEditingController();
  TextEditingController hargaController = new TextEditingController();
  TextEditingController keteranganController = new TextEditingController();
  File _imageFile;

  FormType formType = FormType.security;

  @override
  Widget build(BuildContext context) => _UploadPageView(this);

  void changeFormType(FormType type) {
    formType = type;
    setState(() {});
  }

  void submit() async {
    if (namaBarangController.text.isEmpty) {
      showSnackBar(scaffoldKey, "Judul harap diisii");
    } else if (keteranganController.text.isEmpty) {
      showSnackBar(scaffoldKey, "Keterangan harap diisi");
    } else if (_imageFile == null) {
      showSnackBar(scaffoldKey, "Gambar Barang belum dipilih");
    } else {
      setState(() => isLoading = true);

      try {
        var stream =
            http.ByteStream(DelegatingStream.typed(_imageFile.openRead()));
        var length = await _imageFile.length();
        var uri = Uri.parse(API_URL_NEW + 'upload');
        var request = http.MultipartRequest("POST", uri);
        request.fields['title'] = namaBarangController.text;
        request.fields['description'] = keteranganController.text;

        request.files.add(http.MultipartFile("image", stream, length,
            filename: path.basename(_imageFile.path)));

        var response = await request.send();
        if (response.toString() != '') {
          if (response.statusCode == 201) {
            Alert(
              context: scaffoldKey.currentContext,
              type: AlertType.success,
              title: 'success',
              buttons: [
                DialogButton(
                  child: Text(
                    "OK",
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                  onPressed: () async {
                    Navigator.pop(context);
                    Navigator.pop(context);
                  },
                  width: 120,
                )
              ],
            ).show();
          } else {
            showSnackBar(scaffoldKey, 'failed');
          }
        } else {
          showSnackBar(scaffoldKey, NETWORK_ERROR);
        }
      } catch (e) {
        debugPrint("Error $e");
      }
    }
  }

  void chooseImage(ImageSource source) async {
    try {
      imageFile = await ImagePicker.pickImage(source: source, imageQuality: 50);
      setState(() {});
    } catch (e) {
      pickImageError = e;
    }
  }

  _selectImage(ImageSource source) async {
    var image = await ImagePicker.pickImage(
        source: source, maxHeight: 1920.0, maxWidth: 1080.0);
    setState(() {
      _imageFile = image;
    });
  }

  Widget _previewImage() {
    final Text retrieveError = _getRetrieveErrorWidget();
    if (retrieveError != null) {
      return retrieveError;
    }
    if (_imageFile != null) {
      return ClipOval(
          child: Image.file(
        _imageFile,
        width: 200,
        height: 200,
      ));
    } else if (pickImageError != null) {
      return Text(
        'Pick image error: $pickImageError',
        textAlign: TextAlign.center,
      );
    } else {
      return const Text(
        'Gambar Barang',
        textAlign: TextAlign.center,
      );
    }
  }

  Text _getRetrieveErrorWidget() {
    if (retrieveDataError != null) {
      final Text result = Text(retrieveDataError);
      retrieveDataError = null;
      return result;
    }
    return null;
  }

  Future<void> retrieveLostData() async {
    final LostDataResponse response = await ImagePicker.retrieveLostData();
    if (response.isEmpty) {
      return;
    }
    if (response.file != null) {
      setState(() {
        imageFile = response.file;
      });
    } else {
      retrieveDataError = response.exception.code;
    }
  }
}

class _UploadPageView extends StatelessWidget {
  final _UploadPageController state;

  const _UploadPageView(this.state, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: state.scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: hexToColor("#344b6b"),
          title: Text("Open Tiket"),
        ),
        body: state.isLoading
            ? loadingScreen()
            : Column(
                children: [
                  Expanded(
                    child: SingleChildScrollView(
                      child: Container(
                        color: Colors.white,
                        child: Padding(
                          padding: const EdgeInsets.all(25),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                'Komplain Form Kepada',
                                style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w700,
                                  color: Colors.blue,
                                ),
                              ),
                              SizedBox(height: 15),
                              RadioListTile(
                                title: Text('Keamanan'),
                                value: FormType.security,
                                groupValue: state.formType,
                                onChanged: (v) {
                                  state.changeFormType(v);
                                },
                              ),
                              RadioListTile(
                                title: Text('Kebersihan'),
                                value: FormType.janitor,
                                groupValue: state.formType,
                                onChanged: (v) {
                                  state.changeFormType(v);
                                },
                              ),
                              RadioListTile(
                                title: Text('Maintenance'),
                                value: FormType.maintenance,
                                groupValue: state.formType,
                                onChanged: (v) {
                                  state.changeFormType(v);
                                },
                              ),
                              SizedBox(height: 15),
                              // TextField(
                              //   controller: state.namaBarangController,
                              //   decoration: InputDecoration(
                              //       contentPadding:
                              //           EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                              //       hintText: "Judul",
                              //       border: OutlineInputBorder(
                              //           borderRadius: BorderRadius.circular(32.0))),
                              // ),
                              // SizedBox(
                              //   height: 10.0,
                              // ),
                              TextField(
                                maxLines: 8,
                                controller: state.keteranganController,
                                decoration: InputDecoration(
                                    contentPadding: EdgeInsets.fromLTRB(
                                        20.0, 15.0, 20.0, 15.0),
                                    hintText: "Deskripsi",
                                    border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(10))),
                              ),
                              SizedBox(
                                height: 10.0,
                              ),
                              Container(
                                width: double.maxFinite,
                                child: OutlineButton(
                                  borderSide: BorderSide(color: Colors.purple),
                                  padding: EdgeInsets.all(15),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5)),
                                  color: Colors.blue[300],
                                  child: Text(
                                    'Unggah Foto',
                                    style: TextStyle(
                                      color: Colors.purple,
                                    ),
                                  ),
                                  onPressed: () {
                                    state._selectImage(ImageSource.gallery);
                                  },
                                ),
                              ),
                              // Row(
                              //   children: <Widget>[

                              //     Padding(
                              //       padding: const EdgeInsets.all(5),
                              //       child: FlatButton(
                              //         onPressed: () {
                              //           state._selectImage(ImageSource.gallery);
                              //         },
                              //         child: const Icon(Icons.photo_library),
                              //       ),
                              //     ),
                              // Padding(
                              //   padding: const EdgeInsets.all(5),
                              //   child: FloatingActionButton(
                              //     onPressed: () {
                              //       state._selectImage(ImageSource.camera);
                              //     },
                              //     heroTag: 'image1',
                              //     child: const Icon(Icons.photo_camera),
                              //   ),
                              // ),
                              // Platform.isAndroid
                              //     ? FutureBuilder<void>(
                              //         future: state.retrieveLostData(),
                              //         builder: (BuildContext context,
                              //             AsyncSnapshot<void> snapshot) {
                              //           switch (snapshot.connectionState) {
                              //             case ConnectionState.none:
                              //             case ConnectionState.waiting:
                              //               return const Text(
                              //                 'Gambar belum dipilih.',
                              //                 textAlign: TextAlign.center,
                              //               );
                              //             case ConnectionState.done:
                              //               return state._previewImage();
                              //             default:
                              //               if (snapshot.hasError) {
                              //                 return Text(
                              //                   'Pick image/video error: ${snapshot.error}}',
                              //                   textAlign: TextAlign.center,
                              //                 );
                              //               } else {
                              //                 return const Text(
                              //                   'Gambar belum dipilih.',
                              //                   textAlign: TextAlign.center,
                              //                 );
                              //               }
                              //           }
                              //         },
                              //       )
                              //     : state._previewImage()
                              //   ],
                              // ),

                              SizedBox(
                                height: 10.0,
                              ),
                              Container(
                                width: double.maxFinite,
                                child: FlatButton(
                                  padding: EdgeInsets.all(15),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5)),
                                  color: Colors.blue[300],
                                  child: Text('Kirim Info / Komplain'),
                                  onPressed: state.submit,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Row(children: [
                    Expanded(
                      child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(0.0)),
                        child: Image(
                            image: AssetImage('assets/images/iklan1.jpg')),
                      ),
                    ),
                    Expanded(
                      child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(0.0)),
                        child: Image(
                            image: AssetImage('assets/images/iklan2.jpg')),
                      ),
                    ),
                  ]),
                ],
              ));
  }
}

typedef void OnPickImageCallback(
    double maxWidth, double maxHeight, int quality);
