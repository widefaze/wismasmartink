import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:wismartlink/helper/utility.dart';
import 'package:wismartlink/model/voucherModel.dart';
import 'package:html/dom.dart' as dom;

class VoucherDetailPage extends StatefulWidget {
  final Voucher item;
  const VoucherDetailPage({Key key, this.item}) : super(key: key);

  @override
  _VoucherPageController createState() => _VoucherPageController();
}

class _VoucherPageController extends State<VoucherDetailPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  bool isLoading = false;

  @override
  Widget build(BuildContext context) => _VoucherPageView(this);
}

class _VoucherPageView extends StatelessWidget {
  final _VoucherPageController state;

  const _VoucherPageView(this.state, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: state.scaffoldKey,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: hexToColor("#344b6b"),
        title: Text(state.widget.item.title),
      ),
      body: ListView(
        children: <Widget>[
          Image.network(state.widget.item.image),
          Padding(
            padding: const EdgeInsets.all(20),
            child: Html(
              data: """${state.widget.item.description}""",
              //Optional parameters:
              padding: EdgeInsets.all(8.0),
              linkStyle: const TextStyle(
                color: Colors.redAccent,
                decorationColor: Colors.redAccent,
                decoration: TextDecoration.underline,
              ),

              //Must have useRichText set to false for this to work
              customRender: (node, children) {
                if (node is dom.Element) {
                  switch (node.localName) {
                    case "br":
                      return Column(children: children);
                  }
                }
                return null;
              },
              customTextAlign: (dom.Node node) {
                if (node is dom.Element) {
                  switch (node.className) {
                    case "ql-align-right":
                      return TextAlign.right;
                    default:
                      return TextAlign.justify;
                  }
                }
                return null;
              },
              customTextStyle: (dom.Node node, TextStyle baseStyle) {
                if (node is dom.Element) {
                  switch (node.localName) {
                    case "p":
                      return baseStyle.merge(
                        TextStyle(
                          height: 1.5,
                          fontSize: 14,
                        ),
                      );
                    case "br":
                      return baseStyle.merge(
                        TextStyle(
                          height: 0,
                          fontSize: 0,
                        ),
                      );
                  }
                }
                return baseStyle;
              },
            ),
          ),
        ],
      ),
    );
  }
}
