import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:wismartlink/component/component.dart';
import 'package:wismartlink/helper/constants.dart';
import 'package:wismartlink/helper/utility.dart';
import 'package:wismartlink/model/voucherModel.dart';
import 'package:wismartlink/page/voucher_detail_page.dart';

class VoucherPage extends StatefulWidget {
  const VoucherPage({Key key}) : super(key: key);

  @override
  _VoucherPageController createState() => _VoucherPageController();
}

class _VoucherPageController extends State<VoucherPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  bool isLoading = false;

  String kodeTiket;
  List<Voucher> data;

  @override
  void initState() {
    super.initState();
    fetchData();
  }

  @override
  Widget build(BuildContext context) => _VoucherPageView(this);

  Future fetchData() async {
    setState(() => isLoading = true);

    try {
      Response response;

      response = await Dio().get(API_URL_NEW + "voucher",
          options: Options(responseType: ResponseType.json));

      var json = response.data;

      print("json " + json["data"].toString());

      if (response.toString() != '') {
        if (response.statusCode == 200) {
          if (json['data'].isNotEmpty) {
            List<Voucher> temp = [];
            json['data'].forEach((v) {
              temp.add(voucherFromJson(v));
            });
            data = temp;
          }
          if (mounted) setState(() {});
        } else {
          throw Exception('Failed to load');
        }
      } else {
        showSnackBar(scaffoldKey, NETWORK_ERROR);
      }
    } on DioError catch (e) {
      showSnackBar(scaffoldKey, SERVER_RESPONSE_ERROR);
    } catch (e) {
      debugPrint("Error $e");
    } finally {
      if (mounted) setState(() => isLoading = false);
    }
  }
}

class _VoucherPageView extends StatelessWidget {
  final _VoucherPageController state;

  const _VoucherPageView(this.state, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: state.scaffoldKey,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: hexToColor("#344b6b"),
        title: Text("Voucher"),
      ),
      body: state.data == null
          ? loadingScreen()
          : Column(
              children: <Widget>[
                Flexible(
                  child: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: ListView.separated(
                      itemCount: state.data?.length ?? 0,
                      itemBuilder: (context, index) {
                        Voucher item = state.data[index];
                        return GestureDetector(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => VoucherDetailPage(
                                      item: item,
                                    )));
                          },
                          child: Image.network(
                            item.image,
                          ),
                        );
                      },
                      separatorBuilder: (context, index) {
                        return Divider();
                      },
                    ),
                  ),
                ),
              ],
            ),
    );
  }
}
